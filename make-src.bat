rd tbots-src /S /Q
md tbots-src\T-Bots
xcopy TBots\Src\* tbots-src\T-Bots /E /Y /EXCLUDE:exclude.txt
del tbots-src\.* /S
del tbots-src\*.aps /S
del tbots-src\*.plg /S
del tbots-src\*.opt /S
del tbots-src\*.ncb /S
del tbots-src\*.3ds /S
del tbots-src\conv3ds.exe /S
xcopy TBots\license.txt tbots-src\T-Bots /Y
xcopy TBots\changelog.txt tbots-src\T-Bots /Y
