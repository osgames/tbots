	CHANGELOG FOR T-Bots
	====================

v0.3b
----------------------
- 		         -
----------------------
- Mine Bug Fixed (animation when a mine was planted : 2D View)
- Updating DLL Bot Sample (PretMoaTBot)
- Laser Bug Fixed
- Added Global Ambiant Light for better 3D View
- Decrease power of Laser item
- Interface 2D/3D Updated (Win32 Only)
- Bugs Fixes

v0.3a
----------------------
- February 28th, 2004    -
----------------------
- Changed the API Render to OpenGL
- Upgraded the project for Visual C++ 7.0
- Linux Compatibility
- Adding lighting
- Bugs Fixes

v0.2c
----------------------
- September 09th, 2003   -
----------------------
- Upgrading Render DirectX8 To DirectX9
- DrawDebugBots removed
- FPS Display added
- Bugs fixes

v0.2b
----------------------
- August 04th, 2003   -
----------------------
- Full 3D Renderer Mode Added. (Self-illumination lightning, no fx, no real camera)
- Updating DLL Bot Sample (PretMoaTBot)
- Updating DLL Bot Sample (LechTBot)
- Bugs fixes

v0.2a
----------------------
- July 28th, 2003   -
----------------------
- The Game Design is fully implemented, changing status to "Alpha"
- DLL Context added
- Map Choice
- Updating DLL Bot Sample (PretMoaTBot)
- Items Respawning
- Random Bot start order added
- Restart Battle bug fixed
- Winner Text updated
- Vertical shift of the font fixed
- Bugs fixes

v0.1h
----------------------
- July 24th, 2003   -
----------------------
- Max Life fixed
- Winner Text added
- "Clear Map" button in the Maze Editor added 
- New *.MAZ file icon added 
- Battles management (take in account the power ability of a bot or the distance) 
- Dead bots management (tombs don't block anymore) 
- Bonus Joker is now functional 
- Agility removed
- Bugs Fixed

v0.1g
----------------------
- July 23th, 2003   -
----------------------
- Add a pathfinder to (PretMoaTBot)
- Add a class system to (PretMoaTBot) : 
  When the match begin (PretMoaTBot) choose to be an intelligent bot or a killer bot
- Add management of new items to (PretMoaTBot)
- Updating Mine Item
- Adding Drop Item action
- Adding a new DLL Bot Sample (PrenMesBot)
- Updating DLL Bot Sample (RuffiTBot)
- Updating DLL Bot Sample (PretMoaTBot)
- Bugs Fixed

v0.1f
----------------------
- July 22th, 2003   -
----------------------
- Adding Points Bots Feature
- Adding Drop Item action
- Updating the GetpAction() DLL Function
- Bugs Fixed

v0.1e
----------------------
- July 21th, 2003   -
----------------------
- Adding Step-By-Step Game Feature
- Adding Binoculars Item
- Adding Mine Item
- Adding Laser Item
- Updating DLL Bot Sample (LechTBot)
- Bugs Fixed

v0.1d
----------------------
- July 20th, 2003   -
----------------------
- Adding the Restart Game/Battle Feature
- Adding Regenerator Item
- Adding Medikit Item
- Adding a new DLL Bot Sample (PretMoaTBot)
- Adding a new DLL Bot Sample (R_Bot)
- Bugs Fixed

v0.1c
----------------------
- July 19th, 2003   -
----------------------
- DLL loading bug fixed
- Adding Inventary management
- Adding a new DLL Bot Sample (LechBot)
- Bugs Fixed

v0.1b
----------------------
- July 18th, 2003   -
----------------------
- Updating MapEditor (Tile library, Flip H/V of tiles)
- Adding Bonus Items
- Updating Protocol (Core <=> Dll)
- Updating K-Bot
- Bugs Fixed

v0.1a
----------------------
- July 17th, 2003   -
----------------------
The First version ! :)
- MapEditor
- Core
- DLL Bot Sample (K-Bot)
