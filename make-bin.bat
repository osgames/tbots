rd tbots-bin /S /Q
md tbots-bin\T-Bots
xcopy TBots\Src\Bin\*.* tbots-bin\T-Bots /E /Y
del  tbots-bin\.* /S
del  tbots-bin\*.3ds /S
del  tbots-bin\conv3ds.exe /S
xcopy TBots\license.txt tbots-bin\T-Bots /Y
xcopy TBots\changelog.txt tbots-bin\T-Bots /Y
