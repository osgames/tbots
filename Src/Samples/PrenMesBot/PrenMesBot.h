//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: PrenMesBot.h
//	Author			: - Aitonfrere	
//					  - Dino			dino@zythum-project.com
//
//	Date			: 22/07/2003
//	Modification	:
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------

// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the PRENMESBOT_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// PRENMESBOT_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#ifdef _WIN32
#ifdef PRENMESBOT_EXPORTS
#define PRENMESBOT_API __declspec(dllexport)
#else
#define PRENMESBOT_API __declspec(dllimport)
#endif
#endif // _WIN32

#ifdef _LINUX
#define PRENMESBOT_API
#endif // _LINUX

#include "BotDefs.h"
#include "Level.h"
#include "Bot.h"

//--------------------------------------------------------------------------------------------
// Fonctions export�
extern "C" PRENMESBOT_API char* GetpBotName();
extern "C" PRENMESBOT_API KBotInfo* GetpInfos( void* pContext );
extern "C" PRENMESBOT_API void* StartGame( s32 MapWidth, s32 MapHeight );
extern "C" PRENMESBOT_API void EndGame( void* pContext );
extern "C" PRENMESBOT_API KBotAction* GetpBotAction( KBotActionInfo* pBotActionInfo, void* pContext );
//extern "C" PRENMESBOT_API void DebugDisplay( KRender *pRender, void* pContext );


