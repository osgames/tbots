//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: KBot.cpp
//	Author			: - Aitonfrere	
//					  - Dino			dino@zythum-project.com
//
//	Date			: 17/07/2003
//	Modification	:
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------
#include "Path.h"
#include "KBot.h"

KBotInfo		g_BotInfo;
KBotAction		g_BotAction;
KCell*			g_pMapCell = NULL;
s32				g_MapWidth;
s32				g_MapHeight;
KPathFinder*	g_pPathFinder = NULL;
KPath*			g_pCurrentPath = NULL;
u32				g_CurrentPathPos;

#ifdef _WIN32
//--------------------------------------------------------------------------------------------
BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
    switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
		case DLL_THREAD_ATTACH:
		case DLL_THREAD_DETACH:
		case DLL_PROCESS_DETACH:
			break;
    }
    return TRUE;
}
#endif // _WIN32

//--------------------------------------------------------------------------------------------
KBOT_API char* GetpBotName()
{
	// Nom long du Robot
	return "K-Bot The Killer";
}

//--------------------------------------------------------------------------------------------
KBOT_API KBotInfo* GetpInfos( void* pContext )
{

	// Nom court dur Robot
	g_BotInfo.SetpBotName( "K-Bot" );
	g_BotInfo.SetPoints( 6, 3, 3, 3 );

	return &g_BotInfo;
}

//--------------------------------------------------------------------------------------------
KBOT_API void* StartGame( s32 MapWidth, s32 MapHeight )
{
	g_MapWidth	= MapWidth;
	g_MapHeight	= MapHeight;

	// Creation d'une map qui jouera le role de memoire pour le robot
	g_pMapCell	= new KCell[g_MapWidth * g_MapHeight];
	memset( g_pMapCell, UNKNOWN_CELL_TYPE, g_MapWidth * g_MapHeight * sizeof( KCell ) );

	// Initialisation du pathfinder
	g_pPathFinder = new KPathFinder( g_pMapCell, MapWidth, MapHeight );

	return NULL;
}

//--------------------------------------------------------------------------------------------
KBOT_API void EndGame( void* pContext )
{
	SafeDelete( g_pMapCell );
	SafeDelete( g_pPathFinder );
}

//--------------------------------------------------------------------------------------------
//
//					>>>>>   NOUVELLE FONCTION A ECRIRE   <<<<<
//
//--------------------------------------------------------------------------------------------
/*KBOT_API void DebugDisplay( KRender *pRender, void* pContext )
{
	if( !g_pCurrentPath )
		return;

	for( s32 i = 1; i < g_pCurrentPath->GetSize(); i ++ )
	{
		KPt	Pos = g_pCurrentPath->GetPos( i );
		pRender->DrawQuad( SCREEN_OFFSETX + Pos.x * CELL_SIZE, Pos.y * CELL_SIZE, CELL_SIZE, CELL_SIZE, 0, KRGBA( 255, 0, 0, 64 ) );
	}
}
*/
//--------------------------------------------------------------------------------------------
KBOT_API KBotAction* GetpBotAction( KBotActionInfo* pBotActionInfo, void* pContext )
{
	// Mise a jour de la map
	for( s32 my = 0; my < g_MapHeight; my ++ )
	{
		for( s32 mx = 0; mx < g_MapWidth; mx ++ ) 
		{
			KCell* pCell = &pBotActionInfo->m_pMapCell[my * g_MapWidth + mx];
			if( pCell->GetType() != UNKNOWN_CELL_TYPE )
			{
				g_pMapCell[my * g_MapWidth + mx].SetType( pCell->GetType() );
			}
		}
	}

	// Nothing
	g_BotAction.SetAction( KBA_NOTHING );

	if( !g_pCurrentPath )
	{
		// Cherche une destination
		KPt	Dest = FindTarget( pBotActionInfo->m_Pos, pBotActionInfo->m_pItems, pBotActionInfo->m_nItems );

		if( Dest.Distance( pBotActionInfo->m_Pos ) <= 1.01f )
		{
			KBOTATTACKTARGET	Target = KBAT_UP;

			if( Dest.x < pBotActionInfo->m_Pos.x )
			{
				Target = KBAT_LEFT;
			}
			if( Dest.x > pBotActionInfo->m_Pos.x )
			{
				Target = KBAT_RIGHT;
			}
			if( Dest.y < pBotActionInfo->m_Pos.y )
			{
				Target = KBAT_UP;
			}
			if( Dest.y > pBotActionInfo->m_Pos.y )
			{
				Target = KBAT_DOWN;
			}

			g_BotAction.SetAction( KBA_ATTACK );
			g_BotAction.SetAttackTarget( Target );

			return &g_BotAction;
		}

		// Determine le chemin
		g_pCurrentPath = g_pPathFinder->FindPath( pBotActionInfo->m_Pos.x, pBotActionInfo->m_Pos.y, Dest.x, Dest.y );
		if( !g_pCurrentPath )
		{
			Dest = RandomTarget();
			g_pCurrentPath = g_pPathFinder->FindPath( pBotActionInfo->m_Pos.x, pBotActionInfo->m_Pos.y, Dest.x, Dest.y );
		}

		if( g_pCurrentPath )
		{
			if( g_pCurrentPath->GetSize() )
			{
				g_CurrentPathPos = 1;
			}
			else
			{
				Deletep( g_pCurrentPath );
			}
		}
	}

	if( g_pCurrentPath )
	{
		KPt			NextPos = g_pCurrentPath->GetPos( g_CurrentPathPos );
		KBOTMOVEDIR	ModeDir = KBMD_UP;

		g_CurrentPathPos ++;
		if( g_CurrentPathPos >= g_pCurrentPath->GetSize() )
		{
			Deletep( g_pCurrentPath );
		}

		if( NextPos.x < pBotActionInfo->m_Pos.x )
		{
			ModeDir = KBMD_LEFT;
		}
		if( NextPos.x > pBotActionInfo->m_Pos.x )
		{
			ModeDir = KBMD_RIGHT;
		}
		if( NextPos.y < pBotActionInfo->m_Pos.y )
		{
			ModeDir = KBMD_UP;
		}
		if( NextPos.y > pBotActionInfo->m_Pos.y )
		{
			ModeDir = KBMD_DOWN;
		}

		// Moving
		g_BotAction.SetAction( KBA_MOVE );
		g_BotAction.SetMoveDir( ModeDir );
	}

	return &g_BotAction;
}


//--------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------
KPt FindTarget( KPt& Pos, KItemInfo* pItems, u32 nItems )
{
	KPt		Target = RandomTarget();
	float	Near = 10000.0f;

	for( u32 i = 0; i < nItems; i ++ )
	{
		if( pItems[i].GetPos() != Pos )
		{
			if( ( pItems[i].GetType() == KIT_BOT ) && pItems[i].IsAlive() )
			{
				float Distance = pItems[i].GetPos().Distance( Pos );

				if( Distance < Near )
				{
					Target = pItems[i].GetPos();
					Near = Distance;
				}
			}
		}
	}

	return Target;
}

//--------------------------------------------------------------------------------------------
KItemInfo* GetpItem( KPt& Pos, KItemInfo* pItems, u32 nItems )
{
	for( u32 i = 0; i < nItems; i ++ )
	{
		if( pItems[i].GetPos() == Pos )
			return &pItems[i];
	}

	return NULL;
}

//--------------------------------------------------------------------------------------------
KPt	RandomTarget()
{
	KPt		Target;

	Target.x = rand() % g_MapWidth;
	Target.y = rand() % g_MapHeight;

	return Target;
}
