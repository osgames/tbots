//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 Ruffi
//
//	File			: R_Bot.cpp
//	Author			: - Ruffi
//
//	Date			: 20/07/2003
//	Modification	:
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------
#include "R_Bot.h"

//**************************variables globales******************************

KBotInfo		g_BotInfo;
KBotAction		g_BotAction;
KCell*			g_pMapCell = NULL;
s32				g_MapWidth;
s32				g_MapHeight;
KPt				g_Target(-1,-1);
KBOTMOVEDIR		g_ModeDir;

bool*			g_deja_venu;
bool			StayHere=false ; // sert a dire au bot de rester l�au tour suivant
// je suis sur que le tableau de booleen est inutile car on peu utiliser un autre tableau. 
// Faudra que je regarde sa pour une prochaine version

//**********************fonctions inline*************************************

inline CellType	GetGlobalMapCellType(KPt Pos)					{ return g_pMapCell[Pos.y * g_MapWidth + Pos.x].GetType(); }
inline void		SetGlobalMapCellType(KPt Pos, CellType Type)	{ g_pMapCell[Pos.y * g_MapWidth + Pos.x].SetType(Type); }



//**********************prototype des fonctions**************************

void UpdateMap(KCell* pMapCell) ;
KPt FindNearestItem(KPt BotPosition, KItemInfo* pItems, u32 nItems, KITEMTYPE Type);
void FindTactique(KPt BotPosition, KItemInfo* pItems, u32 nItems, KItemInfo* pInventory, u32	nInventory);
bool CanMove(s32 X,s32 Y);
void MoveRandom(KPt BotPosition);

//***********************fonctions de la dll*********************************
#ifdef _WIN32
BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
		case DLL_THREAD_ATTACH:
		case DLL_THREAD_DETACH:
		case DLL_PROCESS_DETACH:
			break;
    }
    return TRUE;
}
#endif // _WIN32


R_BOT_API char* GetpBotName()
{
	// Nom long du Robot
	return "BilBot The Hobit";
	// G h�it�pour le nom :
	// Bot �Nic (ou BotAnik) The Ecologist
	// PODBot The Gamer (allusion a Counter Strike)
	// BotEntrain The Fetard
	// Bot27lieu The Petit Pousset
	// ChuiBot The Craneur
	// SaBoteur The Terrorist 
	// Botin The Annuaire
	// Bot�atal The TopModel
	// ...
}



R_BOT_API KBotInfo* GetpInfos( void* pContext )
{

	// Nom court dur Robot
	g_BotInfo.SetpBotName( "BilBot" );
	g_BotInfo.SetPoints( 5, 5, 1, /*1,*/ 3 );
	return &g_BotInfo;
}



R_BOT_API void* StartGame( s32 MapWidth, s32 MapHeight )
{
	
	g_MapWidth	= MapWidth;
	g_MapHeight	= MapHeight;
	StayHere=false ;

	// Creation d'une map qui jouera le role de memoire pour le robot
	g_pMapCell	= new KCell[g_MapWidth * g_MapHeight];
	memset( g_pMapCell, UNKNOWN_CELL_TYPE, g_MapWidth * g_MapHeight * sizeof( KCell ) );

	// Creation d'une map qui permetera de savoir si l'on est deja all�quelque part
	//g_deja_venu	= new bool[g_MapWidth * g_MapHeight]=false;

	return NULL;
}



R_BOT_API void EndGame( void* pContext )
{
	SafeDelete( g_pMapCell );
	SafeDelete( g_deja_venu );
}



R_BOT_API KBotAction* GetpBotAction( KBotActionInfo* pBotActionInfo, void* pContext )
{				

// Mise �jour de la map
	UpdateMap( pBotActionInfo->m_pMapCell ) ;	
	FindTactique( pBotActionInfo->m_Pos, pBotActionInfo->m_pItems, pBotActionInfo->m_nItems, pBotActionInfo->m_pInventory, pBotActionInfo->m_nInventory ) ; 

// Finalement, tout se fait dans FindTactique !

//  Ce qui suit est la premier version de R_Bot ( Heureusement, sa �evolu�depuis !)
/*	switch (rand() % 4){

	case 0: break;
	case 1:	g_BotAction.SetAction( KBA_ATTACK );
			g_BotAction.SetAttackTarget( (KBOTATTACKTARGET)(rand() % KBAT_COUNT) );
			break;

	case 2:	switch (rand() % 4){
					case 0:	g_ModeDir = KBMD_LEFT;break;
					case 1:	g_ModeDir = KBMD_RIGHT;break;
					case 2:	g_ModeDir = KBMD_UP;break;	
					case 3:	g_ModeDir = KBMD_DOWN;break;  }
			g_BotAction.SetAction( KBA_MOVE );		
			g_BotAction.SetMoveDir( g_ModeDir );
			break;

	case 3:	g_BotAction.SetAction( KBA_NOTHING );
			break;
	} */

	return &g_BotAction;
}

//*******************fonctions du bot******************************************************

// Fonction FindNearestItem
// Permet de trouver l'Item le plus proche
// Fonction recup��de LechBot
void UpdateMap(KCell* pMapCell)
{
    for( s32 my = 0; my < g_MapHeight; my ++ )
    {
        for( s32 mx = 0; mx < g_MapWidth; mx ++ ) 
        {
            KCell* pCell = &pMapCell[my * g_MapWidth + mx];
            if( pCell->GetType() != UNKNOWN_CELL_TYPE )
            {
                SetGlobalMapCellType( KPt(mx,my), pCell->GetType() );
            }
        }
	}
}


// Fonction FindNearestItem
// Permet de trouver l'Item le plus proche
// Fonction recup��de LechBot ( et un peu adapt�)
KPt FindNearestItem(KPt BotPosition, KItemInfo* pItems, u32 nItems, KITEMTYPE Type)
{
	KPt		Target(-1,-1);
	float	Nearest = 10000.0f;
	float	Distance;

	for( u32 i = 0; i < nItems; i ++ )
	{
		if( pItems[i].GetPos() != BotPosition )
		{
			if( ( pItems[i].GetType() == Type ) )
			{
				if ( pItems[i].GetType() == KIT_BOT && !pItems[i].IsAlive() ) 
					continue;

				Distance = pItems[i].GetPos().Distance( BotPosition );

				if( Distance < Nearest )
				{
					Target = pItems[i].GetPos();
					Nearest = Distance;
				}
			}
		}
	}

	return Target;
}


// Fonction FindTactique
// Permet de trouver ce que le Bot doit faire
void FindTactique(KPt BotPosition, KItemInfo* pItems, u32 nItems, KItemInfo* pInventory, u32	nInventory)
{
//variables static  (Est-ce mieu si je les met en static ou en global ???)
	static s32 type_enemi=0; //0=rien 1=bot 2=bonus 3=coin
	static KPt last_position(-1,-1);
	static s32 NbTours=0;  // sert a savoir depuis combien de tour il recherche un objet
	
//les items les + proches
	KPt DestBot         = FindNearestItem(BotPosition,pItems,nItems, KIT_BOT);
    KPt DestBonusVision = FindNearestItem(BotPosition,pItems,nItems, KIT_BONUSVISION);
    KPt DestBonusPower  = FindNearestItem(BotPosition,pItems,nItems, KIT_BONUSPOWER);
    KPt DestBonusSpeed  = FindNearestItem(BotPosition,pItems,nItems, KIT_BONUSSPEED);
    KPt DestBonusJoker  = FindNearestItem(BotPosition,pItems,nItems, KIT_BONUSJOKER);
	KPt DestRegenerator = FindNearestItem(BotPosition,pItems,nItems, KIT_REGENERATOR);
	KPt DestMedikit  = FindNearestItem(BotPosition,pItems,nItems, KIT_MEDIKIT);
	KPt DestBinoculars  = FindNearestItem(BotPosition,pItems,nItems, KIT_BINOCULARS);
	KPt DestMine  = FindNearestItem(BotPosition,pItems,nItems, KIT_MINE);
	KPt DestLazer  = FindNearestItem(BotPosition,pItems,nItems, KIT_LASER);

// On commence �reflechire... <o_O> 

	// Un nouveau tour commence
	NbTours++;

	// T'arive pas a aller chercher la cible au bout de 10 tours? Recherche en une autre !
	if (NbTours==5)  // faut regarder si 10 tours ce n'est pas trop (ou pas assez)
	{
		StayHere=false; // tu peu fair autre chose maintenant 
		type_enemi=0; // j'initialiserai le nombre de tour lorsqu'il aura trouv�une cible
	}

	// si la cible est un bot, il s'est deplac�: on recherche donc le + proche
	if (type_enemi==1)
		if (DestBot != KPt(-1,-1))
			g_Target=DestBot;
		else
			type_enemi=0;

	// G une mine ? Poson la :)
//	if (g_SuperPouvoir[1]==true)
//	{
//		g_BotAction.SetAction(KBA_DROPMINE);
//		return;
//	}


	// priorit�a la sant� oubli ta cible et fonce faire le plein !
	// TODO partir quand on a toute sa vie :)
	if(DestRegenerator != KPt(-1,-1))		{g_Target=DestRegenerator ; type_enemi=2; NbTours=0; }
	

	//si on a pas de cible...
	if (type_enemi==0 || type_enemi==3)
	{
		// ...on cherche une cible (item ou enemi)
		 
		if(DestRegenerator != KPt(-1,-1))		{g_Target=DestRegenerator ; type_enemi=2; NbTours=0; }   //je sais que c'est pas bien de faire trop de chose sur une ligne :( 
		  
		else if(DestMedikit != KPt(-1,-1))		{g_Target=DestMedikit ; type_enemi=2;NbTours=0;}
		  
		else if(DestLazer != KPt(-1,-1))		{g_Target=DestLazer ; type_enemi=2; NbTours=0; }
		
		else if(DestBonusJoker != KPt(-1,-1))		{g_Target=DestBonusJoker; type_enemi=2; NbTours=0; }

		else if(DestBinoculars != KPt(-1,-1))		{g_Target=DestBinoculars ; type_enemi=2; NbTours=0; }
		  
		else if(DestMine != KPt(-1,-1))		{g_Target=DestMine ; type_enemi=2; NbTours=0; }

		else if(DestBonusPower != KPt(-1,-1))		{g_Target=DestBonusPower ; type_enemi=2; NbTours=0; }

		else if(DestBonusSpeed != KPt(-1,-1))		{g_Target=DestBonusSpeed ; type_enemi=2; NbTours=0; }
    
		else if(DestBonusVision != KPt(-1,-1))		{g_Target=DestBonusVision ; type_enemi=2; NbTours=0; }
   
		else if(DestBot != KPt(-1,-1))		{g_Target=DestBot ; type_enemi=1; NbTours=0; }

		else if (type_enemi==0 )// tu sais vraiment pas o aller? Va dans un coin, sa te fera voir du pays !
		{	switch (rand() % 4){
				case 0:	g_Target= KPt(0,0) ;break;
				case 1:	g_Target= KPt(g_MapWidth,0) ;break;
				case 2:	g_Target= KPt(0,g_MapHeight) ;break;	
				case 3:	g_Target= KPt(g_MapWidth,g_MapHeight) ;break;  }
			type_enemi=3;
			NbTours=0; 
		}
	}

	// on a une cible faut aller la chercher maintenant

	// mais faut verifier qu'il n'y a pas un enemi dans les parages
	if (DestBot.Distance( BotPosition ) <= 1.01f ) // On m'attaque !!
		{
		// On attaque l'enemi
		KBOTATTACKTARGET target = KBAT_UP;
       		g_Target=DestBot;

			if ( g_Target.x < BotPosition.x )            target = KBAT_LEFT ;
			else if ( g_Target.x > BotPosition.x )   target = KBAT_RIGHT ;
			if ( g_Target.y < BotPosition.y )            target = KBAT_UP ;
			else if ( g_Target.y > BotPosition.y )   target = KBAT_DOWN ;

			g_BotAction.SetAction(KBA_ATTACK) ;
			g_BotAction.SetAttackTarget(target) ;
			last_position=BotPosition;
			type_enemi=0; 
			return ;
		}

	// On regarde si on peu shooter quelqu'un de loin au lazer :)
	for (u32 i=0; i < nInventory ; i++ )
		if (pInventory[i].GetType()==KIT_LASER)  // on regarde si on a le lazer
		{
			if (DestBot.x==BotPosition.x) // on regarde si un bot est pareil que nous en x
				if ( DestBot.Distance( BotPosition ) <= 4.01f ) // on regarde s'il est proche
				{
					KBOTATTACKTARGET target = KBAT_UP;
   					g_Target=DestBot;
						
					if ( g_Target.y < BotPosition.y )            target = KBAT_UP ; // on regarde de quelle cot�il est
					else if ( g_Target.y > BotPosition.y )   target = KBAT_DOWN ;
						
					g_BotAction.SetAction(KBA_ATTACKLASER);
					g_BotAction.SetAttackTarget(target);
					return;
				}

			if (DestBot.y==BotPosition.y) // on regarde si un bot est pareil que nous en y
				if ( DestBot.Distance( BotPosition ) <= 4.01f ) // on regarde s'il est proche
				{
					KBOTATTACKTARGET target = KBAT_UP;
   					g_Target=DestBot;
						
					if ( g_Target.x < BotPosition.x )            target = KBAT_LEFT ; // on regarde de quelle cot�il est
					else if ( g_Target.x > BotPosition.x )   target = KBAT_RIGHT ;
						
					g_BotAction.SetAction(KBA_ATTACKLASER);
					g_BotAction.SetAttackTarget(target);
					return;
				}
		}


	// TODO : prevoir le cas ou je me fait attaquer se loin (en regardant si j'ai perdu de l'energie)

	// Si on est sur un Regenerator, on y reste
	// TODO partir quand on a toute sa vie :)
	// TODO sa marche pas (l'Item a la place du Bot n'est pas comptabilis� -> �ameliorer !
	if(StayHere==true)		{ NbTours=0; g_BotAction.SetAction( KBA_NOTHING ); return ;}



	if(g_Target != KPt(-1,-1))
	{
		if (g_Target.Distance( BotPosition ) <= 1.01f )
		{
			// Bot en vue => A L'ATTAQUE !!!!!!!!!
			if(g_Target == DestBot)
			{
				// On attaque l'enemi, mais normalement, il le fait avant (en gros, ces lignes ne servent plus a rien !)
				KBOTATTACKTARGET target = KBAT_UP;
                
				if ( g_Target.x < BotPosition.x )            target = KBAT_LEFT ;
				else if ( g_Target.x > BotPosition.x )   target = KBAT_RIGHT ;
				if ( g_Target.y < BotPosition.y )            target = KBAT_UP ;
				else if ( g_Target.y > BotPosition.y )   target = KBAT_DOWN ;

				g_BotAction.SetAction(KBA_ATTACK) ;
				g_BotAction.SetAttackTarget(target) ;
				last_position=BotPosition;
				type_enemi=0;
				return ;
			}
			
			// Super : un Item :)
			else if( type_enemi==2 )
			{
					// On ramasse l'item
					if ( g_Target.x < BotPosition.x )            g_ModeDir = KBMD_LEFT ;
						else if ( g_Target.x > BotPosition.x )   g_ModeDir = KBMD_RIGHT ;
					if ( g_Target.y < BotPosition.y )            g_ModeDir = KBMD_UP ;
						else if ( g_Target.y > BotPosition.y )   g_ModeDir = KBMD_DOWN ;
                
					g_BotAction.SetAction(KBA_MOVE) ;
					g_BotAction.SetMoveDir(g_ModeDir) ;

					if (g_Target==DestRegenerator)
						StayHere=true;

					type_enemi=0;
					last_position=BotPosition;
					return ;
			}
			
			// T au coin ? attend le prochain tour !
			else if( type_enemi==3 ) 
			{
					type_enemi=0;
					g_BotAction.SetAction( KBA_NOTHING );
					return ;
			}

		}
	
	}
	else // si le bot n'a pas de cible
		MoveRandom( BotPosition );


	// On va a la position souhait� (en evitant les murs)
	if ( g_Target.x < BotPosition.x && CanMove( BotPosition.x-1 , BotPosition.y) )      
		g_ModeDir = KBMD_LEFT ;
	else if ( g_Target.x > BotPosition.x  && CanMove(BotPosition.x+1 ,BotPosition.y) )
		g_ModeDir = KBMD_RIGHT ;
	if ( g_Target.y < BotPosition.y  && CanMove(BotPosition.x,BotPosition.y-1 ) )
		g_ModeDir = KBMD_UP ;
	else if ( g_Target.y > BotPosition.y  && CanMove(BotPosition.x,BotPosition.y+1 ) )
		g_ModeDir = KBMD_DOWN ;
       
	g_BotAction.SetAction(KBA_MOVE) ;
	g_BotAction.SetMoveDir(g_ModeDir) ;


	// On va eviter de rester trop au meme endroi
	if(BotPosition==last_position)
	{
//		if( type_enemi==3 )
//			type_enemi=0; // comme sa, il recherchera un item ou un coin au prochain tour
		switch (rand() % 4)
		{
			case 0:	g_ModeDir = KBMD_LEFT;break;
			case 1:	g_ModeDir = KBMD_RIGHT;break;
			case 2:	g_ModeDir = KBMD_UP;break;	
			case 3:	g_ModeDir = KBMD_DOWN;break;
		}
		g_BotAction.SetAction( KBA_MOVE );		
		g_BotAction.SetMoveDir( g_ModeDir );
		
		//MoveRandom(pBot);  // quand je met la fonction al�toir de deplacement, il ne bouge plus ( et je ne comprend pas pourquoi!)
	}
	last_position=BotPosition;
} 
// fin de FindTactique


// Fonction CanMove 
// Permet de savoir si on peu aller sur une case ou non
bool CanMove(s32 X,s32 Y) // pour savoir si on peu bouger ,mais je sais pas comment faire :( 
{
	KPt Tmp(X,Y);
	if( GetGlobalMapCellType(Tmp)!=0 )
		return true;
	return false;
// TODO : rajouter le cas ou il y ai une mine ou un cadavre

}  // je pense mettre la fonction en inligne 


// Fonction MoveRandom
// Permet de faire bouger aleatoirement le robot (si possible en evitant les murs)
void MoveRandom(KPt BotPosition)
{
	switch (rand() % 4)
		{
			case 0:	if ( CanMove( BotPosition.x , BotPosition.y) )
						{g_ModeDir = KBMD_LEFT;break;}

			case 1:	if ( CanMove( BotPosition.x , BotPosition.y) )
						{g_ModeDir = KBMD_RIGHT;break;}

			case 2:	if ( CanMove( BotPosition.x , BotPosition.y) )
						{g_ModeDir = KBMD_UP;break;}

			case 3:	if ( CanMove( BotPosition.x , BotPosition.y) )
						{g_ModeDir = KBMD_DOWN;break;}
			default : switch (rand() % 20) // Si le(s) mouvement(s) sont impossible, on retente !
						{
							case 0 : g_BotAction.SetAction( KBA_NOTHING ); break; // j'evite le probleme de boucle infini (a revoir)
							default : MoveRandom( BotPosition );break; // probleme si l'on es conc�: boucle infini
						}	
		}

}

/**************Ameliorations Prevu****************************
Ne pas chercher d'objet si on a deja le maximum
Contourner les murs
Utiliser le lazer et les mines
Attaquer l'enemi que si si on a des chances de gagner 
Eviter les mines (pour qd les autres bots les gerreront)
Eviter les tirs de lazer (idem)
Afficher en debug les actions en cours
Essayer de faire s'entretuer les autres (gnac gnac gnac)
Se cacher et attaquer �distance quand on a plus de vie
Choisir les bons item (selon l'environement du robot)
Gerer le cas o le bot est conc�Mettre en ordre et optimiser mon code et l'optimiser
*************************************************************/
