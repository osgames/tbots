//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: LechStats.h
//	Author			: - Aitonfrere	
//					  - Dino			dino@zythum-project.com
//
//	Date			: 03/08/2003
//	Modification	:
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------

#ifndef __LECHSTATS_H
#define __LECHSTATS_H


////////////////////////////////////////////////////////////////////////////////
// 								CLASS LechBotStats
////////////////////////////////////////////////////////////////////////////////
class LechBotStats
{
private :
#ifdef _WIN32
	s64			m_Time;
	s64			m_Frequency;
#endif // _WIN32
public :

	s64			m_TurnDurationSum;		// Dur� d'un tour de LechBot
	s32			m_TurnCount;			// Nombre de tours ex�ut�
	s32			m_PathLength;			// Longueur du Path en cours
	bool		m_bRandomTarget;		// La cible courante a-t-elle ��choisie al�toirement ?

	LechBotStats()
	{ 
		m_TurnDurationSum	= 0;
		m_TurnCount			= 0;
		m_PathLength		= 0;
		m_bRandomTarget		= false;
		
#ifdef _WIN32
		QueryPerformanceFrequency((LARGE_INTEGER *) &m_Frequency);
#endif // _WIN32
	}

	inline s64 GetTime()
	{
#ifdef _WIN32
		QueryPerformanceCounter((LARGE_INTEGER *) &m_Time);
		return m_Time;
#endif // _WIN32
#ifdef _LINUX
		// DINO : TODO
		return 0;
#endif // _LINUX
	}

	inline s64 GetTurnDurationAVG()	{ return m_TurnDurationSum / (m_TurnCount?m_TurnCount:1); }
};



#endif	// __LECHSTATS_H
