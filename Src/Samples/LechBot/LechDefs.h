//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: LechDefs.h
//	Author			: Aitonfrere	
//
//	Date			: 03/08/2003
//	Modification	:
//
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------


#ifndef __LECHDEFS_H
#define __LECHDEFS_H

////////////////////////////////////////////////////////////////////////////////
// Defines & Enumerations
////////////////////////////////////////////////////////////////////////////////
#define INVALID_POSITION			KPt(-1,-1)				// Position d�finie comme incorrecte

#define PATH_LENGTH_THRESHOLD		50						// Longueur limite � ne pas d�passer pour un chemin
#define PATH_MAX_COST				9


#endif	// __LECHDEFS_H
