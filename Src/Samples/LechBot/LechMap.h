//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: LechMap.h
//	Author			: Aitonfrere	
//
//	Date			: 03/08/2003
//	Modification	:
//
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------

#ifndef __LECHMAP_H
#define __LECHMAP_H



////////////////////////////////////////////////////////////////////////////////
// 								CLASS LechBotCell
////////////////////////////////////////////////////////////////////////////////
class LechBotCell
{
public :

	CellType	m_Type;				// Type d'une cellule
	u8			m_Cost;				// Sert ��aluer les chemins �suivre
	u8			m_Memory;			// Compteur de passage sur une cellule

	LechBotCell()
	{
		m_Type		= UNKNOWN_CELL_TYPE;
		m_Cost		= (u8)-1;
		m_Memory	= 0;
	}
};


////////////////////////////////////////////////////////////////////////////////
// 								CLASS LechMap
////////////////////////////////////////////////////////////////////////////////
class LechMap
{
public :

	// Map information

	LechBotCell			*m_pMapCell;
	s32					m_MapWidth;
	s32					m_MapHeight;

	LechMap(s32 _MapWidth, s32 _MapHeight)
	{
		m_pMapCell		= new LechBotCell[_MapWidth * _MapHeight];
		m_MapWidth		= _MapWidth;
		m_MapHeight		= _MapHeight;
	}

	~LechMap()									{ SafeDeleteArray(m_pMapCell); }

	LechBotCell *operator ()(s32 _x, s32 _y)	{ return &m_pMapCell[_y * m_MapWidth + _x]; }


	// Retourner le cot d'une cellule de la map

	inline CellType GetCost(KPt &_Position)
	{
		if ( (_Position.x < 0) || (_Position.x >= m_MapWidth) || (_Position.y < 0) || (_Position.y >= m_MapHeight) )
			return PATH_MAX_COST;

		return m_pMapCell[_Position.y * m_MapWidth + _Position.x].m_Cost;
	}

	inline CellType GetCost(s32 _x, s32 _y)
	{
		if ( (_x < 0) || (_x >= m_MapWidth) || (_y < 0) || (_y >= m_MapHeight) )
			return PATH_MAX_COST;

		return m_pMapCell[_y * m_MapWidth + _x].m_Cost;
	}
};

#endif	// __LECHMAP_H
