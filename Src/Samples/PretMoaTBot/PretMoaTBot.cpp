//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: PretMoaTBot.cpp
//	Author			: - Macload
//	Date			: 17/07/2003
//	Modification	: 24/03/2004
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------
#include "PretMoaTBot.h"
#include "Pathfinder.h"
#include <time.h>
#include "Richedit.h"

#define MAP_WALL 20
#define MAP_TEMP_WALL 30
#define NB_TOUR_MAP_TEMP_WALL_REST 40
#define BOT_NAME_LONG1 "PretMoaTBot : Tu me les prete sinon je te tape"
#define BOT_NAME_LONG2 "PretMoaTBot : Tu me les prete sinon je te bute"
#define BOT_NAME_LONG3 "PretMoaTBot : Tu me les prete sinon je te massacre"
#define BOT_NAME_LONG4 "PretMoaTBot : Tu me les prete sinon je t'explose"
#define BOT_NAME_LONG5 "PretMoaTBot : Tu me les prete sinon je vais le dire a ta mere"
#define BOT_NAME_SHORT "PretMoaTBot"

////////////////////////////////////////////////////////////////////////////////
// Globals
////////////////////////////////////////////////////////////////////////////////

KBotInfo    g_BotInfo ;
HINSTANCE	g_hInstance ;
HWND		g_hWndMainWindow ;
HWND		g_hWndDebugWindow ;
HWND		g_hWndEdit ;
HMODULE		g_hModuleRichEdit ;

////////////////////////////////////////////////////////////////////////////////
// DllMain
//
////////////////////////////////////////////////////////////////////////////////
#ifdef _WIN32
BOOL APIENTRY DllMain( HANDLE hModule, DWORD  ul_reason_for_call,  LPVOID lpReserved )
{
    switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
		case DLL_THREAD_ATTACH:
		case DLL_THREAD_DETACH:
		case DLL_PROCESS_DETACH:
			break;
    }
    return TRUE;
}
#endif // _WIN32

//==============================================================================
//
//							  T-Bots API Functions
//
//==============================================================================

////////////////////////////////////////////////////////////////////////////////
// GetpBotName
//
////////////////////////////////////////////////////////////////////////////////
PRETMOATBOT_API char* GetpBotName()
{
	// Nom long du Robot....Bon ok je me la pete....et alors!!

    srand((unsigned)time(NULL)) ;    
    int iBotName = rand() % 100 ;

		 if(iBotName < 20) return BOT_NAME_LONG1 ;
    else if(iBotName < 40) return BOT_NAME_LONG2 ;
    else if(iBotName < 60) return BOT_NAME_LONG3 ;
    else if(iBotName < 80) return BOT_NAME_LONG4 ;
    else return BOT_NAME_LONG5 ;
}

////////////////////////////////////////////////////////////////////////////////
// GetpInfos
//
////////////////////////////////////////////////////////////////////////////////
PRETMOATBOT_API KBotInfo* GetpInfos( void* pContext )
{
    PretMoaTBot *pPretMoaTBot = (PretMoaTBot*)pContext ;

	g_BotInfo.SetpBotName(pPretMoaTBot->m_pBotName) ;
    g_BotInfo.SetPoints(pPretMoaTBot->m_iVitality,pPretMoaTBot->m_iPower,pPretMoaTBot->m_iSpeed,pPretMoaTBot->m_iVision) ;
    
	return &g_BotInfo;
}

////////////////////////////////////////////////////////////////////////////////
// StartGame
//
////////////////////////////////////////////////////////////////////////////////
PRETMOATBOT_API void* StartGame( s32 MapWidth, s32 MapHeight )
{
    PretMoaTBot *pPretMoaTbot	= new PretMoaTBot(MapWidth,MapHeight) ;

//  DEBUT PARAMETRES PERSONNELS
    
	strcpy(pPretMoaTbot->m_pBotName,"PretMoaTBot") ;

    pPretMoaTbot->m_iVitality				= 5 ;
	pPretMoaTbot->m_iPower					= 2 ;
	pPretMoaTbot->m_iSpeed					= 4 ;
	pPretMoaTbot->m_iVision					= 4 ;

	pPretMoaTbot->m_iMinLifeBeforeEscape	= 160 ;

	pPretMoaTbot->m_iNbTurnsTempWallRest	= 20 ;
	pPretMoaTbot->m_iNbTurnsMinePositionRest= 60 ;

	pPretMoaTbot->m_tabPreferenceGOTO[0]	= KIT_REGENERATOR ;
	pPretMoaTbot->m_tabPreferenceGOTO[1]	= KIT_MEDIKIT ;
	pPretMoaTbot->m_tabPreferenceGOTO[2]	= KIT_LASER ;
	pPretMoaTbot->m_tabPreferenceGOTO[3]	= KIT_BINOCULARS ;
	pPretMoaTbot->m_tabPreferenceGOTO[4]	= KIT_BOT ;
	pPretMoaTbot->m_tabPreferenceGOTO[5]	= KIT_BONUSPOWER ;
	pPretMoaTbot->m_tabPreferenceGOTO[6]	= KIT_MINE ;
	pPretMoaTbot->m_tabPreferenceGOTO[7]	= KIT_BONUSJOKER ;
	pPretMoaTbot->m_tabPreferenceGOTO[8]	= KIT_BONUSSPEED ;
	pPretMoaTbot->m_tabPreferenceGOTO[9]	= KIT_BONUSVISION ;

	pPretMoaTbot->m_tabPreferenceUselessItems[0] = KIT_BONUSVISION ;
	pPretMoaTbot->m_tabPreferenceUselessItems[1] = KIT_BONUSSPEED ;
	pPretMoaTbot->m_tabPreferenceUselessItems[2] = KIT_BONUSJOKER ;
	pPretMoaTbot->m_tabPreferenceUselessItems[3] = KIT_NONE ;
	
//  FIN PARAMETRES PERSONNELS
	
	return pPretMoaTbot ;
}

////////////////////////////////////////////////////////////////////////////////
// EndGame
//
////////////////////////////////////////////////////////////////////////////////
PRETMOATBOT_API void EndGame( void* pContext )
{
    PretMoaTBot *pPretMoaTBot = (PretMoaTBot*)pContext ;

	SafeDelete(pPretMoaTBot) ;
	#if _DEBUG
		#if _WIN32
			DestroyWindow(g_hWndDebugWindow) ;
			UnregisterClass("PretMoaTBot",g_hInstance) ;
		#endif _WIN32
	#endif _DEBUG
}

////////////////////////////////////////////////////////////////////////////////
// DebugDisplay
//
////////////////////////////////////////////////////////////////////////////////
LRESULT CALLBACK WindowProc( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam ) ;
void DebugConsoleAddText(char *pText) ;
void DebugConsoleClear(void) ;


PRETMOATBOT_API void DebugDisplay(HINSTANCE hInstance,HWND hWnd)
{
	g_hInstance = hInstance ;
	g_hWndMainWindow = hWnd ;

	#if _DEBUG
		#if _WIN32


			WNDCLASS	WndClass;

			memset( &WndClass, 0, sizeof( WndClass ) );
			WndClass.style			= CS_HREDRAW | CS_VREDRAW;
			WndClass.lpfnWndProc	= WindowProc;
			WndClass.hInstance		= hInstance;
			WndClass.hIcon			= NULL;
			WndClass.hCursor		= NULL;
			WndClass.hbrBackground	= 0;
			WndClass.lpszMenuName	= NULL;
			WndClass.lpszClassName	= "PretMoaTBot";

			if( !RegisterClass( &WndClass ) )
				return ;

			DWORD	Style = WS_SIZEBOX | WS_MINIMIZEBOX;
			g_hWndDebugWindow = CreateWindowEx(	WS_EX_APPWINDOW,
										"PretMoaTBot",
										"PretMoaTBot Debug Window",
										Style,
										850,
										5,
										300,
										500,
										NULL,
										NULL,
										g_hInstance,
										NULL );

			if ( !g_hWndDebugWindow )
				return ;

			ShowWindow( g_hWndDebugWindow, SW_NORMAL );
			UpdateWindow( g_hWndDebugWindow );

			//
			// Creation de l'édit
			//
			g_hModuleRichEdit = LoadLibrary( "RICHED20.DLL" );
			if( !g_hModuleRichEdit )
				return ;

			g_hWndEdit = CreateWindowEx(	WS_EX_STATICEDGE,
										"RichEdit20W",
										NULL,
										WS_CHILD |
										WS_HSCROLL | WS_VSCROLL | ES_AUTOHSCROLL | ES_AUTOVSCROLL | ES_MULTILINE | ES_READONLY,
										0,
										0,
										300,
										500,
										g_hWndDebugWindow,
										NULL,
										g_hInstance,
										NULL );

			if ( !g_hWndEdit )
				return ;

			ShowWindow( g_hWndEdit, SW_SHOWMAXIMIZED );

		#endif _WIN32
	#endif _DEBUG
}

LRESULT CALLBACK WindowProc( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
	return DefWindowProc( hWnd, uMsg, wParam, lParam);
}

void DebugConsoleAddText(char *pText)
{
	#if _DEBUG
		#if _WIN32

			if(pText)
			{
				CHARFORMAT	Format;
				memset( &Format, 0, sizeof( Format ) );
				Format.cbSize		= sizeof( Format );
				Format.dwMask		= CFM_COLOR | CFM_SIZE | CFM_FACE;
				Format.yHeight		= 200;
				strcpy( Format.szFaceName, "Trebuchet MS" );
				Format.crTextColor	= RGB( 0, 0, 0 );
				SendMessage( g_hWndEdit, EM_SETCHARFORMAT, (WPARAM)SCF_SELECTION, (LPARAM)&Format );
				SendMessage( g_hWndEdit, EM_REPLACESEL, (WPARAM)FALSE, (LPARAM)pText);
				SendMessage( g_hWndEdit, EM_REPLACESEL, (WPARAM)FALSE, (LPARAM)"\n");
			}
		#endif _WIN32
	#endif _DEBUG
}
void DebugConsoleClear(void)
{
	#if _DEBUG
		#if _WIN32
			SendMessage( g_hWndEdit, WM_SETTEXT, (WPARAM)FALSE, (LPARAM)"");
		#endif _WIN32
	#endif _DEBUG
}
////////////////////////////////////////////////////////////////////////////////
// GetpBotAction
//
////////////////////////////////////////////////////////////////////////////////
PRETMOATBOT_API KBotAction* GetpBotAction( KBotActionInfo* pBotActionInfo, void* pContext )
{
    PretMoaTBot *pPretMoaTBot = (PretMoaTBot*)pContext ;

    return pPretMoaTBot->Manage(pBotActionInfo) ;
}


//==============================================================================
//
//								Misc. Functions
//
//==============================================================================
/**
 **************************************************************************************************************************************************************
 * <Detailed description of the PretMoaTBot method>
 **************************************************************************************************************************************************************
 * @author : Mac Load
 * @date   : 02/08/2003
 * @param  : s32 MapWidth, s32 MapHeight
 * @return : 
 **************************************************************************************************************************************************************/
PretMoaTBot::PretMoaTBot(s32 MapWidth, s32 MapHeight)
{
    m_MapWidth	                = MapWidth ;
    m_MapHeight                 = MapHeight ;
    m_pMapCell                  = NULL;
    m_PathFinder                = NULL ;
	m_iCptTour					= 0 ;
    m_BotMode.bGoto             = BOT_GOTO_NOTHING ;
    DestBonusRegeneratorGLOBAL  = KPt(-1,-1) ;
    m_LastPathFinderDest        = KPt(-1,-1) ;
	m_pBotName					= new char[256] ;
	m_pTabItems					= new vector<KPt>[KIT_COUNT] ;
 
    m_BotAction.SetAction( KBA_NOTHING ) ;
    
    InitMap() ;
}
/**
 **************************************************************************************************************************************************************
 * <Detailed description of the ~PretMoaTBot method>
 **************************************************************************************************************************************************************
 * @author : Mac Load
 * @date   : 02/08/2003
 * @param  :
 * @return : 
 **************************************************************************************************************************************************************/
PretMoaTBot::~PretMoaTBot()
{
	SafeDeleteArray(m_pMapCell);	// Dino, the memory free-er :)
    SafeDelete(m_PathFinder) ;
	SafeDelete(m_pBotName) ;

	if(m_pTabItems)
	{
		for(u32 uCpt = 0 ; uCpt <  KIT_COUNT ; uCpt ++)
			m_pTabItems[uCpt].clear() ;
		delete[] m_pTabItems ;
	}
}

/**
 **************************************************************************************************************************************************************
 * <Detailed description of the DebugDisplay method>
 **************************************************************************************************************************************************************
 * @author : Mac Load
 * @date   : 03/08/2003
 * @param  : KRender *pRender
 * @return : 
 **************************************************************************************************************************************************************/
/*void PretMoaTBot::DebugDisplay(KRender *pRender)
{
    if(m_PathFinder == NULL)
        return ;
    
    for( s32 i = 1 ; i < m_PathFinder->m_FinalCellsList.size() ; i ++ )
    {
        int iCell = m_PathFinder->m_FinalCellsList[i] ;
        sCell cellule = m_PathFinder->GetCell(iCell) ;

        pRender->DrawQuad( SCREEN_OFFSETX + cellule.iPosX * CELL_SIZE, cellule.iPosY * CELL_SIZE, CELL_SIZE, CELL_SIZE, 0, KRGBA( 255, 0, 255, 64 )) ;
    }
    pRender->DrawQuad( SCREEN_OFFSETX + m_PathFinder->m_iEndPosX * CELL_SIZE, m_PathFinder->m_iEndPosY * CELL_SIZE, CELL_SIZE, CELL_SIZE, 0, KRGBA( 255, 0, 255, 64 )) ;
}
*/
/**
 **************************************************************************************************************************************************************
 * <Detailed description of the Manage method>
 **************************************************************************************************************************************************************
 * @author : Mac Load
 * @date   : 02/08/2003
 * @param  : KBotActionInfo* pBotActionInfo
 * @return : KBotAction*
 **************************************************************************************************************************************************************/
KBotAction *PretMoaTBot::Manage(KBotActionInfo* pBotActionInfo)
{
	DebugConsoleClear() ;

	m_iCptTour ++ ;
	
	// AFFICHAGE DES INFOS DE DEBUG
	char cTemp[100] = "" ;
	DebugConsoleAddText("--INFO JEU--") ;
	u32 uWallTempToDelete = MAP_TEMP_WALL + m_iCptTour % m_iNbTurnsTempWallRest ;
	sprintf (cTemp,"Tour : %i",m_iCptTour) ; DebugConsoleAddText(cTemp) ;
	sprintf (cTemp,"Valeur des murs temporaires : %i",uWallTempToDelete) ; DebugConsoleAddText(cTemp) ;
	DebugConsoleAddText("") ;
	// FIN -- AFFICHAGE DES INFOS DE DEBUG

	// On vire tous les murs temporaires
	RemoveAllTempWall() ;

    m_pBotActionInfo = pBotActionInfo ;

    m_BotAction.SetAction( KBA_NOTHING ) ;

    // Update de la map
	UpdateMap(pBotActionInfo->m_pMapCell) ;

	UpdateItems(pBotActionInfo->m_pMapCell,pBotActionInfo->m_Pos,pBotActionInfo->m_pItems,pBotActionInfo->m_nItems) ;

    // Recherche des solutions
    SearchSolutions(pBotActionInfo->m_Pos,pBotActionInfo->m_pItems,pBotActionInfo->m_nItems) ;    
    KPt Dest = m_BotMode.destination ;

    if(IsPathFinderMustBeClear())
    {
        SafeDelete(m_PathFinder) ;
    }

    if(m_BotMode.bGoto == BOT_GOTO_NOTHING)
    {
        m_BotAction.SetAction( KBA_NOTHING ) ;
	    return &m_BotAction ;
    }
    else if(Dest != KPt(-1,-1))
    {
		if((m_BotMode.bGoto == BOT_GOTO_ITEM) && (m_pBotActionInfo->m_nInventory == BOT_MAX_CHILDS) && HaveAnItemToDrop())
		{
			KITEMTYPE itemToDrop = GetItemToDrop() ;
			m_BotAction.SetAction(KBA_DROP) ;
            m_BotAction.SetDropItemType(itemToDrop) ;
		}
		// On regarde si on peut le shooter
        else if(CanShoot())
        {
            m_BotAction.SetAction(KBA_ATTACKLASER) ;
            m_BotAction.SetAttackTarget(m_BotMode.direction) ;
        }
        // Si on est à une case du but
        else if (Dest.Distance( pBotActionInfo->m_Pos ) <= 1.01f )
		{
            if(m_BotMode.bGoto == BOT_GOTO_BOT)
            {
                // On attaque l'enemi a bras nu
                KBOTATTACKTARGET target = KBAT_UP;
            
                if      (Dest.x < pBotActionInfo->m_Pos.x)  target = KBAT_LEFT ;
                else if (Dest.x > pBotActionInfo->m_Pos.x)  target = KBAT_RIGHT ;
                if      (Dest.y < pBotActionInfo->m_Pos.y)  target = KBAT_UP ;
			    else if (Dest.y > pBotActionInfo->m_Pos.y)  target = KBAT_DOWN ;

                m_BotAction.SetAction(KBA_ATTACK) ;
			    m_BotAction.SetAttackTarget(target) ;

            }
            else if((m_BotMode.bGoto == BOT_GOTO_ITEM) || 
                    (m_BotMode.bGoto == BOT_GOTO_ESCAPE) ||
                    (m_BotMode.bGoto == BOT_GOTO_POS))

            {
                // On ramasse l'item
                KBOTMOVEDIR direction = KBMD_UP;
                if      (Dest.x < pBotActionInfo->m_Pos.x)  direction = KBMD_LEFT ;
                else if (Dest.x > pBotActionInfo->m_Pos.x)  direction = KBMD_RIGHT ;
                if      (Dest.y < pBotActionInfo->m_Pos.y)  direction = KBMD_UP ;
                else if (Dest.y > pBotActionInfo->m_Pos.y)  direction = KBMD_DOWN ;
                
                if((Dest.x == pBotActionInfo->m_Pos.x) && (Dest.y == pBotActionInfo->m_Pos.y))
                    m_BotAction.SetAction( KBA_NOTHING ) ;
                else
                {
                    m_BotAction.SetAction(KBA_MOVE) ;
                    m_BotAction.SetMoveDir(direction) ;
                }               

                m_BotMode.bGoto = BOT_GOTO_NOTHING ;                
            }
        }
        // Si on est à plus d'une case du but
        else
        {   
            // Recherche d'un chemin pour se diriger vers la case de destination
            bool bresPath = true ;
            if(m_PathFinder == NULL)
            {
                m_LastPathFinderDest = m_BotMode.destination ;
                m_PathFinder = new CPathFinder(
                                                m_pMapCell,
                                                m_MapWidth,
                                                m_MapHeight,
                                                pBotActionInfo->m_Pos.x,
                                                pBotActionInfo->m_Pos.y,
                                                m_BotMode.destination.x,
                                                m_BotMode.destination.y
                                                ) ;
                bresPath = m_PathFinder->ComputePath() ;
            }
        
            if(bresPath)
            {
                // On regarde si on a rien a droper
                int iDropMine = rand() % 100 ;
                if ((HaveAMine() && (iDropMine < 50)))
                {
                    m_BotAction.SetAction(KBA_DROP) ;
                    m_BotAction.SetDropItemType(KIT_MINE) ;
                    // On marque sur la map comme quoi il y a un mur temporaire
					AddTempWall(pBotActionInfo->m_Pos,false) ;
	            }
				else
                {
                    int iNextCell = m_PathFinder->GetNextCell() ;
                    sCell cellule = m_PathFinder->GetCell(iNextCell) ;
                    
                    KBOTMOVEDIR direction = KBMD_UP;
                    if      (cellule.iPosX < pBotActionInfo->m_Pos.x)   direction = KBMD_LEFT ;
                    else if (cellule.iPosX > pBotActionInfo->m_Pos.x)   direction = KBMD_RIGHT ;
                    if      (cellule.iPosY < pBotActionInfo->m_Pos.y)   direction = KBMD_UP ;
                    else if (cellule.iPosY > pBotActionInfo->m_Pos.y)   direction = KBMD_DOWN ;

                    m_BotAction.SetAction(KBA_MOVE) ;
                    m_BotAction.SetMoveDir(direction) ;
                }                    
            }

            // Si on a pas bougé depuis 2 coups alors on efface 
            // le pathfinder pour lancer une nouvelle recherche au prochain coup
            if(m_LastPos == pBotActionInfo->m_Pos)
            {
                m_BotMode.bGoto = BOT_GOTO_NOTHING ;
                SafeDelete(m_PathFinder) ;
            }

            m_LastPos = pBotActionInfo->m_Pos ;        
        }
    }

    return &m_BotAction ;
}

/**
 **************************************************************************************************************************************************************
 * <Detailed description of the InitMap method>
 **************************************************************************************************************************************************************
 * @author : Mac Load
 * @date   : 25/07/2003
 * @param  :
 * @return : 
 **************************************************************************************************************************************************************/
 void PretMoaTBot::InitMap()
{
     // Creation de la map qui permettra au robot de connaitre la map
     m_pMapCell = new KCell[m_MapWidth * m_MapHeight] ;
     memset(m_pMapCell, UNKNOWN_CELL_TYPE, m_MapWidth * m_MapHeight * sizeof( KCell ) ) ;
     

    // On met des murs tout autours de la map
    for( s32 my = 0; my < m_MapHeight; my ++ )
    {
        for( s32 mx = 0; mx < m_MapWidth; mx ++ ) 
        {
            if((mx == 0) || (my == 0) || (mx == (m_MapWidth - 1)) ||(my == (m_MapHeight - 1)))
            {
                SetGlobalMapCellType( KPt(mx,my), MAP_WALL) ;
            }
        }
    }
}

/**
 **************************************************************************************************************************************************************
 * <Detailed description of the UpdateMap method>
 **************************************************************************************************************************************************************
 * @author : Mac Load
 * @date   : 22/07/2003
 * @param  : KCell* pMapCell
 * @return : 
 **************************************************************************************************************************************************************/
void PretMoaTBot::UpdateMap(KCell* pMapCell)
{
    for( s32 my = 0; my < m_MapHeight; my ++ )
    {
        for( s32 mx = 0; mx < m_MapWidth; mx ++ ) 
        {
            KCell* pCell = &pMapCell[my * m_MapWidth + mx];
            KCell* pGlobalCell = &m_pMapCell[my * m_MapWidth + mx];

            if((pCell->GetType() != UNKNOWN_CELL_TYPE) && (pGlobalCell->GetType() != MAP_WALL) && ((pGlobalCell->GetType() < MAP_TEMP_WALL) || (pGlobalCell->GetType() > ( MAP_TEMP_WALL + m_iNbTurnsTempWallRest + m_iNbTurnsMinePositionRest))))
            {
                SetGlobalMapCellType( KPt(mx,my), pCell->GetType() );
            }
        }
	}
}


/**
 **************************************************************************************************************************************************************
 * <Detailed description of the SearchSolutions method>
 **************************************************************************************************************************************************************
 * @author : Mac Load
 * @date   : 22/07/2003
 * @param  : KPt BotPosition, KItemInfo* pItems, u32 nItems
 * @return : 
 **************************************************************************************************************************************************************/
void PretMoaTBot::SearchSolutions(KPt BotPosition, KItemInfo* pItems, u32 nItems)
{
    // RECHERCHE DES ITEMS LES PLUS PROCHE

	m_pTabDest[KIT_BOT]			= GetNearestValidItem(BotPosition, KIT_BOT);
	m_pTabDest[KIT_BONUSVISION] = GetNearestValidItem(BotPosition, KIT_BONUSVISION);
	m_pTabDest[KIT_BONUSPOWER]	= GetNearestValidItem(BotPosition, KIT_BONUSPOWER);
	m_pTabDest[KIT_BONUSSPEED]	= GetNearestValidItem(BotPosition, KIT_BONUSSPEED);
	m_pTabDest[KIT_BONUSJOKER]	= GetNearestValidItem(BotPosition, KIT_BONUSJOKER);
	m_pTabDest[KIT_REGENERATOR] = GetNearestValidItem(BotPosition, KIT_REGENERATOR);
	m_pTabDest[KIT_MEDIKIT]		= GetNearestValidItem(BotPosition, KIT_MEDIKIT);
	m_pTabDest[KIT_BINOCULARS]	= GetNearestValidItem(BotPosition, KIT_BINOCULARS);
	m_pTabDest[KIT_MINE]		= GetNearestValidItem(BotPosition, KIT_MINE);
	m_pTabDest[KIT_LASER]		= GetNearestValidItem(BotPosition, KIT_LASER);

// AFFICHAGE DES INFOS DE DEBUG
	char cTemp[100] = "" ;
	DebugConsoleAddText("--MODE COURANT--") ;
	if(m_BotMode.bGoto == BOT_GOTO_ITEM) DebugConsoleAddText("BOT_GOTO_ITEM") ;
	if(m_BotMode.bGoto == BOT_GOTO_BOT) DebugConsoleAddText("BOT_GOTO_BOT") ;
	if(m_BotMode.bGoto == BOT_GOTO_POS) DebugConsoleAddText("BOT_GOTO_POS") ;
	if(m_BotMode.bGoto == BOT_GOTO_ESCAPE) DebugConsoleAddText("BOT_GOTO_ESCAPE") ;
	if(m_BotMode.bGoto == BOT_GOTO_NOTHING) DebugConsoleAddText("BOT_GOTO_NOTHING") ;
	DebugConsoleAddText("") ;
	DebugConsoleAddText("--POSITION--") ;
	sprintf (cTemp,"(%i ,%i )",BotPosition.x,BotPosition.y) ; DebugConsoleAddText(cTemp) ;
	DebugConsoleAddText("") ;
	DebugConsoleAddText("--DESTINATION--") ;
	sprintf (cTemp,"(%i ,%i )",m_BotMode.destination.x,m_BotMode.destination.y) ; DebugConsoleAddText(cTemp) ;
	DebugConsoleAddText("") ;
	DebugConsoleAddText("--ITEMS LES PLUS PROCHES--") ;
	sprintf (cTemp,"Bot :\t\t(%i ,%i )",m_pTabDest[KIT_BOT].x,m_pTabDest[KIT_BOT].y) ; DebugConsoleAddText(cTemp) ;
	sprintf (cTemp,"Regenerator :\t(%i ,%i )",m_pTabDest[KIT_REGENERATOR].x,m_pTabDest[KIT_REGENERATOR].y) ; DebugConsoleAddText(cTemp) ;
	sprintf (cTemp,"Medikit :\t(%i ,%i )",m_pTabDest[KIT_MEDIKIT].x,m_pTabDest[KIT_MEDIKIT].y) ; DebugConsoleAddText(cTemp) ;
	sprintf (cTemp,"Laser :\t\t(%i ,%i )",m_pTabDest[KIT_LASER].x,m_pTabDest[KIT_LASER].y) ; DebugConsoleAddText(cTemp) ;
	sprintf (cTemp,"Binoculars :\t(%i ,%i )",m_pTabDest[KIT_BINOCULARS].x,m_pTabDest[KIT_BINOCULARS].y) ; DebugConsoleAddText(cTemp) ;
	sprintf (cTemp,"Mine :\t\t(%i ,%i )",m_pTabDest[KIT_MINE].x,m_pTabDest[KIT_MINE].y) ; DebugConsoleAddText(cTemp) ;
	sprintf (cTemp,"Power :\t\t(%i ,%i )",m_pTabDest[KIT_BONUSPOWER].x,m_pTabDest[KIT_BONUSPOWER].y) ; DebugConsoleAddText(cTemp) ;
	sprintf (cTemp,"Vision :\t\t(%i ,%i )",m_pTabDest[KIT_BONUSVISION].x,m_pTabDest[KIT_BONUSVISION].y) ; DebugConsoleAddText(cTemp) ;
	sprintf (cTemp,"Speed :\t\t(%i ,%i )",m_pTabDest[KIT_BONUSSPEED].x,m_pTabDest[KIT_BONUSSPEED].y) ; DebugConsoleAddText(cTemp) ;
	sprintf (cTemp,"Joker :\t\t(%i ,%i )",m_pTabDest[KIT_BONUSJOKER].x,m_pTabDest[KIT_BONUSJOKER].y) ; DebugConsoleAddText(cTemp) ;
// FIN -- AFFICHAGE DES INFOS DE DEBUG

    if(m_pTabDest[KIT_REGENERATOR] != KPt(-1,-1)) 
		DestBonusRegeneratorGLOBAL = m_pTabDest[KIT_REGENERATOR] ;

    // Si on est a moins de m_iMinLifeBeforeEscape points de vies,on fuit comme un lache
    if(m_pBotActionInfo->m_Life <= m_iMinLifeBeforeEscape)
    {
        Escape(BotPosition) ;
        return ;
    }

    // Si un mechant est a coté et qu on est chaud comme la braise, on le frappe
    if(((m_pTabDest[KIT_BOT] != KPt(-1,-1)) && (BotPosition.Distance( m_pTabDest[KIT_BOT] ) <= 1.01f )) ||
        ((m_pTabDest[KIT_BOT] != KPt(-1,-1)) && (m_BotMode.bGoto == BOT_GOTO_POS)))
    {
        m_BotMode.bGoto = BOT_GOTO_BOT ;
        m_BotMode.destination = m_pTabDest[KIT_BOT] ;
        m_BotMode.itemType = KIT_BOT ;
        return ;
    }

    // Si on s'est rechargé au max, on se casse
    if(m_pBotActionInfo->m_Life >= GETLIFE(m_iVitality))
        m_pTabDest[KIT_REGENERATOR] = KPt(-1,-1) ;

    // on verifie si ce qu'on voulais chercher est toujours la    
	if (VerifyCurrentActions())
        return ;
    
    // Si on n'avais rien a faire, on cherche un item ou un Bot
    DefineActionForBourrinBot() ;

    // Si on a quelque chose a faire c bon
    if(m_BotMode.bGoto != BOT_GOTO_NOTHING)
        return ;   
    
    // Si on a pas de cul et qu'on a rien trouve, on se casse ailleurs
    m_BotMode.bGoto = BOT_GOTO_POS ;

    RandomPos(BotPosition) ;
}

/**
 **************************************************************************************************************************************************************
 * <Detailed description of the VerifyCurrentActions method>
 **************************************************************************************************************************************************************
 * @author : Mac Load
 * @date   : 25/07/2003
 * @param  : 
 * @return : bool
 **************************************************************************************************************************************************************/
bool PretMoaTBot::VerifyCurrentActions(void)
{
	// Si on est pas plein de vie et qu on ne se dirige pas vers un item de vie alors on onblie ce qu on voulait faire.
	if((m_pBotActionInfo->m_Life < GETLIFE(m_iVitality)) && (m_BotMode.itemType != KIT_REGENERATOR) && (m_BotMode.itemType != KIT_MEDIKIT) && (m_BotMode.itemType != KIT_BOT))
		return false ;

    // on verifie si les items sont touours la    
	if(m_BotMode.bGoto == BOT_GOTO_ITEM)
    {
		bool bVerifyAction = false ;

		for(int iCpt=0 ; iCpt < (KIT_COUNT - 1) ; iCpt++)
		{
			KITEMTYPE item = m_tabPreferenceGOTO[iCpt] ;

			if((item == KIT_REGENERATOR) || (item == KIT_MEDIKIT))
				continue ;

			if((m_BotMode.itemType != item) && (m_pTabDest[item] != KPt(-1,-1)))
			{
				if(!bVerifyAction)  // on a trouvé un meilleur item a allé chercher
				{
					if((m_BotMode.itemType == KIT_REGENERATOR) && (m_pTabDest[KIT_REGENERATOR] != KPt(-1,-1)))
						return true ;
					if((m_BotMode.itemType == KIT_MEDIKIT) && (m_pTabDest[KIT_MEDIKIT] != KPt(-1,-1)))
						return true ;
					break ;
				}
			}

			if((m_BotMode.itemType == item) && (m_pTabDest[item] != KPt(-1,-1))) // l'item est toujours là, on va le chercher
			{
				bVerifyAction = true ;
				break ;
			}
		}

		return bVerifyAction ;

		/*
        if((m_BotMode.itemType == KIT_BONUSVISION) && (m_pTabDest[KIT_BONUSVISION] != KPt(-1,-1)))
            return true ;
        if((m_BotMode.itemType == KIT_BONUSPOWER) && (m_pTabDest[KIT_BONUSPOWER] != KPt(-1,-1)))
            return true ;
        if((m_BotMode.itemType == KIT_BONUSSPEED) && (m_pTabDest[KIT_BONUSSPEED] != KPt(-1,-1)))
            return true ;
        if((m_BotMode.itemType == KIT_BONUSJOKER) && (m_pTabDest[KIT_BONUSJOKER] != KPt(-1,-1)))
            return true ;
        if((m_BotMode.itemType == KIT_REGENERATOR) && (m_pTabDest[KIT_REGENERATOR] != KPt(-1,-1)))
            return true ;
        if((m_BotMode.itemType == KIT_MEDIKIT) && (m_pTabDest[KIT_MEDIKIT] != KPt(-1,-1)))
            return true ;
        if((m_BotMode.itemType == KIT_BINOCULARS) && (m_pTabDest[KIT_BINOCULARS] != KPt(-1,-1)))
            return true ;
        if((m_BotMode.itemType == KIT_MINE) && (m_pTabDest[KIT_MINE] != KPt(-1,-1)))
            return true ;
        if((m_BotMode.itemType == KIT_LASER) && (m_pTabDest[KIT_LASER] != KPt(-1,-1)))
            return true ;
		*/

    }
    
    // on verifie si l'enemi est touours la    
    if((m_BotMode.bGoto == BOT_GOTO_BOT) && (m_pTabDest[KIT_BOT] != KPt(-1,-1)))
    {    
        m_BotMode.destination = m_pTabDest[KIT_BOT] ;
        return true ;
    }
    else if((m_BotMode.bGoto == BOT_GOTO_BOT) && (m_pTabDest[KIT_BOT] == KPt(-1,-1)))
    {
        m_BotMode.bGoto = BOT_GOTO_NOTHING ; 
    }
    
    return false ;
}

/**
 **************************************************************************************************************************************************************
 * <Detailed description of the DefineActionForBourrinBot method>
 **************************************************************************************************************************************************************
 * @author : Mac Load
 * @date   : 25/07/2003
 * @param  : 
 * @return : 
 **************************************************************************************************************************************************************/
void PretMoaTBot::DefineActionForBourrinBot()
{
	for(int iCpt=0 ; iCpt < (KIT_COUNT - 1) ; iCpt++)
	{
		KITEMTYPE item = m_tabPreferenceGOTO[iCpt] ;

		if((item == KIT_REGENERATOR) || (item == KIT_MEDIKIT))
		{
			if((m_pTabDest[item] != KPt(-1,-1)) && ((m_pBotActionInfo->m_Life < GETLIFE(m_iVitality))))
			{
				m_BotMode.bGoto = BOT_GOTO_ITEM ;
				m_BotMode.destination = m_pTabDest[item] ;
				m_BotMode.itemType = item ;
				return ;
			}
		}
		else if(item == KIT_BOT)
		{
			if(m_pTabDest[item] != KPt(-1,-1))
			{
				m_BotMode.bGoto = BOT_GOTO_BOT ;
				m_BotMode.destination = m_pTabDest[item] ;
				m_BotMode.itemType = item ;
				return ;
			}
		}
		else if(item == KIT_BINOCULARS)
		{
			if(m_pTabDest[item] != KPt(-1,-1))
			{
				m_BotMode.bGoto = BOT_GOTO_ITEM ;
				m_BotMode.destination = m_pTabDest[item] ;
				m_BotMode.itemType = item ;
				return ;
			}
		}
		else
		{
			if((m_pTabDest[item] != KPt(-1,-1)) && ((m_pBotActionInfo->m_nInventory < BOT_MAX_CHILDS) || ((m_pBotActionInfo->m_nInventory == BOT_MAX_CHILDS) && (HaveAnItemToDrop()))))
			{
				if((m_pBotActionInfo->m_nInventory == BOT_MAX_CHILDS) && (HaveAnItemToDrop()))
				{
					u32 iCptItem=0 ;
					bool bUselessItem = false ;
					while((m_tabPreferenceUselessItems[iCptItem] != -1) && (iCptItem < KIT_COUNT))
					{
						if (item == m_tabPreferenceUselessItems[iCptItem])
							bUselessItem = true ;
						iCptItem ++ ;
					}
					// on ne droppe pas un objet inutile si c'est pour reprendre un objet inutile
					if(!bUselessItem)
					{
						m_BotMode.bGoto = BOT_GOTO_ITEM ;
						m_BotMode.destination = m_pTabDest[item] ;
						m_BotMode.itemType = item ;
						return ;
					}
				}
				else
				{
					m_BotMode.bGoto = BOT_GOTO_ITEM ;
					m_BotMode.destination = m_pTabDest[item] ;
					m_BotMode.itemType = item ;
					return ;
				}				
			}
		}
	}

/*
    // On cherche d'abord le regenerator , histoire de faire le plein
    if((m_pTabDest[KIT_REGENERATOR] != KPt(-1,-1)) && ((m_pBotActionInfo->m_Life < GETLIFE(m_iVitality))))
    {
        m_BotMode.bGoto = BOT_GOTO_ITEM ;
        m_BotMode.destination = m_pTabDest[KIT_REGENERATOR] ;
        m_BotMode.itemType = KIT_REGENERATOR ;
    }
    else if((m_pTabDest[KIT_MEDIKIT] != KPt(-1,-1)) && ((m_pBotActionInfo->m_Life < GETLIFE(m_iVitality))))
    {
        m_BotMode.bGoto = BOT_GOTO_ITEM ;
        m_BotMode.destination = m_pTabDest[KIT_MEDIKIT] ;
        m_BotMode.itemType = KIT_MEDIKIT ;
    }
	// Puis des lasers car c'est quand meme efficace
	else if((m_pTabDest[KIT_LASER] != KPt(-1,-1)) && (m_pBotActionInfo->m_nInventory < BOT_MAX_CHILDS))
    {
        m_BotMode.bGoto = BOT_GOTO_ITEM ;
        m_BotMode.destination = m_pTabDest[KIT_LASER] ;
        m_BotMode.itemType = KIT_LASER ;
    }
	// On cherche ensuite les jumelles pour y voir plus clair
	else if(m_pTabDest[KIT_BINOCULARS] != KPt(-1,-1))
    {
        m_BotMode.bGoto = BOT_GOTO_ITEM ;
        m_BotMode.destination = m_pTabDest[KIT_BINOCULARS] ;
        m_BotMode.itemType = KIT_BINOCULARS ;
    }
    // On cherche ensuite un enemi pour l'eclater
    else if(m_pTabDest[KIT_BOT] != KPt(-1,-1))
    {
        m_BotMode.bGoto = BOT_GOTO_BOT ;
        m_BotMode.destination = m_pTabDest[KIT_BOT] ;
        m_BotMode.itemType = KIT_BOT ;
    }

    // Puis des items pour s'occuper
    else if((m_pTabDest[KIT_BONUSPOWER] != KPt(-1,-1)) && (m_pBotActionInfo->m_nInventory < BOT_MAX_CHILDS))
    {
        m_BotMode.bGoto = BOT_GOTO_ITEM ;
        m_BotMode.destination = m_pTabDest[KIT_BONUSPOWER] ;
        m_BotMode.itemType = KIT_BONUSPOWER ;
    }
    else if((m_pTabDest[KIT_BONUSJOKER] != KPt(-1,-1)) && (m_pBotActionInfo->m_nInventory < BOT_MAX_CHILDS))
    {
        m_BotMode.bGoto = BOT_GOTO_ITEM ;
        m_BotMode.destination = m_pTabDest[KIT_BONUSJOKER] ;
        m_BotMode.itemType = KIT_BONUSJOKER ;
    }
    else if((m_pTabDest[KIT_MINE] != KPt(-1,-1)) && (m_pBotActionInfo->m_nInventory < BOT_MAX_CHILDS))
    {
        m_BotMode.bGoto = BOT_GOTO_ITEM ;
        m_BotMode.destination = m_pTabDest[KIT_MINE] ;
        m_BotMode.itemType = KIT_MINE ;
    }
    else if((m_pTabDest[KIT_BONUSSPEED] != KPt(-1,-1)) && (m_pBotActionInfo->m_nInventory < BOT_MAX_CHILDS))
    {
        m_BotMode.bGoto = BOT_GOTO_ITEM ;
        m_BotMode.destination = m_pTabDest[KIT_BONUSSPEED] ;
        m_BotMode.itemType = KIT_BONUSSPEED ;
    }
    else if((m_pTabDest[KIT_BONUSVISION] != KPt(-1,-1)) && (m_pBotActionInfo->m_nInventory < BOT_MAX_CHILDS) && (m_pBotActionInfo->m_Vision < 5))
    {
        m_BotMode.bGoto = BOT_GOTO_ITEM ;
        m_BotMode.destination = m_pTabDest[KIT_BONUSVISION] ;
        m_BotMode.itemType = KIT_BONUSVISION ;
    }
*/
}

/**
 **************************************************************************************************************************************************************
 * <Detailed description of the RandomPos method>
 **************************************************************************************************************************************************************
 * @author : Mac Load
 * @date   : 25/07/2003
 * @param  : KPt BotPosition
 * @return : 
 **************************************************************************************************************************************************************/
void PretMoaTBot::RandomPos(KPt BotPosition)
{
    // On cherche un endroit ou on est pas encore alle
    for( s32 my = 2; my < (m_MapHeight - 2); my ++ )
    {
        for( s32 mx = 2; mx < (m_MapWidth - 2); mx ++ ) 
        {
            KCell* pCell = &m_pMapCell[my * m_MapWidth + mx];
            if( pCell->GetType() == UNKNOWN_CELL_TYPE )
            {
                m_BotMode.destination.x = mx ;
                m_BotMode.destination.y = my ;
                if(BotPosition.Distance(m_BotMode.destination) > 10)
                    return ;
            }
        }
	}

    // Si on est alle partout alors on va a des endroits connus(sans blague....)
    KCell* pCell ;
    int iPosx ;
    int iPosy ;
    do 
    {      
        iPosx = rand() % m_MapWidth ;
	    iPosy = rand() % m_MapHeight ;
        pCell = &m_pMapCell[iPosy * m_MapWidth + iPosx] ;
	
    } while(pCell->GetType() != 0) ;

    m_BotMode.destination.x = iPosx ;
    m_BotMode.destination.y = iPosy ;
    
}

/**
 **************************************************************************************************************************************************************
 * <Detailed description of the Escape method>
 **************************************************************************************************************************************************************
 * @author : Mac Load
 * @date   : 25/07/2003
 * @param  : KPt BotPosition
 * @return : 
 **************************************************************************************************************************************************************/
void PretMoaTBot::Escape(KPt BotPosition)
{
    // Et bien la on fuit
    if((DestBonusRegeneratorGLOBAL != KPt(-1,-1)) && (m_pTabDest[KIT_BOT] != DestBonusRegeneratorGLOBAL))
    {
		AddTempWall(m_pTabDest[KIT_BOT]) ;
        m_BotMode.bGoto = BOT_GOTO_ESCAPE ;
        m_BotMode.itemType = KIT_REGENERATOR ;
        m_BotMode.destination = DestBonusRegeneratorGLOBAL ;
        return ;
    }   
    else if(m_pTabDest[KIT_MEDIKIT] != KPt(-1,-1))
    {
		AddTempWall(m_pTabDest[KIT_BOT]) ;
        m_BotMode.bGoto = BOT_GOTO_ESCAPE ;
        m_BotMode.itemType = KIT_MEDIKIT ;
        m_BotMode.destination = m_pTabDest[KIT_MEDIKIT] ;
        return ;
    }    
    else if(m_BotMode.bGoto != BOT_GOTO_ESCAPE)
    {
        // On marque 1 sur l'emplacement du bot pour le contourner
        if(m_pTabDest[KIT_BOT] != KPt(-1,-1))
            AddTempWall(m_pTabDest[KIT_BOT]) ;
        m_BotMode.bGoto = BOT_GOTO_ESCAPE ;
        RandomPos(BotPosition) ;
        return ;
    }

    if((DestBonusRegeneratorGLOBAL != KPt(-1,-1)) && (m_pTabDest[KIT_BOT] == DestBonusRegeneratorGLOBAL))
    {
		AddTempWall(m_pTabDest[KIT_BOT]) ;
        RandomPos(BotPosition) ;
    }
}

/**
 **************************************************************************************************************************************************************
 * <Detailed description of the HaveAGun method>
 **************************************************************************************************************************************************************
 * @author : Mac Load
 * @date   : 25/07/2003
 * @param  : 
 * @return : bool
 **************************************************************************************************************************************************************/
bool PretMoaTBot::HaveAGun()
{
    // On regarde si on a un flingue
    for (u32 iCpt = 0 ; iCpt < m_pBotActionInfo->m_nInventory ; iCpt++ )
    {
        if (m_pBotActionInfo->m_pInventory[iCpt].GetType() == KIT_LASER)
            return true ;
    }
    return false ;
}

/**
 **************************************************************************************************************************************************************
 * <Detailed description of the HaveAMine method>
 **************************************************************************************************************************************************************
 * @author : Mac Load
 * @date   : 25/07/2003
 * @param  : 
 * @return : bool
 **************************************************************************************************************************************************************/
bool PretMoaTBot::HaveAMine()
{
    // On regarde si on a une mine
    for (u32 iCpt = 0 ; iCpt < m_pBotActionInfo->m_nInventory ; iCpt++ )
    {
        if (m_pBotActionInfo->m_pInventory[iCpt].GetType() == KIT_MINE)
            return true ;
    }
    return false ;
}

/**
 **************************************************************************************************************************************************************
 * <Detailed description of the HaveAnItemToDrop method>
 **************************************************************************************************************************************************************
 * @author : Mac Load
 * @date   : 01/08/2004
 * @param  : 
 * @return : bool
 **************************************************************************************************************************************************************/
bool PretMoaTBot::HaveAnItemToDrop()
{
    // On regarde si on a un item a droper et si on peut le dropper

	for( u32 i = 0 ; i < KIT_COUNT ; i ++ )
	{
		if(i != KIT_BOT)
        {
			if(m_pTabDest[i] == m_pBotActionInfo->m_Pos )
			{
				return false ; // un item se trouve deja sur la case ou on est
			}
		}
	}

	u32 iCpt=0 ;
	while((m_tabPreferenceUselessItems[iCpt] != -1) && (iCpt < KIT_COUNT))
	{
		for (u32 iCptItem = 0 ; iCptItem < m_pBotActionInfo->m_nInventory ; iCptItem++ )
		{
			if (m_pBotActionInfo->m_pInventory[iCptItem].GetType() == m_tabPreferenceUselessItems[iCpt])
				return true ;
		}
		iCpt ++ ;
	}
    return false ;
}

/**
 **************************************************************************************************************************************************************
 * <Detailed description of the GetItemToDrop method>
 **************************************************************************************************************************************************************
 * @author : Mac Load
 * @date   : 01/08/2004
 * @param  : 
 * @return : bool
 **************************************************************************************************************************************************************/
KITEMTYPE PretMoaTBot::GetItemToDrop()
{
	u32 iCpt=0 ;
	while((m_tabPreferenceUselessItems[iCpt] != -1) && (iCpt < KIT_COUNT))
	{
		for (u32 iCptItem = 0 ; iCptItem < m_pBotActionInfo->m_nInventory ; iCptItem++ )
		{
			if (m_pBotActionInfo->m_pInventory[iCptItem].GetType() == m_tabPreferenceUselessItems[iCpt])
				return m_tabPreferenceUselessItems[iCpt] ;
		}
		iCpt ++ ;
	}
    return KIT_NONE ;
}

/**
 **************************************************************************************************************************************************************
 * <Detailed description of the CanShoot method>
 **************************************************************************************************************************************************************
 * @author : Mac Load
 * @date   : 25/07/2003
 * @param  : 
 * @return : bool
 **************************************************************************************************************************************************************/
bool PretMoaTBot::CanShoot()
{
    // On regarde si on peu tirer
    if(!HaveAGun())
        return false ;
    if(m_BotMode.bGoto != BOT_GOTO_BOT)
        return false ;
    
	u32 uRealVision = GETSIGHT(g_BotInfo.GetVision()) ;
	float fDistanceToShoot = uRealVision - 1 + 0.01f ; // 2.01f ;

	//comme le shoot au flingue rapporte peu, on le limite a une distance de 3.
	if(fDistanceToShoot > 4) fDistanceToShoot = 3.01f ;

    if(((m_pBotActionInfo->m_Pos.x == m_BotMode.destination.x) &&
        (m_pBotActionInfo->m_Pos.Distance(m_BotMode.destination) < fDistanceToShoot)) ||
        ((m_pBotActionInfo->m_Pos.y == m_BotMode.destination.y) &&
        (m_pBotActionInfo->m_Pos.Distance(m_BotMode.destination) < fDistanceToShoot)))
    {
        if ( m_BotMode.destination.x < m_pBotActionInfo->m_Pos.x )        m_BotMode.direction = KBAT_LEFT ;
        else if ( m_BotMode.destination.x > m_pBotActionInfo->m_Pos.x )   m_BotMode.direction = KBAT_RIGHT ;
        if ( m_BotMode.destination.y < m_pBotActionInfo->m_Pos.y )        m_BotMode.direction = KBAT_UP ;
        else if ( m_BotMode.destination.y > m_pBotActionInfo->m_Pos.y )   m_BotMode.direction = KBAT_DOWN ;

        // On Verifie si il n'y a pas de mur entre nous et le bot
        int iStep = 0 ;
        if(m_BotMode.direction == KBAT_LEFT)		iStep = -1 ;
        else if(m_BotMode.direction == KBAT_RIGHT)	iStep = 1 ;
        else if(m_BotMode.direction == KBAT_UP)     iStep = -m_MapWidth ;
        else if(m_BotMode.direction == KBAT_DOWN)   iStep = m_MapWidth ;
        
        int iCell = m_pBotActionInfo->m_Pos.y * m_MapWidth + m_pBotActionInfo->m_Pos.x ;
        int iBotCell = m_BotMode.destination.y * m_MapWidth + m_BotMode.destination.x ;
        int iNewCell = iCell + iStep ;

        while(iNewCell != iBotCell)
        {
            if(m_pMapCell[iNewCell].GetType() > 0)
                return false ;

            iNewCell += iStep ;
        } 

        return true ;
    }
    
    return false ;
}

/**
**************************************************************************************************************************************************************
* <Detailed description of the IsPathFinderMustBeClear method>
**************************************************************************************************************************************************************
* @author : Mac Load
* @date   : 02/08/2003
* @param  : 
* @return : bool
**************************************************************************************************************************************************************/
bool PretMoaTBot::IsPathFinderMustBeClear(void)
{
    if(m_LastPathFinderDest != m_BotMode.destination)
        return true ;
    
    if(m_PathFinder == NULL)
        return true ;
    
    for( u32 i = 1 ; i < m_PathFinder->m_FinalCellsList.size() ; i ++ )
    {
        int iCell = m_PathFinder->m_FinalCellsList[i] ;

        if((m_pMapCell[iCell].GetType() != UNKNOWN_CELL_TYPE) && (m_pMapCell[iCell].GetType() > 0))
            return true ;
    }

    return false ;
}

/**
 **************************************************************************************************************************************************************
 * <Detailed description of the AddItemToList method>
 **************************************************************************************************************************************************************
 * @author : Mac Load
 * @date   : 23/03/2004
 * @param  : u32 uItemType , KPt itemPos
 * @return : void
 **************************************************************************************************************************************************************/
void PretMoaTBot::AddItemToList(u32 uItemType , KPt itemPos)
{
	if(!m_pTabItems) return ;

	if(IsItemAlreadyExist(uItemType,itemPos))
		return ;

	m_pTabItems[uItemType].push_back(itemPos) ;
}

/**
 **************************************************************************************************************************************************************
 * <Detailed description of the RemoveItemToList method>
 **************************************************************************************************************************************************************
 * @author : Mac Load
 * @date   : 23/03/2004
 * @param  : u32 uItemType , KPt itemPos
 * @return : void
 **************************************************************************************************************************************************************/
void PretMoaTBot::RemoveItemToList(u32 uItemType , KPt itemPos)
{
	if(!m_pTabItems) return ;

	if(!IsItemAlreadyExist(uItemType,itemPos))
		return ;

	typedef vector<KPt> tabItems ; 
    tabItems::iterator it ;
    KPt iValue ;  ;
    for (it = m_pTabItems[uItemType].begin(); it != m_pTabItems[uItemType].end();it++)
    {
        iValue = *it ;
        if(iValue == itemPos)
        {
            m_pTabItems[uItemType].erase(it) ;
            break ;
        }		    
	}
}

/**
 **************************************************************************************************************************************************************
 * <Detailed description of the RemoveItemToList method>
 **************************************************************************************************************************************************************
 * @author : Mac Load
 * @date   : 23/03/2004
 * @param  : KPt itemPos
 * @return : void
 **************************************************************************************************************************************************************/
void PretMoaTBot::RemoveItemToList(KPt itemPos)
{
	if(!m_pTabItems) return ;

	for(u32 uCptItemsType = 0 ; uCptItemsType <  KIT_COUNT ; uCptItemsType ++)
	{
		for(u32 uCpt = 0 ; uCpt < m_pTabItems[uCptItemsType].size() ; uCpt ++)
		{
			if(m_pTabItems[uCptItemsType][uCpt] == itemPos)
			{
				RemoveItemToList(uCptItemsType,itemPos) ;
				break ;
			}
		}
    }
}

/**
 **************************************************************************************************************************************************************
 * <Detailed description of the IsItemAlreadyExist method>
 **************************************************************************************************************************************************************
 * @author : Mac Load
 * @date   : 23/03/2004
 * @param  : u32 uItemType , KPt itemPos
 * @return : bool
 **************************************************************************************************************************************************************/
bool PretMoaTBot::IsItemAlreadyExist(u32 uItemType , KPt itemPos)
{
	if(!m_pTabItems) return true ;

	for(u32 iCpt = 0 ; iCpt < m_pTabItems[uItemType].size() ; iCpt ++)
    {
        if(m_pTabItems[uItemType][iCpt] == itemPos)
            return true ;
    }
    return false ;
}

/**
 **************************************************************************************************************************************************************
 * <Detailed description of the UpdateItems method>
 **************************************************************************************************************************************************************
 * @author : Mac Load
 * @date   : 23/03/2004
 * @param  : KCell* pMapCell,KPt BotPosition, KItemInfo* pItems, u32 nItems
 * @return : void
 **************************************************************************************************************************************************************/
void PretMoaTBot::UpdateItems(KCell* pMapCell,KPt BotPosition, KItemInfo* pItems, u32 nItems)
{
	if(!m_pTabItems) return ;

	// On vire tous les items des cases visibles
	for( int iy = 0; iy < m_MapHeight; iy ++ )
	{
        for( int ix = 0; ix < m_MapWidth; ix ++ ) 
        {
            KCell* pCell = &pMapCell[iy * m_MapWidth + ix] ;
			if(pCell->GetType() == 0)
			{
				RemoveItemToList(KPt(ix,iy)) ;
			}
		}
	}

	// On ajoute les nouveaux items
	for( u32 i = 0; i < nItems; i ++ )
	{
		if(pItems[i].GetType() == KIT_BOT)
        {
			if(pItems[i].GetPos() != BotPosition )
			{
				if(pItems[i].IsAlive())
					AddItemToList(KIT_BOT,pItems[i].GetPos()) ;
			}
		}
		else
			AddItemToList(pItems[i].GetType(),pItems[i].GetPos()) ;
	}
}

/**
 **************************************************************************************************************************************************************
 * <Detailed description of the GetNearestValidItem method>
 **************************************************************************************************************************************************************
 * @author : Mac Load
 * @date   : 23/03/2004
 * @param  : KPt BotPosition, u32 uItemType
 * @return : KPt
 **************************************************************************************************************************************************************/
KPt PretMoaTBot::GetNearestValidItem(KPt BotPosition, u32 uItemType)
{
	if(!m_pTabItems) return KPt(-1,-1) ;

	KPt		Target(-1,-1);
	float	Nearest = 10000.0f;
	float	Distance;
    
	for(u32 iCpt = 0 ; iCpt < m_pTabItems[uItemType].size() ; iCpt ++)
    {
		KCell* pCell = &m_pMapCell[m_pTabItems[uItemType][iCpt].y * m_MapWidth + m_pTabItems[uItemType][iCpt].x] ;

		Distance = m_pTabItems[uItemType][iCpt].Distance( BotPosition ) ;

        if(Distance < Nearest)
		{
			Nearest = Distance ;
			Target = m_pTabItems[uItemType][iCpt] ;
		}
    }

	return Target ;
}

/**
 **************************************************************************************************************************************************************
 * <Detailed description of the RemoveAllTempWall method>
 **************************************************************************************************************************************************************
 * @author : Mac Load
 * @date   : 23/03/2004
 * @param  : 
 * @return : void
 **************************************************************************************************************************************************************/
void PretMoaTBot::RemoveAllTempWall()
{
	u32 uWallTempToDelete = MAP_TEMP_WALL + m_iCptTour % m_iNbTurnsTempWallRest ;
	u32 uMineTempToDelete = MAP_TEMP_WALL + m_iNbTurnsTempWallRest + m_iCptTour % m_iNbTurnsMinePositionRest ;

	for( s32 my = 0; my < m_MapHeight; my ++ )
    {
        for( s32 mx = 0; mx < m_MapWidth; mx ++ ) 
        {
            KCell* pGlobalCell = &m_pMapCell[my * m_MapWidth + mx];

            if((pGlobalCell->GetType() == uWallTempToDelete) || (pGlobalCell->GetType() == uMineTempToDelete))
            {
                SetGlobalMapCellType( KPt(mx,my), 0 ) ;
            }
        }
	}
}

/**
 **************************************************************************************************************************************************************
 * <Detailed description of the AddTempWall method>
 **************************************************************************************************************************************************************
 * @author : Mac Load
 * @date   : 31/07/2004
 * @param  : 
 * @return : void
 **************************************************************************************************************************************************************/
void PretMoaTBot::AddTempWall(KPt Pos, bool bTempWall)
{
	u32 uWallTempToDelete = 0 ;
	if(bTempWall)
		uWallTempToDelete = MAP_TEMP_WALL + m_iCptTour % m_iNbTurnsTempWallRest ;
	else
		uWallTempToDelete = MAP_TEMP_WALL + m_iNbTurnsTempWallRest + m_iCptTour % m_iNbTurnsMinePositionRest ;
	u32 uType = GetGlobalMapCellType(Pos) ;
	if(uType < 30)
		SetGlobalMapCellType(Pos,uWallTempToDelete) ;
}