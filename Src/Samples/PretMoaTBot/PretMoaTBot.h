//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: PretMoaTBot.h
//	Author			: - Macload
//	Date			: 19/07/2003
//	Modification	:
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------

// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the LECHBOT_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// LECHBOT_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#ifdef _WIN32
#ifdef PRETMOATBOT_EXPORTS
#define PRETMOATBOT_API __declspec(dllexport)
#else
#define PRETMOATBOT_API __declspec(dllimport)
#endif
#endif // _WIN32

#ifdef _LINUX
#define PRETMOATBOT_API
#endif // _LINUX

#include "BotDefs.h"
#include "Level.h"
#include "Bot.h"
#include "Pathfinder.h"
#include <vector>

typedef enum BOT_GOTO
{
    BOT_GOTO_ITEM,
    BOT_GOTO_BOT,
    BOT_GOTO_POS,
    BOT_GOTO_ESCAPE,
    BOT_GOTO_NOTHING,
};

struct BOT_MODE
{
    BOT_GOTO         bGoto ;
    KITEMTYPE        itemType ;
    KPt              destination ;
    KBOTATTACKTARGET direction ;
};

//--------------------------------------------------------------------------------------------
// Fonctions exporté
extern "C" PRETMOATBOT_API char* GetpBotName();
extern "C" PRETMOATBOT_API KBotInfo* GetpInfos( void* pContext );
extern "C" PRETMOATBOT_API void* StartGame( s32 MapWidth, s32 MapHeight );
extern "C" PRETMOATBOT_API void EndGame( void* pContext );
extern "C" PRETMOATBOT_API KBotAction* GetpBotAction( KBotActionInfo* pBotActionInfo, void* pContext );
extern "C" PRETMOATBOT_API void DebugDisplay( HINSTANCE hInstance,HWND hWnd );

class PretMoaTBot
{
 public :
    PretMoaTBot(s32 MapWidth, s32 MapHeight) ;
    ~PretMoaTBot() ;

    KBotAction*     Manage(KBotActionInfo* pBotActionInfo) ;

    u32				m_iVitality ;
	u32				m_iPower ;
	u32				m_iSpeed ;
	u32				m_iVision ;
	u32				m_iMinLifeBeforeEscape ;
	u32				m_iNbTurnsTempWallRest ;
	u32				m_iNbTurnsMinePositionRest ;

	char			*m_pBotName ;

	int				m_iCptTour ;
	vector<KPt>		*m_pTabItems ;
	KPt				m_pTabDest[KIT_COUNT] ;
	KITEMTYPE		m_tabPreferenceGOTO[KIT_COUNT] ;
	KITEMTYPE		m_tabPreferenceUselessItems[KIT_COUNT] ;


 protected :

    bool            IsPathFinderMustBeClear() ;
    void            InitMap() ;
    void            UpdateMap(KCell* pMapCell) ;
    void            SearchSolutions(KPt BotPosition, KItemInfo* pItems, u32 nItems) ;
    bool            VerifyCurrentActions(void) ;
    void            DefineActionForBourrinBot() ;
    void            Escape(KPt BotPosition) ;
    void            RandomPos(KPt BotPosition) ;
    bool            HaveAGun() ;
    bool            HaveAMine() ;
    bool            HaveAnItemToDrop() ;
	KITEMTYPE		GetItemToDrop() ;
	bool            CanShoot() ;
	void			AddTempWall(KPt Pos, bool bTempWall=true) ;
    void			RemoveAllTempWall() ;
	inline CellType	GetGlobalMapCellType(KPt Pos)					{ return m_pMapCell[Pos.y * m_MapWidth + Pos.x].GetType(); }
    inline void		SetGlobalMapCellType(KPt Pos, CellType Type)	{ m_pMapCell[Pos.y * m_MapWidth + Pos.x].SetType(Type); }

	void			AddItemToList(u32 uItemType , KPt itemPos) ;
	void			RemoveItemToList(u32 uItemType , KPt itemPos) ;
	void			RemoveItemToList(KPt itemPos) ;
	bool			IsItemAlreadyExist(u32 uItemType , KPt itemPos) ;
	void			UpdateItems(KCell* pMapCell, KPt BotPosition, KItemInfo* pItems, u32 nItems) ;
	KPt				GetNearestValidItem(KPt BotPosition, u32 uItemType) ;

    KBotActionInfo* m_pBotActionInfo ;

    KPt             DestBonusRegeneratorGLOBAL ;
    KPt             m_LastPathFinderDest ;

    CPathFinder     *m_PathFinder ;
    KBotAction		m_BotAction;
    KCell*			m_pMapCell ;
    s32				m_MapWidth;
    s32				m_MapHeight;
    BOT_MODE        m_BotMode ;
    KPt             m_LastPos ;
};
