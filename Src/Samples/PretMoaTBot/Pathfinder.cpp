//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: Pathfinder.cpp
//	Author			: - Macload
//	Date			: 17/07/2003
//	Modification	: 24/03/2004
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------
#include "Pathfinder.h"

/**
 **************************************************************************************************************************************************************
 * <Detailed description of the CPathFinder::CPathFinder method>
 **************************************************************************************************************************************************************
 * @author : Mac Load
 * @date   : 22/07/2003
 * @param  :KCell* pMapCell,int iMapWidth,int iMapHeight,int iStartPosX,int iStartPosY,int iEndPosX,int iEndPosY : 
 * @return : 
 **************************************************************************************************************************************************************/
CPathFinder::CPathFinder(KCell* pMapCell,int iMapWidth,int iMapHeight,int iStartPosX,int iStartPosY,int iEndPosX,int iEndPosY)
{
    m_iCount        = 0 ;
    m_iMapWidth     = iMapWidth ;
    m_iMapHeight    = iMapHeight ;
    m_iStartPosX    = iStartPosX ;
    m_iStartPosY    = iStartPosY ;
    m_iEndPosX      = iEndPosX ;
    m_iEndPosY      = iEndPosY ;
    m_pMapCell      = new sCell[m_iMapWidth * m_iMapHeight] ;

    for( int iy = 0; iy < m_iMapHeight; iy ++ )
        for( int ix = 0; ix < m_iMapWidth; ix ++ ) 
        {
            KCell* pCell = &pMapCell[iy * m_iMapWidth + ix];

			 m_pMapCell[iy * m_iMapWidth + ix].iId = iy * m_iMapWidth + ix ;
             m_pMapCell[iy * m_iMapWidth + ix].iParentID = -1 ;
             m_pMapCell[iy * m_iMapWidth + ix].iPosX = ix ;
             m_pMapCell[iy * m_iMapWidth + ix].iPosY = iy ;

            if((pCell->GetType() == UNKNOWN_CELL_TYPE) || (pCell->GetType() == 0))
                m_pMapCell[iy * m_iMapWidth + ix].bWall = false ;
            else
                m_pMapCell[iy * m_iMapWidth + ix].bWall = true ;
        }
}

/**
 **************************************************************************************************************************************************************
 * <Detailed description of the CPathFinder::~CPathFinder method>
 **************************************************************************************************************************************************************
 * @author : Mac Load
 * @date   : 22/07/2003
 * @param  :
 * @return : 
 **************************************************************************************************************************************************************/
CPathFinder::~CPathFinder()
{
    SafeDeleteArray(m_pMapCell) ;
    m_OpenList.clear() ;
    m_CloseList.clear() ;
    m_FinalCellsList.clear() ;
    m_OpenList.empty() ;
    m_CloseList.empty() ;
    m_FinalCellsList.empty() ;
}

/**
 **************************************************************************************************************************************************************
 * <Detailed description of the CPathFinder::ComputePath method>
 **************************************************************************************************************************************************************
 * @author : Mac Load
 * @date   : 22/07/2003
 * @param  :
 * @return : bool
 **************************************************************************************************************************************************************/
bool CPathFinder::ComputePath(void)
{
    int iStartCell = m_pMapCell[m_iStartPosY * m_iMapWidth + m_iStartPosX].iId ;
    int iEndCell = m_pMapCell[m_iEndPosY * m_iMapWidth + m_iEndPosX].iId ;

    m_pMapCell[iStartCell].fG = 0.0f ;

    m_OpenList.push_back(iStartCell) ;

    int iLowestSquare = 0 ;

    // On boucle tant que iLowestSquare != iEndCell
    do 
    {
        // On recupere le carre qui a le fResult le plus faible
        iLowestSquare = GetLowestSquare() ;

        if(iLowestSquare == -1)
            break ;

        // Ajoute les carre autours du point qui a le fResult le plus faible
		int iNewCell1 = iLowestSquare - 1 ;
		int iNewCell2 = iLowestSquare + 1 ;
		int iNewCell3 = iLowestSquare - m_iMapWidth ;
		int iNewCell4 = iLowestSquare + m_iMapWidth ;
		AddNearestSquaresToOpenList(iLowestSquare,iNewCell1) ;// Ajoute la case de gauche
		AddNearestSquaresToOpenList(iLowestSquare,iNewCell2) ;// Ajoute la case de droite
		AddNearestSquaresToOpenList(iLowestSquare,iNewCell3) ;// Ajoute la case du haut
		AddNearestSquaresToOpenList(iLowestSquare,iNewCell4) ;// Ajoute la case du bas

        // On passe le iLowestSquare dans la closelist
        ChangeToCloseList(iLowestSquare) ;

    } while(iLowestSquare != iEndCell) ;

    // On est arrive a la fin du parcours , 
	// maintenant on le reconstitue en partant de la fin

    int iParentId = m_pMapCell[iEndCell].iParentID ;
        
    if(iParentId == -1)
    {
        iEndCell = m_CloseList[(m_CloseList.size() - 1)] ;
        iParentId = m_pMapCell[iEndCell].iParentID ;
    }

    m_FinalCellsList.push_back(iEndCell) ;

    do 
    {
		if(iParentId > (m_iMapHeight * m_iMapWidth))
		{
//			KASSERT(false) ;
			return false ;
		}
        m_FinalCellsList.push_back(iParentId) ;
        iParentId = m_pMapCell[iParentId].iParentID ;
    	
    } while(iParentId != iStartCell) ;

    return true ;
}

/**
 **************************************************************************************************************************************************************
 * <Detailed description of the CPathFinder::GetLowestSquare method>
 **************************************************************************************************************************************************************
 * @author : Mac Load
 * @date   : 22/07/2003
 * @param  :
 * @return : int
 **************************************************************************************************************************************************************/
int CPathFinder::GetLowestSquare(void)
{
    int iLowestCell = -1 ;
    int iMaxValue = 100000 ;

    if(m_OpenList.size() == 1)
        return m_OpenList[0] ;
    else
    {
        for(u32 iCpt = 0 ; iCpt < m_OpenList.size() ; iCpt ++)
        {
            if(m_pMapCell[m_OpenList[iCpt]].fResult < iMaxValue)
            {
                iLowestCell = m_OpenList[iCpt] ;
                iMaxValue = (int)m_pMapCell[m_OpenList[iCpt]].fResult ;
            }
        }
    }

    return iLowestCell ;
}

/**
 **************************************************************************************************************************************************************
 * <Detailed description of the CPathFinder::IsInOpenList method>
 **************************************************************************************************************************************************************
 * @author : Mac Load
 * @date   : 22/07/2003
 * @param  : iCellId
 * @return : bool
 **************************************************************************************************************************************************************/
bool CPathFinder::IsInOpenList(int iCellId)
{
    for(u32 iCpt = 0 ; iCpt < m_OpenList.size() ; iCpt ++)
    {
        if(m_pMapCell[m_OpenList[iCpt]].iId == iCellId)
            return true ;
    }
    return false ;
}

/**
 **************************************************************************************************************************************************************
 * <Detailed description of the CPathFinder::AddNearestSquareToOpenList method>
 **************************************************************************************************************************************************************
 * @author : Mac Load
 * @date   : 22/07/2003
 * @param  : int iParentSquareId
 * @return : 
 **************************************************************************************************************************************************************/
void CPathFinder::AddNearestSquaresToOpenList(int iParentSquareId,int iNewCell)
{
	float fGParent = m_pMapCell[iParentSquareId].fG ;

    if(!m_pMapCell[iNewCell].bWall)
    {
        if((!IsInOpenList(iNewCell)) && (m_pMapCell[iNewCell].iParentID == -1))
        {
            m_pMapCell[iNewCell].iParentID = iParentSquareId ;
            CalculCoefficient(iNewCell,fGParent) ;
            m_OpenList.push_back(iNewCell) ;
        }
        else
        {
            if(m_pMapCell[iNewCell].iParentID != -1)
            {
                float fGValueOldParent = m_pMapCell[iNewCell].fG ;

                if(fGParent < fGValueOldParent)
                {
                    m_pMapCell[iNewCell].iParentID = iParentSquareId ;
                    CalculCoefficient(iNewCell,fGParent) ;
                }
            }
        }
    }
}

/**
 **************************************************************************************************************************************************************
 * <Detailed description of the CPathFinder::ChangeToCloseList method>
 **************************************************************************************************************************************************************
 * @author : Mac Load
 * @date   : 22/07/2003
 * @param  : int iCellId
 * @return : 
 **************************************************************************************************************************************************************/
void CPathFinder::ChangeToCloseList(int iCellId)
{
    typedef vector<int> COpenList ; 
    COpenList::iterator it ;
    int iValue ;  ;
    for (it = m_OpenList.begin(); it != m_OpenList.end();it++)
    {
        iValue = *it ;
        if(iValue == iCellId)
        {
            m_OpenList.erase(it) ;
            break ;
        }		    
	}
    
    m_CloseList.push_back(iCellId) ;
}

/**
 **************************************************************************************************************************************************************
 * <Detailed description of the CPathFinder::CalculCoefficient method>
 **************************************************************************************************************************************************************
 * @author : Mac Load
 * @date   : 22/07/2003
 * @param  : int iCellId
 * @return : 
 **************************************************************************************************************************************************************/
void CPathFinder::CalculCoefficient(int iCellId,float fGParent)
{
    m_pMapCell[iCellId].fG = (float)(10 + fGParent);
    m_pMapCell[iCellId].fH = (float)(10 * (abs(m_pMapCell[iCellId].iPosX - m_iEndPosX) + abs(m_pMapCell[iCellId].iPosY - m_iEndPosY)));
    m_pMapCell[iCellId].fResult = m_pMapCell[iCellId].fH + m_pMapCell[iCellId].fG ;
}

/**
 **************************************************************************************************************************************************************
 * <Detailed description of the CPathFinder::GetCell method>
 **************************************************************************************************************************************************************
 * @author : Mac Load
 * @date   : 22/07/2003
 * @param  : 
 * @return : sCell
 **************************************************************************************************************************************************************/
sCell CPathFinder::GetCell(int iCellId)
{
    return m_pMapCell[iCellId] ;
}

/**
 **************************************************************************************************************************************************************
 * <Detailed description of the GetNextCell::GetCell method>
 **************************************************************************************************************************************************************
 * @author : Mac Load
 * @date   : 22/07/2003
 * @param  : 
 * @return : int
 **************************************************************************************************************************************************************/
int CPathFinder::GetNextCell()
{
    int iPos = m_FinalCellsList.size() - 1 - m_iCount ;
    if(iPos <= 0)
        iPos = 0 ; 
    m_iCount ++ ;
    return m_FinalCellsList[iPos] ;
}
