//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: Bot.cpp
//	Author			: - Aitonfrere	
//					  - Dino			dino@zythum-project.com
//
//	Date			: 16/07/2003
//	Modification	:
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------
#include "Render.h"
#include "Level.h"
#include "Timer.h"
#include "Mesh.h"
#include "Shader.h"
#include "Bonus.h"

//--------------------------------------------------------------------------------------------
KMesh*		KBonus::m_pMesh			= NULL;
KShader*	KBonus::m_pVisionShader	= NULL;
KShader*	KBonus::m_pSpeedShader	= NULL;
KShader*	KBonus::m_pPowerShader	= NULL;
KShader*	KBonus::m_pJokerShader	= NULL;

//--------------------------------------------------------------------------------------------
KBonus::KBonus( KRender* pRender, KLevel* pLevel )
: KItem( pRender, pLevel )
{
	m_Type		= KIT_BONUSJOKER;
	m_TypeJoker	= (KITEMTYPE) ( KIT_BONUSVISION + (rand() % 3) );
	m_bTakeAway	= true;

}

//--------------------------------------------------------------------------------------------
KBonus::~KBonus()
{
}

//--------------------------------------------------------------------------------------------
bool KBonus::Load( KRender* pRender )
{
	pRender->LoadMesh( "Meshes/bonus", &m_pMesh, false );
	pRender->LoadShader( "Textures/bonusvis.png", &m_pVisionShader );
	pRender->LoadShader( "Textures/bonusspeed.png", &m_pSpeedShader );
	pRender->LoadShader( "Textures/bonuspower.png", &m_pPowerShader );
	pRender->LoadShader( "Textures/bonusjoker.png", &m_pJokerShader );

	return true;
}

//--------------------------------------------------------------------------------------------
void KBonus::Unload()
{
	SafeDelete( m_pMesh );
	SafeDelete( m_pVisionShader );
	SafeDelete( m_pSpeedShader );
	SafeDelete( m_pPowerShader );
	SafeDelete( m_pJokerShader );
}

//--------------------------------------------------------------------------------------------
void KBonus::Display2D( KPt& Pos, KPt& Size )
{
	float	pTU[4], pTV[4];
	float	Type = (float)(m_Type - KIT_BONUSVISION);

	// Affiche le bonus
	pTV[0] = 0.125f;
	pTV[1] = 0.125f;
	pTV[2] = 0.250f;
	pTV[3] = 0.250f;

	pTU[0] = Type * 0.125f + 0.5f;
	pTU[1] = Type * 0.125f + 0.625f;
	pTU[2] = Type * 0.125f + 0.5f;
	pTU[3] = Type * 0.125f + 0.625f;

	// Effet top d�lire
	u8		Intensity = u8( fabs( sinf( (float)g_Timer.GetTime() / 200.0f ) * 16.0f ) );
	KCOLOR	Specular = KRGBA( Intensity, Intensity, Intensity, 255 );

	m_pRender->DrawQuad( Pos.x, Pos.y, Size.x, Size.y, m_pLevel->GetpShader(), KRGBA( 255, 255, 255, 255 ), Specular, KRM_NORMAL, pTU, pTV );
}

//--------------------------------------------------------------------------------------------
void KBonus::Display( KPt& Pos )
{
	float	Angle = ((float)g_Timer.GetTime() / 500.0f);

	SetMatrix( Pos, 9.0f, Angle, 0.0f, Angle );

	KShader*	pShader = NULL;

	switch( m_Type )
	{
	case KIT_BONUSVISION:
		pShader = m_pVisionShader;
		break;
	case KIT_BONUSPOWER:
		pShader = m_pSpeedShader;
		break;
	case KIT_BONUSSPEED:
		pShader = m_pVisionShader;
		break;
	case KIT_BONUSJOKER:
		pShader = m_pJokerShader;
		break;
	default:
		break;
	}

	// Define specific shader bot
	if( m_pMesh->GetnMaterials() )
	{
		m_pMesh->GetpMaterial( 0 )->m_pShader = pShader;
	}

	m_pRender->DrawSubMesh( m_Matrix, m_pMesh, 0 );
	m_pRender->DrawSubMesh( m_Matrix, m_pMesh, 1 );
}
