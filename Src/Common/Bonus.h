//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: Bonus.h
//	Author			: - Aitonfrere	
//					  - Dino			dino@zythum-project.com
//
//	Date			: 18/07/2003
//	Modification	:
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------
#ifndef	__BONUS_H__
#define	__BONUS_H__

#include "Item.h"
#include "BotDefs.h"

//--------------------------------------------------------------------------------------------
class KBonus : public KItem
{
protected:
	KITEMTYPE				m_TypeJoker;	// Type de joker
	static KMesh*			m_pMesh;
	static KShader*			m_pVisionShader;
	static KShader*			m_pSpeedShader;
	static KShader*			m_pPowerShader;
	static KShader*			m_pJokerShader;

public:
	KBonus( KRender* pRender, KLevel* pLevel );
	virtual ~KBonus();

	virtual void			Display( KPt& Pos );
	virtual	void			Display2D( KPt& Pos, KPt& Size );
	static bool				Load( KRender* pRender );
	static void				Unload();

	KITEMTYPE				GetTypeJoker() { return m_TypeJoker; }
};

#endif // __BONUS_H__
