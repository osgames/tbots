//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: Timer.h
//	Author			: - Aitonfrere	
//					  - Dino			dino@zythum-project.com
//
//	Date			: 16/07/2003
//	Modification	:
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------
#ifndef	__TIMER_H__
#define	__TIMER_H__

#include "Types.h"

typedef u32					KTIME;

//---------------------------------------------------------------------------------------------------------------------
class KTimer
{
protected:
	s64				m_Frequency;
#ifdef _LINUX
	s64				m_StartTime;
#endif // _LINUX	

public:
	KTimer();

	KTIME			GetTime();

};

//---------------------------------------------------------------------------------------------------------------------
extern KTimer	g_Timer;

#endif // __TIMER_H__
