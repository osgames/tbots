//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: Light.h
//	Author			: - Aitonfrere	
//					  - Dino			dino@zythum-project.com
//
//	Date			: 11/08/2003
//	Modification	:
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------
#ifndef	__LIGHT_H__
#define	__LIGHT_H__

#include "Types.h"
#include "Vector.h"

class KRender;

//extern u32 g_LightNextIndex;

//--------------------------------------------------------------------------------------------
typedef enum _KLIGHTTYPE
{
	KLT_POINT,
	KLT_SPOT,
	KLT_DIRECTIONAL,
} KLIGHTTYPE;

//--------------------------------------------------------------------------------------------
class KLight
{
protected:
	KRender*		m_pRender;
	s32				m_Id;
	KLIGHTTYPE		m_Type;
	KFColor			m_Ambient;
	KFColor			m_Diffuse;
	KFColor			m_Specular;
	KVector			m_Position;
	KVector			m_Direction;

public:
	KLight( KRender* pRender );
	~KLight();

	void			Enable( bool bEnable );
	
	void			SetType( KLIGHTTYPE Type );
	void			SetPosition( KVector& Position );
	void			SetDirection( KVector& Direction );
	void			SetAmbient( KCOLOR Ambient );
	void			SetDiffuse( KCOLOR Diffuse );

	KLIGHTTYPE		GetType()					{ return m_Type;	}
};

#endif // __LIGHT_H__
