//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: Types.h
//	Author			: - Aitonfrere	
//					  - Dino			dino@zythum-project.com
//
//	Date			: 16/07/2003
//	Modification	:
//  [17/07/2003 Aitonfrere] : additional definitons
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------
#ifndef	__TYPES_H__
#define	__TYPES_H__

#ifdef _WIN32
#include <windows.h>
#include <crtdbg.h>
#endif /// _WIN32

#ifdef _LINUX
#include <stdarg.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#endif // _LINUX

#include <math.h>
#include <stdio.h>

//--------------------------------------------------------------------------------------------
#ifdef	_LINUX
#define stricmp	strcasecmp
#endif // _LINUX

//--------------------------------------------------------------------------------------------
#define	PI					3.1415926535897932384626433832795028841971693993751f

//--------------------------------------------------------------------------------------------
typedef unsigned char		u8;
typedef signed char		s8;
typedef unsigned short		u16;
typedef signed short		s16;
typedef unsigned int		u32;
typedef signed int		s32;
#ifdef _WIN32
typedef unsigned __int64	u64;
typedef signed __int64		s64;
#endif // _WIN32

#ifdef _LINUX
typedef unsigned long long	u64;
typedef signed long long	s64;
#endif // _LINUX

//--------------------------------------------------------------------------------------------
typedef	u32							KCOLOR;

//--------------------------------------------------------------------------------------------
typedef u32							KRESULT;
#define	KR_SUCCESS( __Result )		(__Result == KR_OK)
#define	KR_FAILED( __Result )		(__Result != KR_OK)

#define	KR_OK						((KRESULT)0x00000000)
#define	KR_ERROR					((KRESULT)0x00000001)
#define	KR_WINDOWFAILED				((KRESULT)0x00000002)

#define	KR_BADDXVERSION				((KRESULT)0x00000010)
#define	KR_UNSUPPORTEDBPP			((KRESULT)0x00000011)
#define	KR_DEVICEFAILED				((KRESULT)0x00000012)
#define	KR_BUFFERFAILED				((KRESULT)0x00000013)
#define	KR_FLIPFAILED				((KRESULT)0x00000014)
#define	KR_TEXTUREFAILED			((KRESULT)0x00000015)
#define	KR_MESHFAILED				((KRESULT)0x00000016)


//--------------------------------------------------------------------------------------------
#ifdef _WIN32
#define	KASSERT( __Exp )			_ASSERT( __Exp )
#endif // _WIN32
#ifdef _LINUX
#define	KASSERT( __Exp )			assert( __Exp )
#endif // _LINUX

#define KERROR						KError::Error
/*
#define	KRGB(r, g, b)				((int) (((255) << 24) | ((r) << 16) | ((g) << 8) | (b)))
#define	KRGBA(r, g, b, a)			((int) (((a) << 24) | ((r) << 16) | ((g) << 8) | (b)))
#define	KGETA(color)				((color & 0xFF000000) >> 24)
#define	KGETR(color)				((color & 0x00FF0000) >> 16)
#define	KGETG(color)				((color & 0x0000FF00) >> 8)
#define	KGETB(color)				(color & 0x000000FF)
*/
#define	KRGB(r, g, b)				((int) (((255) << 24) | ((b) << 16) | ((g) << 8) | (r)))
#define	KRGBA(r, g, b, a)			((int) (((a) << 24) | ((b) << 16) | ((g) << 8) | (r)))
#define	KGETA(color)				((color & 0xFF000000) >> 24)
#define	KGETB(color)				((color & 0x00FF0000) >> 16)
#define	KGETG(color)				((color & 0x0000FF00) >> 8)
#define	KGETR(color)				(color & 0x000000FF)

#define	Freep( x )					free( x ); x = NULL
#define	Deletep( x )				delete x; x = NULL
#define	Deletev( x )				delete[] x; x = NULL

#define	SQR( x )					((x)*(x))
#define	MIN( a, b )					((a < b) ? a : b)
#define	MAX( a, b )					((a > b) ? a : b)

//--------------------------------------------------------------------------------------------

//  [17/07/2003 Aitonfrere] : additional definitons

// Safe Release macros

#define SafeFree(_p)						if (_p) { free (_p);		(_p) = NULL; }
#define SafeDelete(_p)						if (_p) { delete (_p);		(_p) = NULL; }
#define SafeDeleteArray(_a)					if (_a) { delete[] (_a);	(_a) = NULL; }
#define SafeRelease(_i)						if (_i) { (_i)->Release();	(_i) = NULL; }
#define SafeFreeLibrary(_l)					if (_l) { FreeLibrary(_l);	_l = NULL; }

// Error code control

#define CheckFailed(_c,_ret)				if (FAILED(_c)) return (_ret)
#define CheckFailedMsg(_c,_ret,_msg)		if (FAILED(_c)) { ErrorMsg(_msg); return (_ret); }

//--------------------------------------------------------------------------------------------
class KFColor
{
public:
	float	m_R;
	float	m_G;
	float	m_B;
	float	m_A;
};

//--------------------------------------------------------------------------------------------
class KRect
{
public:
	s32	x1;
	s32	y1;
	s32	x2;
	s32	y2;

	inline KRect()
	{
		x1 = 0;
		y1 = 0;
		x2 = 0;
		y2 = 0;
	}

	inline KRect( s32 x1, s32 y1, s32 x2, s32 y2 )
	{
		this->x1 = x1;
		this->y1 = y1;
		this->x2 = x2;
		this->y2 = y2;
	}
};


//--------------------------------------------------------------------------------------------
class KFPt
{
public:
	float	x;
	float	y;

	inline KFPt()
	{
		x = 0;
		y = 0;
	}

	inline KFPt( float x, float y )
	{
		this->x = x;
		this->y = y;
	}
};

//--------------------------------------------------------------------------------------------
class KPt
{
public:
	s32	x;
	s32	y;

	KPt()
	{
		x = 0;
		y = 0;
	}

	KPt( s32 x, s32 y )
	{
		this->x = x;
		this->y = y;
	}

	KPt operator +( KPt Point )
	{
		KPt	NewPoint;

		NewPoint.x = x + Point.x;
		NewPoint.y = y + Point.y;

		return NewPoint;
	}

	KPt operator * ( KPt Point )
	{
		KPt	NewPoint;

		NewPoint.x = x * Point.x;
		NewPoint.y = y * Point.y;

		return NewPoint;
	}

	KPt operator * ( s32 Value )
	{
		KPt	NewPoint;

		NewPoint.x = x * Value;
		NewPoint.y = y * Value;

		return NewPoint;
	}

	KPt( KFPt Point )
	{
		this->x = (s32)Point.x;
		this->y = (s32)Point.y;
	}

	bool operator != ( KPt Point )
	{
		return ( ( x != Point.x ) || ( y != Point.y ) );
	}

	bool operator == ( KPt Point )
	{
		return ( ( x == Point.x ) && ( y == Point.y ) );
	}

	s32 SquareDistance( KPt Point )
	{
		return ( SQR( x - Point.x ) + SQR( y - Point.y ) );
	}

	float Distance( KPt Point )
	{
		return sqrtf( (float)SquareDistance( Point ) );
	}
};

//--------------------------------------------------------------------------------------------
class KError
{
public:
	static void Error( const char *pFormat, ... )
	{
		char	pBuffer[1024];
		va_list	arg_ptr;

		va_start( arg_ptr, pFormat );
#ifdef _WIN32
		_vsnprintf( pBuffer, sizeof( pBuffer ), pFormat, arg_ptr );
		MessageBox( GetActiveWindow(), pBuffer, "Error", MB_ICONSTOP );
#endif // _WIN32

#ifdef _LINUX
		vsnprintf( pBuffer, sizeof( pBuffer ), pFormat, arg_ptr );
		printf( "%s\n", pBuffer );
#endif // _LINUX
	}

};


//---------------------------------------------------------------------------------------------------------------------
class KListElement
{
public:
	void*			m_pElement;
	KListElement*	m_pPrev;
	KListElement*	m_pNext;
};


template <class TYPE>
class KList
{
protected:
	KListElement*	m_pHead;
	KListElement*	m_pTail;
	u32				m_Size;

public:
	KList()
	{
		m_pHead	= NULL;
		m_pTail	= NULL;
		m_Size	= 0;
	}

	~KList()
	{
	}

	// Efface tout les �ements
	void			Clear()
	{
		KListElement*	pElement = m_pHead;
		KListElement*	pNext;

		while( pElement )
		{
			pNext = pElement->m_pNext;
			SafeDelete( pElement );
			pElement = pNext;
		}

		m_pHead = NULL;
		m_pTail = NULL;
	}

	// Ajoute un nouvel �ement en queue de liste
	void			Add( TYPE pData )
	{
		KListElement*	pElement = new KListElement();
		pElement->m_pElement	= (void*)pData;
		pElement->m_pPrev		= m_pTail;
		pElement->m_pNext		= NULL;
		if( m_pTail )
			m_pTail->m_pNext	= pElement;

		m_pTail					= pElement;

		if( !m_pHead )
			m_pHead = pElement;

		m_Size ++;
	}

	// Retire l'��ent de la liste
	void			Remove( TYPE pData )
	{
		KListElement*	pElement = m_pHead;

		while( pElement )
		{
			if( pElement->m_pElement == (void*)pData )
			{
				if( pElement->m_pPrev )
					pElement->m_pPrev->m_pNext = pElement->m_pNext;
				else
					m_pHead = pElement->m_pNext;

				if( pElement->m_pNext )
					pElement->m_pNext->m_pPrev = pElement->m_pPrev;
				else
					m_pTail = pElement->m_pPrev;

				SafeDelete( pElement );

				m_Size --;

				return;
			}
			pElement = pElement->m_pNext;
		}
	}

	u32				GetSize()				{ return m_Size;												}
	TYPE			GetFirst()				{ return (m_pHead ? (TYPE)m_pHead->m_pElement : (TYPE)NULL);	}
	TYPE			GetLast()				{ return (m_pTail ? (TYPE)m_pTail->m_pElement : (TYPE)NULL);	}

	TYPE			GetNext( TYPE pData )
	{
		KListElement*	pElement = m_pHead;

		while( pElement )
		{
			if( pElement->m_pElement == (void*)pData )
				return (pElement->m_pNext ? (TYPE)pElement->m_pNext->m_pElement : (TYPE)NULL );

			pElement = pElement->m_pNext;
		}
		return (TYPE)NULL;
	}

	TYPE			GetPosition( u32 Position )
	{
		TYPE	Element = GetFirst();

		while( Position -- )
		{
			Element = GetNext( Element );
		}

		return Element;
	}
};

#endif // __TYPES_H__
