//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: Vector.h
//	Author			: - Aitonfrere	
//					  - Dino			dino@zythum-project.com
//
//	Date			: 11/08/2003
//	Modification	:
//  [17/07/2003 Aitonfrere] : additional definitons
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------
#ifndef __VECTOR_H__
#define __VECTOR_H__

#include <math.h>

//---------------------------------------------------------------------------------------------------------------------
class KVector
{
public:
	float	x;
	float	y;
	float	z;


	/////////////////////////////////////////////////////////////////////////////////////////
	// Constructeur
	/////////////////////////////////////////////////////////////////////////////////////////
	inline KVector()
	{
/*		x = 0;
		y = 0;
		z = 0;*/
	}
	
	inline KVector( float x, float y, float z )
	{
		this->x	= x;
		this->y	= y;
		this->z	= z;
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////
	// Operateur
	/////////////////////////////////////////////////////////////////////////////////////////
	inline KVector operator + ( KVector& v )
	{
		KVector result;
	
		result.x	= x + v.x;
		result.y	= y + v.y;
		result.z	= z + v.z;
	
		return result;
	}
	
	inline KVector operator - ( KVector& v )
	{
		KVector result;
	
		result.x	= x - v.x;
		result.y	= y - v.y;
		result.z	= z - v.z;
	
		return result;
	}
	
	inline KVector operator * ( KVector& v )
	{
		KVector result;
	
		result.x	= x * v.x;
		result.y	= y * v.y;
		result.z	= z * v.z;
	
		return result;
	}
	
	inline KVector operator / ( KVector& v )
	{
		KVector result;
	
		result.x	= x / v.x;
		result.y	= y / v.y;
		result.z	= z / v.z;
	
		return result;
	}
	
	inline KVector operator - ( )
	{
		KVector result;
	
		result.x	= -x;
		result.y	= -y;
		result.z	= -z;
	
		return result;
	}
	
	inline void operator += ( KVector& v )
	{
		x += v.x;
		y += v.y;
		z += v.z;
	}
	
	inline void operator -= ( KVector& v )
	{
		x -= v.x;
		y -= v.y;
		z -= v.z;
	}

	inline void operator *= ( KVector& v )
	{
		x *= v.x;
		y *= v.y;
		z *= v.z;
	}

	inline void operator /= ( KVector& v )
	{
		x /= v.x;
		y /= v.y;
		z /= v.z;
	}


	/////////////////////////////////////////////////////////////////////////////////////////
	inline KVector operator * ( float f )
	{
		KVector result;
	
		result.x	= x * f;
		result.y	= y * f;
		result.z	= z * f;
	
		return result;
	}
	
	inline void operator *= ( float f )
	{
		x *= f;
		y *= f;
		z *= f;
	}

	inline KVector operator / ( float f )
	{
		KVector result;

		result.x	= x / f;
		result.y	= y / f;
		result.z	= z / f;
	
		return result;
	}

	inline void operator /= ( float f )
	{
		x /= f;
		y /= f;
		z /= f;
	}

	inline bool operator == ( KVector& v )
	{
		return ( ( x == v.x ) && ( y == v.y ) && ( z == v.z ) );
	}
	
	inline bool operator != ( KVector& v )
	{
		return ( ( x != v.x ) || ( y != v.y ) || ( z != v.z ) );
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////
	// Fonctions vecteurs
	/////////////////////////////////////////////////////////////////////////////////////////
	inline void Init( float x, float y, float z )
	{
		this->x = x;
		this->y = y;
		this->z = z;
	}

	inline static KVector CrossProduct( KVector& v1, KVector& v2 )
	{
		KVector	result;
	
		result.x	= v1.y * v2.z - v1.z * v2.y;
		result.y	= v1.z * v2.x - v1.x * v2.z;
		result.z	= v1.x * v2.y - v1.y * v2.x;
	
		return result;
	}

	// DotProduct
	inline static float DotProduct( KVector& v1, KVector& v2 )
	{
		return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
	}
	
	inline double DotProduct( KVector& v )
	{
		return x * v.x + y * v.y + z * v.z;
	}
	
	inline float DotProductf( KVector& v )
	{
		return x * v.x + y * v.y + z * v.z;
	}
	
	inline static float SquareDistance( KVector& v1, KVector& v2 )
	{
		return (v1.x - v2.x) * (v1.x - v2.x) + (v1.y - v2.y) * (v1.y - v2.y) + (v1.z - v2.z) * (v1.z - v2.z);
	}

	inline static float Distance( KVector& v1, KVector& v2 )
	{
		return sqrtf( SquareDistance( v1, v2 ) );
	}

	inline float SquareMagnitude()
	{
		return x * x + y * y + z * z;
	}

	inline float Magnitude()
	{
		return sqrtf( SquareMagnitude() );
	}

	inline void Normalize()
	{
		float magnitude = Magnitude();
		
		if( !magnitude )
			return;

		x = x / magnitude;
		y = y / magnitude;
		z = z / magnitude;
	}

	inline void SetLength( float Length )
	{
		Normalize();
		x *= Length;
		y *= Length;
		z *= Length;
	}

};

#endif // __VECTOR_H__
