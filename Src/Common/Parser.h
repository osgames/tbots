//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: Parser.h
//	Author			: - Dino			dino@zythum-project.com
//
//	Date			: 12/12/2003
//	Modification	:
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------
#ifndef	__PARSER_H__
#define	__PARSER_H__

#include "Types.h"

#define	MAXTOKEN	1024

//--------------------------------------------------------------------------------------------------------------------------------
class KParser
{
protected:
	char*			m_pBuffer;
	int				m_bIsNewLine;
	u32				m_nSize;
	u32				m_nCurrentPos;
	char			m_pToken[MAXTOKEN];
	u32				m_nLine;

public:
					KParser();
					KParser( char* pFileName );
	virtual			~KParser();

	bool			LoadASE( char* pFileName );
	void			Clean();

	bool			SearchToken( char* pToken );
	void			FindToken();
	char*			GetToken();
	int				IsEOF()				{ return m_nCurrentPos >= m_nSize;		}
	int				IsNewLine()			{ return m_bIsNewLine;					}
	u32				GetCurrentLine()	{ return m_nLine;						}
};

#endif	// __PARSER_H__
