//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: Camera.cpp
//	Author			: - Aitonfrere	
//					  - Dino			dino@zythum-project.com
//
//	Date			: 31/07/2003
//	Modification	:
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------
#include <math.h>
#include "Camera.h"

//--------------------------------------------------------------------------------------------
KCamera::KCamera()
{
	m_Matrix.LoadIdentity();
}

//--------------------------------------------------------------------------------------------
void KCamera::Translate( float x, float y, float z )
{
	m_Matrix.Trans( x, y, z );
}

//--------------------------------------------------------------------------------------------
void KCamera::RotateX( float Angle )
{
	m_Matrix.RotX( Angle );
}

//--------------------------------------------------------------------------------------------
void KCamera::RotateY( float Angle )
{
	m_Matrix.RotY( Angle );
}

//--------------------------------------------------------------------------------------------
void KCamera::RotateZ( float Angle )
{
	m_Matrix.RotZ( Angle );
}

//--------------------------------------------------------------------------------------------
KMatrix KCamera::GetMatrix()
{
	return m_Matrix;
}
