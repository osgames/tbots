//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: Mine.cpp
//	Author			: - Aitonfrere	
//					  - Dino			dino@zythum-project.com
//
//	Date			: 21/07/2003
//	Modification	:
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------
#include "Render.h"
#include "Mesh.h"
#include "Level.h"
#include "Timer.h"
#include "Mine.h"

//--------------------------------------------------------------------------------------------
KMesh*	KMine::m_pMesh = NULL;

//--------------------------------------------------------------------------------------------
KMine::KMine( KRender* pRender, KLevel* pLevel )
: KItem( pRender, pLevel )
{
	m_Type			= KIT_MINE;
	m_State			= KMS_DESACTIVATED;
	m_bTakeAway		= true;
}

//--------------------------------------------------------------------------------------------
KMine::~KMine()
{
}

//--------------------------------------------------------------------------------------------
bool KMine::Load( KRender* pRender )
{
	pRender->LoadMesh( "Meshes/mine", &m_pMesh );

	return true;
}

//--------------------------------------------------------------------------------------------
void KMine::Unload()
{
	SafeDelete( m_pMesh );
}

//--------------------------------------------------------------------------------------------
void KMine::Display2D( KPt& Pos, KPt& Size )
{
	float	pTU[4], pTV[4];

	// Affiche la mine
	pTU[0] = 0.375f;
	pTU[1] = 0.5f;
	pTU[2] = 0.375f;
	pTU[3] = 0.5f;

	pTV[0] = 0.75f;
	pTV[1] = 0.75f;
	pTV[2] = 0.875f;
	pTV[3] = 0.875f;

	// Effet top d�lire
	u8		Intensity = u8( fabs( sinf( (float)g_Timer.GetTime() / 200.0f ) * 16.0f ) );
	KCOLOR	Specular = KRGBA( Intensity, Intensity, Intensity, 255 );

	if( m_State != KMS_DESACTIVATED )
	{
		Intensity = u8( fabs( sinf( (float)g_Timer.GetTime() / 200.0f ) * 160.0f ) );
		Specular = KRGBA( Intensity, 0, 0, 255 );
		m_pRender->DrawQuad( Pos.x, Pos.y, Size.x, Size.y, m_pLevel->GetpShader(),KRGBA( 255, Intensity, Intensity, 255), Specular, KRM_NORMAL, pTU, pTV );

	}
	else
		m_pRender->DrawQuad( Pos.x, Pos.y, Size.x, Size.y, m_pLevel->GetpShader(),KRGBA( 255, 255, 255, 255), Specular, KRM_NORMAL, pTU, pTV );
}

//--------------------------------------------------------------------------------------------
void KMine::SetState( KMINESTATE State )
{
	m_State = State;

	switch( m_State )
	{
	case KMS_DESACTIVATED:
		break;
	case KMS_PLANTED:
		m_bTakeAway		= false;		// On ne peut prendre une mine plant�e
		m_bVisible		= false;		// Une mine plant�e devient insivible
		break;
	case KMS_ACTIVATED:
		break;
	case KMS_EXPLODED:
		break;
	default:
		break;
	}
}

//--------------------------------------------------------------------------------------------
void KMine::Display( KPt& Pos )
{
	float	Angle = ((float)g_Timer.GetTime() / 500.0f);

	SetMatrix( Pos, 9.0f, Angle, 0.0f, Angle );
	m_pRender->DrawMesh( m_Matrix, m_pMesh );
}

