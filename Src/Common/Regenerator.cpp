//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: Regenerator.cpp
//	Author			: - Aitonfrere	
//					  - Dino			dino@zythum-project.com
//
//	Date			: 20/07/2003
//	Modification	:
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------
#include "Render.h"
#include "Mesh.h"
#include "Level.h"
#include "Timer.h"
#include "Regenerator.h"

//--------------------------------------------------------------------------------------------
KMesh*	KRegenerator::m_pMesh = NULL;

//--------------------------------------------------------------------------------------------
KRegenerator::KRegenerator( KRender* pRender, KLevel* pLevel )
: KItem( pRender, pLevel )
{
	m_Type		= KIT_REGENERATOR;
	m_RegenLife	= 20;
}

//--------------------------------------------------------------------------------------------
KRegenerator::~KRegenerator()
{
}

//--------------------------------------------------------------------------------------------
bool KRegenerator::Load( KRender* pRender )
{
	pRender->LoadMesh( "Meshes/regenerator", &m_pMesh );

	return true;
}

//--------------------------------------------------------------------------------------------
void KRegenerator::Unload()
{
	SafeDelete( m_pMesh );
}

//--------------------------------------------------------------------------------------------
void KRegenerator::Display2D( KPt& Pos, KPt& Size )
{
	float	pTU[4], pTV[4];

	// Affiche le regenerator
	pTU[0] = 0.125f;
	pTU[1] = 0.250f;
	pTU[2] = 0.125f;
	pTU[3] = 0.250f;

	pTV[0] = 0.75f;
	pTV[1] = 0.75f;
	pTV[2] = 0.875f;
	pTV[3] = 0.875f;

	// Effet top d�lire
	u8		Intensity = u8( fabs( sinf( (float)g_Timer.GetTime() / 200.0f ) * 16.0f ) );
	KCOLOR	Specular = KRGBA( Intensity, Intensity, Intensity, 255 );

	m_pRender->DrawQuad( Pos.x, Pos.y, Size.x, Size.y, m_pLevel->GetpShader(), KRGBA( 255, 255, 255, 255 ), Specular, KRM_NORMAL, pTU, pTV );
}

//--------------------------------------------------------------------------------------------
void KRegenerator::Display( KPt& Pos )
{
	float	Angle = ((float)g_Timer.GetTime() / 500.0f);

	SetMatrix( Pos, 0.0f, 0.0f, Angle, 0.0f );
	m_pRender->DrawMesh( m_Matrix, m_pMesh );
}
