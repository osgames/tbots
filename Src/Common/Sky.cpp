//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: Sky.cpp
//	Author			: - Aitonfrere	
//					  - Dino			dino@zythum-project.com
//
//	Date			: 04/08/2003
//	Modification	:
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------
#include "Render.h"
#include "Shader.h"
#include "Sky.h"

//--------------------------------------------------------------------------------------------
KSky::KSky( KRender* pRender, float Size )
{
	m_pRender	= pRender;
	m_Size		= Size;

	m_pRender->LoadShader( "Textures/sky_up.png",		&m_pShader[0] );
	m_pRender->LoadShader( "Textures/sky_down.png",		&m_pShader[1] );
	m_pRender->LoadShader( "Textures/sky_forward.png",	&m_pShader[2] );
	m_pRender->LoadShader( "Textures/sky_back.png",		&m_pShader[3] );
	m_pRender->LoadShader( "Textures/sky_left.png",		&m_pShader[4] );
	m_pRender->LoadShader( "Textures/sky_right.png",	&m_pShader[5] );
}

//--------------------------------------------------------------------------------------------
KSky::~KSky()
{
	SafeDelete( m_pShader[0] );
	SafeDelete( m_pShader[1] );
	SafeDelete( m_pShader[2] );
	SafeDelete( m_pShader[3] );
	SafeDelete( m_pShader[4] );
	SafeDelete( m_pShader[5] );
}

//--------------------------------------------------------------------------------------------
void KSky::Draw()
{
	KRenderFace		Face;
	KVertex			pVertex[4];
	KINDEX			pIndex[6] = { 0, 1, 2, 2, 1, 3 };
	float			pTU[4];
	float			pTV[4];
	float			dtexelu = 0.5f / (float)m_pShader[0]->GetShaderInfo().m_Width;
	float			dtexelv = 0.5f / (float)m_pShader[0]->GetShaderInfo().m_Height;
	float			s = m_Size / 5.0f;

	pTU[0] = 0.0f + dtexelu;
	pTU[1] = 1.0f - dtexelu;
	pTU[2] = 0.0f + dtexelu;
	pTU[3] = 1.0f - dtexelu;

	pTV[0] = 0.0f + dtexelv;
	pTV[1] = 0.0f + dtexelv;
	pTV[2] = 1.0f - dtexelv;
	pTV[3] = 1.0f - dtexelv;

	m_pRender->ActivateLighting( false );

	// Up
	pVertex[3].x		= -s;
	pVertex[3].y		= s;
	pVertex[3].z		= s;
//	pVertex[3].diffuse	= KRGB( 255, 255, 255 );
	pVertex[3].nx		= 0.0f;
	pVertex[3].ny		= 0.0f;
	pVertex[3].nz		= -1.0f;
	pVertex[3].tu		= pTU[0];
	pVertex[3].tv		= pTV[0];
					
	pVertex[2].x		= s;
	pVertex[2].y		= s;
	pVertex[2].z		= s;
//	pVertex[2].diffuse	= KRGB( 255, 255, 255 );
	pVertex[2].nx		= 0.0f;
	pVertex[2].ny		= 0.0f;
	pVertex[2].nz		= -1.0f;
	pVertex[2].tu		= pTU[1];
	pVertex[2].tv		= pTV[1];

	pVertex[1].x		= -s;
	pVertex[1].y		= -s;
	pVertex[1].z		= s;
//	pVertex[1].diffuse	= KRGB( 255, 255, 255 );
	pVertex[1].nx		= 0.0f;
	pVertex[1].ny		= 0.0f;
	pVertex[1].nz		= -1.0f;
	pVertex[1].tu		= pTU[2];
	pVertex[1].tv		= pTV[2];
					
	pVertex[0].x		= s;
	pVertex[0].y		= -s;
	pVertex[0].z		= s;
//	pVertex[0].diffuse	= KRGB( 255, 255, 255 );
	pVertex[0].nx		= 0.0f;
	pVertex[0].ny		= 0.0f;
	pVertex[0].nz		= -1.0f;
	pVertex[0].tu		= pTU[3];
	pVertex[0].tv		= pTV[3];
						
	Face.m_pVertex	= pVertex;
	Face.m_nVertex	= 4;
	Face.m_pIndices	= pIndex;
	Face.m_nIndices	= 6;

	m_pRender->SetShader( m_pShader[0] );
	m_pRender->DrawTriangles( Face );
	m_pRender->FlushTriangles();

	// Down
	pVertex[0].x		= -s;
	pVertex[0].y		= -s;
	pVertex[0].z		= -s;
//	pVertex[0].diffuse	= KRGB( 255, 255, 255 );
	pVertex[0].nx		= 0.0f;
	pVertex[0].ny		= 0.0f;
	pVertex[0].nz		= 1.0f;
	pVertex[0].tu		= pTU[0];
	pVertex[0].tv		= pTV[0];
					
	pVertex[1].x		= s;
	pVertex[1].y		= -s;
	pVertex[1].z		= -s;
//	pVertex[1].diffuse	= KRGB( 255, 255, 255 );
	pVertex[1].nx		= 0.0f;
	pVertex[1].ny		= 0.0f;
	pVertex[1].nz		= 1.0f;
	pVertex[1].tu		= pTU[1];
	pVertex[1].tv		= pTV[1];

	pVertex[2].x		= -s;
	pVertex[2].y		= s;
	pVertex[2].z		= -s;
//	pVertex[2].diffuse	= KRGB( 255, 255, 255 );
	pVertex[2].nx		= 0.0f;
	pVertex[2].ny		= 0.0f;
	pVertex[2].nz		= 1.0f;
	pVertex[2].tu		= pTU[2];
	pVertex[2].tv		= pTV[2];
					
	pVertex[3].x		= s;
	pVertex[3].y		= s;
	pVertex[3].z		= -s;
//	pVertex[3].diffuse	= KRGB( 255, 255, 255 );
	pVertex[3].nx		= 0.0f;
	pVertex[3].ny		= 0.0f;
	pVertex[3].nz		= 1.0f;
	pVertex[3].tu		= pTU[3];
	pVertex[3].tv		= pTV[3];
						
	Face.m_pVertex	= pVertex;
	Face.m_nVertex	= 4;
	Face.m_pIndices	= pIndex;
	Face.m_nIndices	= 6;

	m_pRender->SetShader( m_pShader[1] );
	m_pRender->DrawTriangles( Face );
	m_pRender->FlushTriangles();

	// Forward
	pVertex[0].x		= -s;
	pVertex[0].y		= -s;
	pVertex[0].z		= s;
//	pVertex[0].diffuse	= KRGB( 255, 255, 255 );
	pVertex[0].nx		= 0.0f;
	pVertex[0].ny		= -1.0f;
	pVertex[0].nz		= 0.0f;
	pVertex[0].tu		= pTU[0];
	pVertex[0].tv		= pTV[0];
					
	pVertex[1].x		= s;
	pVertex[1].y		= -s;
	pVertex[1].z		= s;
//	pVertex[1].diffuse	= KRGB( 255, 255, 255 );
	pVertex[1].nx		= 0.0f;
	pVertex[1].ny		= -1.0f;
	pVertex[1].nz		= 0.0f;
	pVertex[1].tu		= pTU[1];
	pVertex[1].tv		= pTV[1];

	pVertex[2].x		= -s;
	pVertex[2].y		= -s;
	pVertex[2].z		= -s;
//	pVertex[2].diffuse	= KRGB( 255, 255, 255 );
	pVertex[2].nx		= 0.0f;
	pVertex[2].ny		= -1.0f;
	pVertex[2].nz		= 0.0f;
	pVertex[2].tu		= pTU[2];
	pVertex[2].tv		= pTV[2];
					
	pVertex[3].x		= s;
	pVertex[3].y		= -s;
	pVertex[3].z		= -s;
//	pVertex[3].diffuse	= KRGB( 255, 255, 255 );
	pVertex[3].nx		= 0.0f;
	pVertex[3].ny		= -1.0f;
	pVertex[3].nz		= 0.0f;
	pVertex[3].tu		= pTU[3];
	pVertex[3].tv		= pTV[3];
						
	Face.m_pVertex	= pVertex;
	Face.m_nVertex	= 4;
	Face.m_pIndices	= pIndex;
	Face.m_nIndices	= 6;

	m_pRender->SetShader( m_pShader[2] );
	m_pRender->DrawTriangles( Face );
	m_pRender->FlushTriangles();

	// Back
	pVertex[0].x		= -s;
	pVertex[0].y		= s;
	pVertex[0].z		= -s;
//	pVertex[0].diffuse	= KRGB( 255, 255, 255 );
	pVertex[0].nx		= 0.0f;
	pVertex[0].ny		= 1.0f;
	pVertex[0].nz		= 0.0f;
	pVertex[0].tu		= pTU[3];
	pVertex[0].tv		= pTV[3];
					
	pVertex[1].x		= s;
	pVertex[1].y		= s;
	pVertex[1].z		= -s;
//	pVertex[1].diffuse	= KRGB( 255, 255, 255 );
	pVertex[1].nx		= 0.0f;
	pVertex[1].ny		= 1.0f;
	pVertex[1].nz		= 0.0f;
	pVertex[1].tu		= pTU[2];
	pVertex[1].tv		= pTV[2];

	pVertex[2].x		= -s;
	pVertex[2].y		= s;
	pVertex[2].z		= s;
//	pVertex[2].diffuse	= KRGB( 255, 255, 255 );
	pVertex[2].nx		= 0.0f;
	pVertex[2].ny		= 1.0f;
	pVertex[2].nz		= 0.0f;
	pVertex[2].tu		= pTU[1];
	pVertex[2].tv		= pTV[1];
					
	pVertex[3].x		= s;
	pVertex[3].y		= s;
	pVertex[3].z		= s;
//	pVertex[3].diffuse	= KRGB( 255, 255, 255 );
	pVertex[3].nx		= 0.0f;
	pVertex[3].ny		= 1.0f;
	pVertex[3].nz		= 0.0f;
	pVertex[3].tu		= pTU[0];
	pVertex[3].tv		= pTV[0];
						
	Face.m_pVertex	= pVertex;
	Face.m_nVertex	= 4;
	Face.m_pIndices	= pIndex;
	Face.m_nIndices	= 6;

	m_pRender->SetShader( m_pShader[3] );
	m_pRender->DrawTriangles( Face );
	m_pRender->FlushTriangles();

	// Left
	pVertex[0].x		= -s;
	pVertex[0].y		= -s;
	pVertex[0].z		= -s;
//	pVertex[0].diffuse	= KRGB( 255, 255, 255 );
	pVertex[0].nx		= 1.0f;
	pVertex[0].ny		= 0.0f;
	pVertex[0].nz		= 0.0f;
	pVertex[0].tu		= pTU[3];
	pVertex[0].tv		= pTV[3];
					
	pVertex[1].x		= -s;
	pVertex[1].y		= s;
	pVertex[1].z		= -s;
//	pVertex[1].diffuse	= KRGB( 255, 255, 255 );
	pVertex[1].nx		= 1.0f;
	pVertex[1].ny		= 0.0f;
	pVertex[1].nz		= 0.0f;
	pVertex[1].tu		= pTU[2];
	pVertex[1].tv		= pTV[2];

	pVertex[2].x		= -s;
	pVertex[2].y		= -s;
	pVertex[2].z		= s;
//	pVertex[2].diffuse	= KRGB( 255, 255, 255 );
	pVertex[2].nx		= 1.0f;
	pVertex[2].ny		= 0.0f;
	pVertex[2].nz		= 0.0f;
	pVertex[2].tu		= pTU[1];
	pVertex[2].tv		= pTV[1];
					
	pVertex[3].x		= -s;
	pVertex[3].y		= s;
	pVertex[3].z		= s;
//	pVertex[3].diffuse	= KRGB( 255, 255, 255 );
	pVertex[3].nx		= 1.0f;
	pVertex[3].ny		= 0.0f;
	pVertex[3].nz		= 0.0f;
	pVertex[3].tu		= pTU[0];
	pVertex[3].tv		= pTV[0];
						
	Face.m_pVertex	= pVertex;
	Face.m_nVertex	= 4;
	Face.m_pIndices	= pIndex;
	Face.m_nIndices	= 6;

	m_pRender->SetShader( m_pShader[4] );
	m_pRender->DrawTriangles( Face );
	m_pRender->FlushTriangles();

	// Right
	pVertex[0].x		= s;
	pVertex[0].y		= -s;
	pVertex[0].z		= s;
//	pVertex[0].diffuse	= KRGB( 255, 255, 255 );
	pVertex[0].nx		= -1.0f;
	pVertex[0].ny		= 0.0f;
	pVertex[0].nz		= 0.0f;
	pVertex[0].tu		= pTU[0];
	pVertex[0].tv		= pTV[0];
					
	pVertex[1].x		= s;
	pVertex[1].y		= s;
	pVertex[1].z		= s;
//	pVertex[1].diffuse	= KRGB( 255, 255, 255 );
	pVertex[1].nx		= -1.0f;
	pVertex[1].ny		= 0.0f;
	pVertex[1].nz		= 0.0f;
	pVertex[1].tu		= pTU[1];
	pVertex[1].tv		= pTV[1];

	pVertex[2].x		= s;
	pVertex[2].y		= -s;
	pVertex[2].z		= -s;
//	pVertex[2].diffuse	= KRGB( 255, 255, 255 );
	pVertex[2].nx		= -1.0f;
	pVertex[2].ny		= 0.0f;
	pVertex[2].nz		= 0.0f;
	pVertex[2].tu		= pTU[2];
	pVertex[2].tv		= pTV[2];
					
	pVertex[3].x		= s;
	pVertex[3].y		= s;
	pVertex[3].z		= -s;
//	pVertex[3].diffuse	= KRGB( 255, 255, 255 );
	pVertex[3].nx		= -1.0f;
	pVertex[3].ny		= 0.0f;
	pVertex[3].nz		= 0.0f;
	pVertex[3].tu		= pTU[3];
	pVertex[3].tv		= pTV[3];
						
	Face.m_pVertex	= pVertex;
	Face.m_nVertex	= 4;
	Face.m_pIndices	= pIndex;
	Face.m_nIndices	= 6;

	m_pRender->SetShader( m_pShader[5] );
	m_pRender->DrawTriangles( Face );
	m_pRender->FlushTriangles();

//	m_pRender->ActivateLighting( false );
}
