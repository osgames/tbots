//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: Level.cpp
//	Author			: - Aitonfrere	
//					  - Dino			dino@zythum-project.com
//					  - Mac Load	
//
//	Date			: 16/07/2003
//	Modification	: 24/03/2004 by Mac Load
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------
#include "Shader.h"
#include "Level.h"
#include "Item.h"
#include "Bot.h"
#include "Mine.h"

//--------------------------------------------------------------------------------------------
KLevel::KLevel( KRender* pRender )
{
	m_Width			= 0;
	m_Height		= 0;
	m_pCell			= NULL;
	m_pSightCell	= NULL;
	m_pRender		= pRender;
	m_pSightItems	= NULL;

	m_pRender->LoadShader( "Tiles.png", &m_pShader );
}

//--------------------------------------------------------------------------------------------
KLevel::~KLevel()
{
	SafeDeleteArray( m_pSightItems );

	SafeDelete( m_pShader );
}

//--------------------------------------------------------------------------------------------
KRESULT KLevel::Create( u32 Width, u32 Height )
{
	Flush();

	KASSERT( !m_pCell );

	m_Width = Width;
	m_Height= Height;
	m_pCell = new KCell[m_Width * m_Height];
	memset( m_pCell, 0, m_Width * m_Height * sizeof( KCell ) );

	m_pSightCell = new KCell[m_Width * m_Height];
	memset( m_pSightCell, 0, m_Width * m_Height * sizeof( KCell ) );

	return KR_OK;
}

//--------------------------------------------------------------------------------------------
KRESULT KLevel::Flush()
{
	m_Width		= 0;
	m_Height	= 0;

	if( m_pCell )
		Deletev( m_pCell );

	if( m_pSightCell )
		Deletev( m_pSightCell );

	// D�truit tout les Items
	FlushItems();

	return KR_OK;
}

//--------------------------------------------------------------------------------------------
KRESULT KLevel::FlushItems()
{
	// D�truit tout les Items
	while( KItem* pItem = m_Items.GetFirst() )
	{
		m_Items.Remove( pItem );
		SafeDelete( pItem );
	}

	return KR_OK;
}

//--------------------------------------------------------------------------------------------
KRESULT KLevel::Load( char* pLevelName )
{
	KASSERT( !m_pCell );

	CellType Type;

	FILE*	pFile;
	KTBotLevelHeader Header;

	if( ( pFile = fopen( pLevelName, "rb" ) ) == NULL )
		return KR_ERROR;
	
	fread (&Header, sizeof(KTBotLevelHeader), 1, pFile);

	Create( Header.Width, Header.Height );
	
	for(u32 y = 0; y < Header.Height; y ++ )
	for(u32 x = 0; x < Header.Width; x ++ )
	{
		fread( &Type, sizeof( Type ), 1, pFile );
		GetpCell( x, y )->SetType( Type );
	}

	fclose( pFile );

	return KR_OK;
}

//--------------------------------------------------------------------------------------------
KRESULT KLevel::Unload()
{
	Flush();

	return KR_OK;
}

//--------------------------------------------------------------------------------------------
KRESULT KLevel::Draw()
{
	CellType Type;
	CellType TypeNoFlags;
	u16 Width = m_pShader->GetShaderInfo().m_Width/CELL_SIZE;

	float			pTU[4];
	float			pTV[4];
	float			dtexelu = 0.5f / (float)m_pShader->GetShaderInfo().m_Width;
	float			dtexelv = 0.5f / (float)m_pShader->GetShaderInfo().m_Height;

	KRenderFace		Face;
	KVertex			pVertex[4];
	KINDEX			pIndex[6] = { 0, 1, 2, 2, 1, 3 };

	bool			bFloor;

	m_pRender->ActivateLighting( true );
	m_pRender->SetShader( m_pShader );

	for( u32 y = 0; y < m_Height; y ++ )
	{
		for( u32 x = 0; x < m_Width; x ++ )
		{
			Type = GetpCell( x, y )->GetType();
			TypeNoFlags = Type & CELL_MASK_TYPE;

			bFloor = (Type == 0) ? true : false;

			if (Type & CELL_FLAG_FLIPH)
			{
				pTU[1] = pTU[3] = ((float) (TypeNoFlags % Width) * 0.125f) + dtexelu;
				pTU[0] = pTU[2] = ((float) (TypeNoFlags % Width) * 0.125f) + 0.125f - dtexelu;
			}
			else
			{
				pTU[0] = pTU[2] = ((float) (TypeNoFlags % Width) * 0.125f) + dtexelu;
				pTU[1] = pTU[3] = ((float) (TypeNoFlags % Width) * 0.125f) + 0.125f - dtexelu;
			}
			
			if (Type & CELL_FLAG_FLIPV)
			{
				pTV[0] = pTV[1] = ((float) ((TypeNoFlags / Width) * 0.125f)) + dtexelv;
				pTV[2] = pTV[3] = ((float) ((TypeNoFlags / Width) * 0.125f)) + 0.125f - dtexelv;
			}
			else
			{
				pTV[2] = pTV[3] = ((float) ((TypeNoFlags / Width) * 0.125f)) + dtexelv;
				pTV[0] = pTV[1] = ((float) ((TypeNoFlags / Width) * 0.125f)) + 0.125f - dtexelv;
			}

			float fy = (float)( m_Height - y );

			// Cell
			pVertex[0].x		= (float)x * CELL_SIZE;
			pVertex[0].y		= fy * CELL_SIZE;
			pVertex[0].z		= bFloor ? 0.0f : CELL_SIZE;
			pVertex[0].nx		= 0.0f;
			pVertex[0].ny		= 0.0f;
			pVertex[0].nz		= 1.0f;
			pVertex[0].tu		= pTU[0];
			pVertex[0].tv		= pTV[0];
							
			pVertex[1].x		= (float)(x+1) * CELL_SIZE;
			pVertex[1].y		= fy * CELL_SIZE;
			pVertex[1].z		= bFloor ? 0.0f : CELL_SIZE;
			pVertex[1].nx		= 0.0f;
			pVertex[1].ny		= 0.0f;
			pVertex[1].nz		= 1.0f;
			pVertex[1].tu		= pTU[1];
			pVertex[1].tv		= pTV[1];

			pVertex[2].x		= (float)x * CELL_SIZE;
			pVertex[2].y		= (fy+1.0f) * CELL_SIZE;
			pVertex[2].z		= bFloor ? 0.0f : CELL_SIZE;
			pVertex[2].nx		= 0.0f;
			pVertex[2].ny		= 0.0f;
			pVertex[2].nz		= 1.0f;
			pVertex[2].tu		= pTU[2];
			pVertex[2].tv		= pTV[2];
							
			pVertex[3].x		= (float)(x+1) * CELL_SIZE;
			pVertex[3].y		= (fy+1.0f) * CELL_SIZE;
			pVertex[3].z		= bFloor ? 0.0f : CELL_SIZE;
			pVertex[3].nx		= 0.0f;
			pVertex[3].ny		= 0.0f;
			pVertex[3].nz		= 1.0f;
			pVertex[3].tu		= pTU[3];
			pVertex[3].tv		= pTV[3];
								
			Face.m_pVertex	= pVertex;
			Face.m_nVertex	= 4;
			Face.m_pIndices	= pIndex;
			Face.m_nIndices	= 6;

			m_pRender->DrawTriangles( Face );

			if( !bFloor )
			{
				// Top
				KCell*	pTop = ( y < m_Height - 1 ) ? GetpCell( x, y + 1 ) : NULL;
				if( !pTop || (pTop->GetType() == 0) )
				{
					pVertex[0].x		= (float)x * CELL_SIZE;
					pVertex[0].y		= fy * CELL_SIZE;
					pVertex[0].z		= 0.0f;
					pVertex[0].nx		= 0.0f;
					pVertex[0].ny		= -1.0f;
					pVertex[0].nz		= 0.0f;
					pVertex[0].tu		= pTU[0];
					pVertex[0].tv		= pTV[0];
									
					pVertex[1].x		= (float)(x+1) * CELL_SIZE;
					pVertex[1].y		= fy * CELL_SIZE;
					pVertex[1].z		= 0.0f;
					pVertex[1].nx		= 0.0f;
					pVertex[1].ny		= -1.0f;
					pVertex[1].nz		= 0.0f;
					pVertex[1].tu		= pTU[1];
					pVertex[1].tv		= pTV[1];

					pVertex[2].x		= (float)x * CELL_SIZE;
					pVertex[2].y		= fy * CELL_SIZE;
					pVertex[2].z		= CELL_SIZE;
					pVertex[2].nx		= 0.0f;
					pVertex[2].ny		= -1.0f;
					pVertex[2].nz		= 0.0f;
					pVertex[2].tu		= pTU[2];
					pVertex[2].tv		= pTV[2];
									
					pVertex[3].x		= (float)(x+1) * CELL_SIZE;
					pVertex[3].y		= fy * CELL_SIZE;
					pVertex[3].z		= CELL_SIZE;
					pVertex[3].nx		= 0.0f;
					pVertex[3].ny		= -1.0f;
					pVertex[3].nz		= 0.0f;
					pVertex[3].tu		= pTU[3];
					pVertex[3].tv		= pTV[3];
										
					Face.m_pVertex	= pVertex;
					Face.m_nVertex	= 4;
					Face.m_pIndices	= pIndex;
					Face.m_nIndices	= 6;

					m_pRender->DrawTriangles( Face );
				}

				// Bottom
				KCell*	pBottom = ( y > 1 ) ? GetpCell( x, y - 1 ) : NULL;
				if( !pBottom || (pBottom->GetType() == 0) )
				{
					pVertex[0].x		= (float)x * CELL_SIZE;
					pVertex[0].y		= (fy+1.0f) * CELL_SIZE;
					pVertex[0].z		= CELL_SIZE;
					pVertex[0].nx		= 0.0f;
					pVertex[0].ny		= 1.0f;
					pVertex[0].nz		= 0.0f;
					pVertex[0].tu		= pTU[0];
					pVertex[0].tv		= pTV[0];
									
					pVertex[1].x		= (float)(x+1) * CELL_SIZE;
					pVertex[1].y		= (fy+1.0f) * CELL_SIZE;
					pVertex[1].z		= CELL_SIZE;
					pVertex[1].nx		= 0.0f;
					pVertex[1].ny		= 1.0f;
					pVertex[1].nz		= 0.0f;
					pVertex[1].tu		= pTU[1];
					pVertex[1].tv		= pTV[1];

					pVertex[2].x		= (float)x * CELL_SIZE;
					pVertex[2].y		= (fy+1.0f) * CELL_SIZE;
					pVertex[2].z		= 0.0f;
					pVertex[2].nx		= 0.0f;
					pVertex[2].ny		= 1.0f;
					pVertex[2].nz		= 0.0f;
					pVertex[2].tu		= pTU[2];
					pVertex[2].tv		= pTV[2];
									
					pVertex[3].x		= (float)(x+1) * CELL_SIZE;
					pVertex[3].y		= (fy+1.0f) * CELL_SIZE;
					pVertex[3].z		= 0.0f;
					pVertex[3].nx		= 0.0f;
					pVertex[3].ny		= 1.0f;
					pVertex[3].nz		= 0.0f;
					pVertex[3].tu		= pTU[3];
					pVertex[3].tv		= pTV[3];
										
					Face.m_pVertex	= pVertex;
					Face.m_nVertex	= 4;
					Face.m_pIndices	= pIndex;
					Face.m_nIndices	= 6;

					m_pRender->DrawTriangles( Face );
				}

				// Left
				KCell*	pLeft = ( x > 0 ) ? GetpCell( x - 1, y ) : NULL;
				if( !pLeft || (pLeft->GetType() == 0) )
				{
					pVertex[0].x		= (float)x * CELL_SIZE;
					pVertex[0].y		= fy * CELL_SIZE;
					pVertex[0].z		= 0.0f;
					pVertex[0].nx		= -1.0f;
					pVertex[0].ny		= 0.0f;
					pVertex[0].nz		= 0.0f;
					pVertex[0].tu		= pTU[0];
					pVertex[0].tv		= pTV[0];
									
					pVertex[1].x		= (float)x * CELL_SIZE;
					pVertex[1].y		= fy * CELL_SIZE;
					pVertex[1].z		= CELL_SIZE;
					pVertex[1].nx		= -1.0f;
					pVertex[1].ny		= 0.0f;
					pVertex[1].nz		= 0.0f;
					pVertex[1].tu		= pTU[1];
					pVertex[1].tv		= pTV[1];

					pVertex[2].x		= (float)x * CELL_SIZE;
					pVertex[2].y		= (fy+1.0f) * CELL_SIZE;
					pVertex[2].z		= 0.0f;
					pVertex[2].nx		= -1.0f;
					pVertex[2].ny		= 0.0f;
					pVertex[2].nz		= 0.0f;
					pVertex[2].tu		= pTU[2];
					pVertex[2].tv		= pTV[2];
									
					pVertex[3].x		= (float)x * CELL_SIZE;
					pVertex[3].y		= (fy+1.0f) * CELL_SIZE;
					pVertex[3].z		= CELL_SIZE;
					pVertex[3].nx		= -1.0f;
					pVertex[3].ny		= 0.0f;
					pVertex[3].nz		= 0.0f;
					pVertex[3].tu		= pTU[3];
					pVertex[3].tv		= pTV[3];
										
					Face.m_pVertex	= pVertex;
					Face.m_nVertex	= 4;
					Face.m_pIndices	= pIndex;
					Face.m_nIndices	= 6;

					m_pRender->DrawTriangles( Face );
				}

				// Right
				KCell*	pRight = ( x < m_Width - 1 ) ? GetpCell( x + 1, y ) : NULL;
				if( !pRight || (pRight->GetType() == 0) )
				{
					pVertex[0].x		= (float)(x+1) * CELL_SIZE;
					pVertex[0].y		= fy * CELL_SIZE;
					pVertex[0].z		= CELL_SIZE;
					pVertex[0].nx		= 1.0f;
					pVertex[0].ny		= 0.0f;
					pVertex[0].nz		= 0.0f;
					pVertex[0].tu		= pTU[0];
					pVertex[0].tv		= pTV[0];
									
					pVertex[1].x		= (float)(x+1) * CELL_SIZE;
					pVertex[1].y		= fy * CELL_SIZE;
					pVertex[1].z		= 0.0f;
					pVertex[1].nx		= 1.0f;
					pVertex[1].ny		= 0.0f;
					pVertex[1].nz		= 0.0f;
					pVertex[1].tu		= pTU[1];
					pVertex[1].tv		= pTV[1];

					pVertex[2].x		= (float)(x+1) * CELL_SIZE;
					pVertex[2].y		= (fy+1.0f) * CELL_SIZE;
					pVertex[2].z		= CELL_SIZE;
					pVertex[2].nx		= 1.0f;
					pVertex[2].ny		= 0.0f;
					pVertex[2].nz		= 0.0f;
					pVertex[2].tu		= pTU[2];
					pVertex[2].tv		= pTV[2];
									
					pVertex[3].x		= (float)(x+1) * CELL_SIZE;
					pVertex[3].y		= (fy+1.0f) * CELL_SIZE;
					pVertex[3].z		= 0.0f;
					pVertex[3].nx		= 1.0f;
					pVertex[3].ny		= 0.0f;
					pVertex[3].nz		= 0.0f;
					pVertex[3].tu		= pTU[3];
					pVertex[3].tv		= pTV[3];
										
					Face.m_pVertex	= pVertex;
					Face.m_nVertex	= 4;
					Face.m_pIndices	= pIndex;
					Face.m_nIndices	= 6;

					m_pRender->DrawTriangles( Face );
				}
			}
		}
	}

	m_pRender->FlushTriangles();
	m_pRender->ActivateLighting( false );
	
	//
	// Affiche les items
	//
	KItem* pItem;

	// Afiche les autres types d'items
	for( pItem = m_Items.GetFirst(); pItem; pItem = m_Items.GetNext( pItem ) )
	{
		// N'affiche que les peres
		if( !pItem->GetpFather() && ( pItem->GetType() != KIT_BOT ) )
			pItem->Display();
	}

	// Afiche les KIT_BOT
	for( pItem = m_Items.GetFirst(); pItem; pItem = m_Items.GetNext( pItem ) )
	{
		// N'affiche que les peres
		if( !pItem->GetpFather() && ( pItem->GetType() == KIT_BOT ) )
			pItem->Display();
	}

	return KR_OK;
}

//--------------------------------------------------------------------------------------------
KRESULT KLevel::Draw2D( KPt& Pos )
{
	CellType Type;
	CellType TypeNoFlags;

	u16 Width = m_pShader->GetShaderInfo().m_Width/CELL_SIZE;

	float			pTU[4];
	float			pTV[4];

	for( u32 y = 0; y < m_Height; y ++ )
	{
		for( u32 x = 0; x < m_Width; x ++ )
		{
			Type = GetpCell( x, y )->GetType();
			TypeNoFlags = Type & CELL_MASK_TYPE;

			if (Type & CELL_FLAG_FLIPH)
			{
				pTU[1] = pTU[3] = ((float) (TypeNoFlags % Width) * 0.125f);
				pTU[0] = pTU[2] = ((float) (TypeNoFlags % Width) * 0.125f) + 0.125f;
			}
			else
			{
				pTU[0] = pTU[2] = ((float) (TypeNoFlags % Width) * 0.125f);
				pTU[1] = pTU[3] = ((float) (TypeNoFlags % Width) * 0.125f) + 0.125f;
			}
			
			if (Type & CELL_FLAG_FLIPV)
			{
				pTV[2] = pTV[3] = ((float) ((TypeNoFlags / Width) * 0.125f));
				pTV[0] = pTV[1] = ((float) ((TypeNoFlags / Width) * 0.125f)) + 0.125f;
			}
			else
			{
				pTV[0] = pTV[1] = ((float) ((TypeNoFlags / Width) * 0.125f));
				pTV[2] = pTV[3] = ((float) ((TypeNoFlags / Width) * 0.125f)) + 0.125f;
			}

			m_pRender->DrawQuad( Pos.x + x * CELL_SIZE, Pos.y + y * CELL_SIZE, CELL_SIZE, CELL_SIZE, m_pShader, KRGBA( 255, 255, 255, 255 ), KRGBA( 0, 0, 0, 0 ), KRM_NORMAL, pTU, pTV );
		}
	}

	//
	// Affiche les items
	//
	KItem* pItem;

	// Afiche les autres types d'items
	for( pItem = m_Items.GetFirst(); pItem; pItem = m_Items.GetNext( pItem ) )
	{
		// N'affiche que les peres
		if( !pItem->GetpFather() && ( pItem->GetType() != KIT_BOT ) )
			pItem->Display2D();
	}

	// Afiche les KIT_BOT
	for( pItem = m_Items.GetFirst(); pItem; pItem = m_Items.GetNext( pItem ) )
	{
		// N'affiche que les peres
		if( !pItem->GetpFather() && ( pItem->GetType() == KIT_BOT ) )
			pItem->Display2D();
	}

	// Affiche les FX
	for( pItem = m_Items.GetFirst(); pItem; pItem = m_Items.GetNext( pItem ) )
	{
		// N'affiche que les peres
		if( !pItem->GetpFather() )
		{
			// Flamme
			if( pItem->GetType() == KIT_BOT )
			{
				KBot*	pBot = (KBot*)pItem;

				if( pBot->GetAction() == KBA_ATTACK )
				{
					KPt		FlamePos;
					KPt		DeltaTarget = GetDeltaAttack( pBot->GetAttackTarget() );
					
					FlamePos.x = SCREEN_OFFSETX + (pBot->GetPos().x + DeltaTarget.x) * CELL_SIZE;
					FlamePos.y = (pBot->GetPos().y + DeltaTarget.y) * CELL_SIZE;
					DrawFlame( FlamePos, (g_Timer.GetTime() / 50) % 4 );
				}
				
				if( pBot->GetAction() == KBA_ATTACKLASER )
				{
					KPt		LaserPos;
					KPt		DeltaTarget = GetDeltaAttack( pBot->GetAttackTarget() );
					
					LaserPos.x = SCREEN_OFFSETX + pBot->GetPos().x * CELL_SIZE;
					LaserPos.y = pBot->GetPos().y * CELL_SIZE;
					DrawLaser( LaserPos, DeltaTarget, pBot->GetFireRange(), (g_Timer.GetTime() / 50) % 4 );
				}
			}

			// Halo sur le regenerator
			if( pItem->GetType() == KIT_REGENERATOR )
			{
				KBot*	pBot = (KBot*)GetpItem( pItem->GetPos().x, pItem->GetPos().y, KIT_BOT );

				if( pBot && pBot->GetLife() )
				{
					KPt		HaloPos;
					
					HaloPos.x = SCREEN_OFFSETX + pBot->GetPos().x * CELL_SIZE;
					HaloPos.y = pBot->GetPos().y * CELL_SIZE;
					DrawHalo( HaloPos );
				}
			}

			// Explosion de mines
			if( pItem->GetType() == KIT_MINE )
			{
				KMine*	pMine = (KMine*)GetpItem( pItem->GetPos().x, pItem->GetPos().y, KIT_MINE );

				if( pMine && ( pMine->GetState() == KMS_DESTROYED ) )
				{
					KPt		MinePos;
					
					MinePos.x = SCREEN_OFFSETX + pMine->GetPos().x * CELL_SIZE;
					MinePos.y = pMine->GetPos().y * CELL_SIZE;
					DrawMineExplosion( MinePos, (g_Timer.GetTime() / 50) % 4 );
				}
			}

		}
	}

	return KR_OK;
}

//--------------------------------------------------------------------------------------------
void KLevel::DrawFlame( KPt Pos, u32 Type )
{
	float	pTU[4];
	float	pTV[4];

	pTU[0] = 0.5f;		pTV[0] = 0.25f;
	pTU[1] = 0.625f;	pTV[1] = 0.25f;
	pTU[2] = 0.5f;		pTV[2] = 0.375f;
	pTU[3] = 0.625f;	pTV[3] = 0.375f;

	pTU[0] += (float)Type * 0.125f;
	pTU[1] += (float)Type * 0.125f;
	pTU[2] += (float)Type * 0.125f;
	pTU[3] += (float)Type * 0.125f;

	m_pRender->DrawQuad( Pos.x, Pos.y, CELL_SIZE, CELL_SIZE, m_pShader, KRGBA( 255, 255, 255, 255 ), KRGBA( 0, 0, 0, 0 ), KRM_NORMAL, pTU, pTV );
}

//--------------------------------------------------------------------------------------------
void KLevel::DrawHalo( KPt Pos )
{
	float	pTU[4];
	float	pTV[4];
	float	Halo = (float)((g_Timer.GetTime() / 100) % 3);

	pTU[0] = 0.5f;		pTV[0] = 0.375f;
	pTU[1] = 0.625f;	pTV[1] = 0.375f;
	pTU[2] = 0.5f;		pTV[2] = 0.5f;
	pTU[3] = 0.625f;	pTV[3] = 0.5f;

	pTU[0] += Halo * 0.125f;
	pTU[1] += Halo * 0.125f;
	pTU[2] += Halo * 0.125f;
	pTU[3] += Halo * 0.125f;

	m_pRender->DrawQuad( Pos.x, Pos.y, CELL_SIZE, CELL_SIZE, m_pShader, KRGBA( 255, 255, 255, 255 ), KRGBA( 0, 0, 0, 0 ), KRM_NORMAL, pTU, pTV );
}

//--------------------------------------------------------------------------------------------
void KLevel::DrawMineExplosion( KPt Pos, u32 Type )
{
	DrawFlame( KPt( Pos.x - CELL_SIZE,	Pos.y - CELL_SIZE ),	Type );
	DrawFlame( KPt( Pos.x - CELL_SIZE,	Pos.y			 ),		Type );
	DrawFlame( KPt( Pos.x - CELL_SIZE,	Pos.y + CELL_SIZE ),	Type );
	DrawFlame( KPt( Pos.x,				Pos.y - CELL_SIZE ),	Type );
	DrawFlame( KPt( Pos.x ,				Pos.y 			 ),		Type );
	DrawFlame( KPt( Pos.x ,				Pos.y + CELL_SIZE ),	Type );
	DrawFlame( KPt( Pos.x + CELL_SIZE,	Pos.y - CELL_SIZE ),	Type );
	DrawFlame( KPt( Pos.x + CELL_SIZE,	Pos.y 			 ),		Type );
	DrawFlame( KPt( Pos.x + CELL_SIZE,	Pos.y + CELL_SIZE ),	Type );
}

//--------------------------------------------------------------------------------------------
void KLevel::DrawLaser( KPt Pos, KPt Dir, s32 Range, u32 Type )
{
// Original Version	
/*
	for( s32 i = 0; i < Range; i ++ )
	{
		KPt	LaserPos = Pos + Dir;
		LaserPos.x *= CELL_SIZE * (i + 1);
		LaserPos.y *= CELL_SIZE * (i + 1);
		DrawLaser( LaserPos, (Dir.x ? false : true) );
	}
*/

// Modified by Mac Load 24-03-2004
	for( s32 i = 0; i < Range; i ++ )
	{
		KPt TempDeltaPos = Dir ;
		TempDeltaPos.x *= (i + 1) ;
		TempDeltaPos.y *= (i + 1) ;
		TempDeltaPos.x *= CELL_SIZE ;
		TempDeltaPos.y *= CELL_SIZE ;

		KPt	LaserPos	= Pos + TempDeltaPos ;

		DrawLaser( LaserPos, (Dir.x ? false : true) );
	}
}

//--------------------------------------------------------------------------------------------
void KLevel::DrawLaser( KPt Pos, bool bUpDown )
{
	float	pTU[4];
	float	pTV[4];

	if( bUpDown )
	{
		pTU[0] = 0.625f;	pTV[0] = 0.750f;
		pTU[1] = 0.625f;	pTV[1] = 0.875f;
		pTU[2] = 0.750f;	pTV[2] = 0.750f;
		pTU[3] = 0.750f;	pTV[3] = 0.875f;
	}
	else
	{
		pTU[0] = 0.625f;	pTV[0] = 0.750f;
		pTU[1] = 0.750f;	pTV[1] = 0.750f;
		pTU[2] = 0.625f;	pTV[2] = 0.875f;
		pTU[3] = 0.750f;	pTV[3] = 0.875f;
	}

	m_pRender->DrawQuad( Pos.x, Pos.y, CELL_SIZE, CELL_SIZE, m_pShader, KRGBA( 255, 255, 255, 255 ), KRGBA( 0, 0, 0, 0 ), KRM_NORMAL, pTU, pTV );
}

//--------------------------------------------------------------------------------------------
KItem* KLevel::GetpItem( u32 x, u32 y )
{
	for( KItem* pItem = m_Items.GetFirst(); pItem; pItem = m_Items.GetNext( pItem ) )
		if( ( pItem->GetPos().x == (s32)x ) && ( pItem->GetPos().y == (s32)y ) )
			return pItem;

	return NULL;
}

//--------------------------------------------------------------------------------------------
KItem* KLevel::GetpItem( u32 x, u32 y, KITEMTYPE Type )
{
	for( KItem* pItem = m_Items.GetFirst(); pItem; pItem = m_Items.GetNext( pItem ) )
		if( ( pItem->GetPos().x == (s32)x ) && ( pItem->GetPos().y == (s32)y ) && ( pItem->GetType() == Type ) )
			return pItem;

	return NULL;
}

//--------------------------------------------------------------------------------------------
KItem* KLevel::GetpNotItem( u32 x, u32 y, KITEMTYPE Type )
{
	for( KItem* pItem = m_Items.GetFirst(); pItem; pItem = m_Items.GetNext( pItem ) )
		if( ( pItem->GetPos().x == (s32)x ) && ( pItem->GetPos().y == (s32)y ) && ( pItem->GetType() != Type ) )
			return pItem;

	return NULL;
}

//--------------------------------------------------------------------------------------------
KItem* KLevel::GetpItem( u32 ItemId )
{
	u32	i = 0;

	for( KItem* pItem = m_Items.GetFirst(); pItem; pItem = m_Items.GetNext( pItem ) )
	{
		if( i == ItemId )
			return pItem;

		i++ ;
	}

	return NULL;
}

//--------------------------------------------------------------------------------------------
u32	KLevel::GetnItems( KITEMTYPE Type )
{
	u32	i = 0;

	for( KItem* pItem = m_Items.GetFirst(); pItem; pItem = m_Items.GetNext( pItem ) )
		if( pItem->GetType() == Type )
			i ++;

	return i;
}

//--------------------------------------------------------------------------------------------
bool KLevel::CanMove( s32 x, s32 y )
{
	// Doit etre dans la map
	if( ( x < 0 ) || ( y < 0 ) || ( x >= (s32)m_Width ) || ( y >= (s32)m_Height ) )
		return false;

	// Doit etre marchable
	// TODO
	// 0 => Ok, 1 => Mur
	if( GetpCell( x, y )->GetType() != 0 )
		return false;

	// Doit y'avoir personne
	KItem*	pItem = GetpItem( x, y );
	if( pItem )
	{
		switch( pItem->GetType() )
		{
		case KIT_BONUSVISION:
		case KIT_BONUSPOWER:
		case KIT_BONUSSPEED:
		case KIT_BONUSJOKER:
		case KIT_REGENERATOR:
		case KIT_MEDIKIT:
		case KIT_BINOCULARS:
		case KIT_MINE:
		case KIT_LASER:
			break;
		case KIT_BOT:
			return ( ((KBot *)pItem)->GetLife() == 0 );		//  [24-07-2003 Aitonfrere] Tombes non bloquantes
		default:
			KASSERT(0);
		}
	}

	return true;
}

//--------------------------------------------------------------------------------------------
bool KLevel::CanSpawn( s32 x, s32 y )
{
	// Doit etre dans la map
	if( ( x < 0 ) || ( y < 0 ) || ( x >= (s32)m_Width ) || ( y >= (s32)m_Height ) )
		return false;

	// Doit etre marchable
	// TODO
	// 0 => Ok, 1 => Mur
	if( GetpCell( x, y )->GetType() != 0 )
		return false;

	// Doit y'avoir personne
	KItem*	pItem = GetpItem( x, y );
	if( pItem )
		return false;

	return true;
}

//---------------------------------------------------------------------------------------------------------------------
KCell* KLevel::GetpSightCell( KPt& Pos, s32 SightDistance )
{
	//	Cette fonction renvoie la map avec uniquement les cellules visibles,
	//	Les cellules en dehors du champs de vision sont marqu�e avec la valeur UNKNOWN_CELL_TYPE
	
	KCell*	pCell;
	KCell*	pSightCell;

	// Efface tout
	memset( m_pSightCell, UNKNOWN_CELL_TYPE, m_Width * m_Height * sizeof( KCell ) );

    if(SightDistance == SIGHT_UNLIMITED)
    {
        for( s32 y = 0; y < (s32)m_Height; y ++ )
        {
            for( s32 x = 0; x < (s32)m_Width; x ++ ) 
            {
                pCell		= GetpCell( x, y );
                pSightCell	= GetpSightCell( x, y );
                
				*pSightCell = *pCell;
            }
        }
        return m_pSightCell ;
    }

	s32		MinX = MAX( 0, Pos.x - SightDistance );
	s32		MinY = MAX( 0, Pos.y - SightDistance );
	s32		MaxX = MIN( (s32)m_Width, Pos.x + SightDistance );
	s32		MaxY = MIN( (s32)m_Height, Pos.y + SightDistance );

	s32		SquareDistance = SightDistance * SightDistance;

	for( s32 y = MinY; y < MaxY; y ++ )
	{
		for( s32 x = MinX; x < MaxX; x ++ )
		{
			// Teste dans le cercle d�finit par SightDistance (Rayon du cercle)
			if( ( SightDistance == SIGHT_UNLIMITED ) || ( ( SQR( Pos.x - x ) + SQR( Pos.y - y ) ) <= SquareDistance ) )
			{
				pCell		= GetpCell( x, y );
				pSightCell	= GetpSightCell( x, y );

				*pSightCell = *pCell;
			}
		}
	}

	return m_pSightCell;
}

//---------------------------------------------------------------------------------------------------------------------
u32 KLevel::GetpSightItem( KPt& Pos, s32 SightDistance, KItemInfo** pItems )
{
	//	Cette fonction renvoie les items dans le champs de vision.
	s32		SquareDistance = SightDistance * SightDistance;

	SafeDeleteArray( m_pSightItems );

	KList<KItem*>			SightItems;
	KItem*					pPosItem = GetpItem( Pos.x, Pos.y );
	u32						nItems;

	// Fait la liste de tout les items visibles
	for( KItem* pItem = m_Items.GetFirst(); pItem; pItem = m_Items.GetNext( pItem ) )
		if( ( SightDistance == SIGHT_UNLIMITED ) || ( ( SQR( Pos.x - pItem->GetPos().x ) + SQR( Pos.y - pItem->GetPos().y ) ) <= SquareDistance ) )
			if( pItem != pPosItem )			// Pour empecher le Bot de se voir
				if( pItem->IsVisible() )	// L'item doit etre visible
					SightItems.Add( pItem );

	nItems = SightItems.GetSize();
	if( !nItems )
	{
		SightItems.Clear();
		*pItems = NULL;
		return 0;
	}

	// Creation de la table des Items
	s32	i = 0;

	m_pSightItems = new KItemInfo[ nItems ];

	for( KItem* pSightItem = SightItems.GetFirst(); pSightItem; pSightItem = SightItems.GetNext( pSightItem ) )
	{
		m_pSightItems[i].SetType( pSightItem->GetType() );
		m_pSightItems[i].SetPos( pSightItem->GetPos() );
		m_pSightItems[i].SetAlive( false );

		switch( pSightItem->GetType() )
		{
		case KIT_BOT:
			m_pSightItems[i].SetAlive( ((KBot*)pSightItem)->GetLife() ? true : false );
			break;
		case KIT_BONUSVISION:
		case KIT_BONUSPOWER:
		case KIT_BONUSSPEED:
		case KIT_BONUSJOKER:
		case KIT_REGENERATOR:
		case KIT_MEDIKIT:
		case KIT_BINOCULARS:
		case KIT_MINE:
		case KIT_LASER:
			break;
		default:
			KASSERT(0);
			break;
		}

		i++;
	}

	*pItems = m_pSightItems;

	SightItems.Clear();

	return nItems;
}

//---------------------------------------------------------------------------------------------------------------------
KPt	KLevel::GetDeltaPos( KBOTMOVEDIR MoveDir )
{
	switch( MoveDir )
	{
	case KBMD_UP:		return KPt( 0, -1 );
	case KBMD_DOWN:		return KPt( 0, 1 );
	case KBMD_LEFT:		return KPt( -1, 0 );
	case KBMD_RIGHT:	return KPt( 1, 0 );
	default:
		break;
	}

	return KPt( 0, 0 );
}

//---------------------------------------------------------------------------------------------------------------------
KPt	KLevel::GetDeltaAttack( KBOTATTACKTARGET Target )
{
	switch( Target )
	{
	case KBAT_UP:		return KPt( 0, -1 );
	case KBAT_DOWN:		return KPt( 0, 1 );
	case KBAT_LEFT:		return KPt( -1, 0 );
	case KBAT_RIGHT:	return KPt( 1, 0 );
	default:
		break;
	}

	return KPt( 0, 0 );
}

