//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: Timer.cpp
//	Author			: - Aitonfrere	
//					  - Dino			dino@zythum-project.com
//
//	Date			: 16/07/2003
//	Modification	:
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------
#include "Timer.h"

#ifdef _LINUX
#include <sys/time.h>
#include <unistd.h>
#endif // _LINUX

//---------------------------------------------------------------------------------------------------------------------
KTimer	g_Timer;

//---------------------------------------------------------------------------------------------------------------------
KTimer::KTimer()
{
#ifdef _WIN32
	QueryPerformanceFrequency( (LARGE_INTEGER *) &m_Frequency );
#endif // _WIN32

#ifdef _LINUX
	struct timeval 	tv;
	struct timezone tz;

	gettimeofday( &tv, &tz );
	m_StartTime = tv.tv_sec;
#endif // _LINUX
}

//---------------------------------------------------------------------------------------------------------------------
KTIME KTimer::GetTime()
{
#ifdef _WIN32
	s64				Time;

	QueryPerformanceCounter((LARGE_INTEGER *) &Time);

	return KTIME(Time / (m_Frequency / 1000));
#endif // _WIN32

#ifdef _LINUX
	struct timeval 	tv;
	struct timezone tz;
	s64	Time;	

	gettimeofday( &tv, &tz );
	Time = ( tv.tv_sec - m_StartTime ) * 1000;
	Time += tv.tv_usec / 1000;
	return Time;
#endif // _LINUX
}
