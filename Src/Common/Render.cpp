//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: RenderOGL.cpp
//	Author			: - Dino			dino@zythum-project.com
//
//	Date			: 26/10/2003
//	Modification	: 24/03/2004 by Mac Load
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------
#include <math.h>
#include "Matrix.h"
#include "Camera.h"
#include "Shader.h"
#include "Mesh.h"
#include "Timer.h"
#include "Render.h"

//--------------------------------------------------------------------------------------------
KRender::KRender()
{
	m_hGLRC				= NULL;
	m_hDC				= NULL;

	m_Width				= 0;
	m_Height			= 0;
	m_Bpp				= 0;
	m_bFullScreen		= false;

	m_nPoly				= 0;
	m_nPrimitive		= 0;

	m_MaxVertex			= 30000;
	m_MaxIndex			= 30000;

	// Vertex
	m_pVertexBuffer		= NULL;
	m_pIndexBuffer		= NULL;
	m_nVertex			= 0;
	m_nIndex			= 0;

	// TLVertex
	m_pTLVertexBuffer	= NULL;
	m_pTLIndexBuffer	= NULL;
	m_nTLVertex			= 0;
	m_nTLIndex			= 0;

	m_pCamera			= NULL;
}

//--------------------------------------------------------------------------------------------
KRESULT KRender::Init( HWND hWnd, HDC hDC, u32 Width, u32 Height, u32 Bpp, bool bFullScreen )
{
	m_hWnd			= hWnd;
	m_Width			= Width;
	m_Height		= Height;
	m_Bpp			= Bpp;
	m_bFullScreen	= bFullScreen;
	m_hDC			= hDC;

	//
	// Init display
	//
#ifdef _WIN32
	PIXELFORMATDESCRIPTOR pFD = {
		sizeof(PIXELFORMATDESCRIPTOR),		// size of this pfd
		1,									// version number
//		PFD_GENERIC_ACCELERATED |
		PFD_DRAW_TO_WINDOW |				// support window
		PFD_SUPPORT_OPENGL |				// support OGL
		PFD_DOUBLEBUFFER,					// double buffered
		PFD_TYPE_RGBA,						// RGBA type
		(BYTE)Bpp,							// color depth
		0, 0, 0, 0, 0, 0,					// color bits ignored
		0,									// no alpha buffer
		0,									// shift bit ignored
		0,									// no accumulation buffer
		0, 0, 0, 0,							// accum bits ignored
		(BYTE)Bpp,							// z-buffer
		0,									// no stencil buffer
		0,									// no auxiliary buffer
		PFD_MAIN_PLANE,						// main layer
		0,									// reserved
		0, 0, 0								// layer masks ignored
	};

	m_PFD = pFD;

	int  iPixelFormat;

	iPixelFormat = ChoosePixelFormat( m_hDC, &m_PFD );

	if( !SetPixelFormat( m_hDC, iPixelFormat, &m_PFD ) )
		return KR_ERROR;

	if( !( m_hGLRC = wglCreateContext( m_hDC ) ) )
		return KR_ERROR;

	if( !( wglMakeCurrent( m_hDC, m_hGLRC ) ) )
		return KR_ERROR;

	if( bFullScreen )
	{
		DEVMODE		DevMode;

		ZeroMemory( &DevMode, sizeof( DevMode ) );
		DevMode.dmSize			= sizeof( DevMode );
		DevMode.dmPelsWidth		= m_Width;
		DevMode.dmPelsHeight	= m_Height;
		DevMode.dmBitsPerPel	= m_Bpp;
		DevMode.dmFields		= DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

		if( ChangeDisplaySettings( &DevMode, CDS_FULLSCREEN ) != DISP_CHANGE_SUCCESSFUL )
		{
			wglDeleteContext( m_hGLRC ); 
			ReleaseDC( m_hWnd, m_hDC );
			return KR_ERROR;
		}

		DWORD	Style = GetWindowLong( m_hWnd, GWL_STYLE );
		int		MenuY = GetSystemMetrics( SM_CYMENUSIZE );
		RECT	WinRect = { 0, MenuY, m_Width, m_Height };
		AdjustWindowRect( &WinRect, Style, ::GetMenu( m_hWnd ) ? TRUE : FALSE );
		MoveWindow( m_hWnd, WinRect.left, WinRect.top, WinRect.right - WinRect.left, WinRect.bottom - WinRect.top, TRUE );
		m_Height -= MenuY;
	}
#endif // _WIN32

	glPixelStorei( GL_PACK_ALIGNMENT, 1 );
	glPixelStorei( GL_UNPACK_ALIGNMENT, 1 );

	//
	// Init buffer
	//

	m_pVertexBuffer		= new KVertex[m_MaxVertex];
	m_pIndexBuffer		= new KINDEX[m_MaxIndex];
	m_pTLVertexBuffer	= new KTLVertex[m_MaxVertex];
	m_pTLIndexBuffer	= new KINDEX[m_MaxIndex];

	// Init Scene
	InitScene();

	// Font shader
	LoadShader( "Font.png", &m_pFontShader );

	return KR_OK;
}

//--------------------------------------------------------------------------------------------
KRESULT KRender::Reset( HWND hWnd, u32 Width, u32 Height, u32 Bpp, bool bFullScreen )
{
	m_hWnd			= hWnd;
	m_Width			= Width;
	m_Height		= Height;
	m_Bpp			= Bpp;
	m_bFullScreen	= bFullScreen;

	//	Init Scene
	InitScene();

	return KR_OK;
}

//--------------------------------------------------------------------------------------------------------------------------
KRESULT KRender::InitScene()
{
	//
	//	Init Scene
	//
// Original version
/*
		GLfloat		pAmbient[] = { 0.0f, 0.0f, 0.0f, 0.0f };
*/

// Modified by Mac Load 24-03-2004 -- Add Global Ambient Light
	GLfloat		pAmbient[] = { 0.8f, 0.8f, 0.8f, 0.8f };
	GLfloat		pGlobalAmbient[] = { 0.6f, 0.6f, 0.6f, 0.6f };
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT,pGlobalAmbient) ;
// END -- Modified by Mac Load 24-03-2004

	GLfloat		pDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat		pSpecular[] = { 0.0f, 0.0f, 0.0f, 0.0f };
	glMaterialfv( GL_FRONT, GL_AMBIENT, pAmbient );
	glMaterialfv( GL_FRONT, GL_DIFFUSE, pDiffuse );
	glMaterialfv( GL_FRONT, GL_SPECULAR, pSpecular );
	glMaterialf( GL_FRONT, GL_SHININESS, 25.0f );

	//
	//	Default States
	//
	glDisable( GL_LIGHTING );
	glEnable( GL_DEPTH_TEST );
	glCullFace( GL_BACK );
	glEnable( GL_CULL_FACE );
	glShadeModel( GL_SMOOTH );
//	glEnable(GL_NORMALIZE);
//	glLightModeli( GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE );

	//
	//	ModelView Matrix
	//
	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();

	//
	//	Projection Matrix
	//
	SetProjectionMatrix( 90.0f, (float)m_Width / (float)m_Height, 10.0f, 10000.0f );

	//
	// Viewport
	//
	glViewport( 0, 0, m_Width, m_Height );

	return KR_OK;
}

//--------------------------------------------------------------------------------------------------------------------------
KRESULT KRender::SetProjectionMatrix( float fFOV, float fAspect, float fNearPlane, float fFarPlane )
{
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();

	float	h = ( tanf( (fFOV * PI / 180.0f) * 0.5f ) * fFarPlane ) * 0.5f;
	float	FovY = (atanf( (h * fAspect) / fFarPlane ) * 2.0f) * 180.0f / PI;
	gluPerspective( FovY, fAspect, fNearPlane, fFarPlane );

	return KR_OK;
}

//--------------------------------------------------------------------------------------------
KRESULT KRender::End()
{
	SafeDelete( m_pFontShader );

	SafeDeleteArray( m_pIndexBuffer );
	SafeDeleteArray( m_pVertexBuffer );
	SafeDeleteArray( m_pTLIndexBuffer );
	SafeDeleteArray( m_pTLVertexBuffer );

	return KR_OK;
}

//--------------------------------------------------------------------------------------------------------------------------
KRESULT KRender::BeginScene()
{
	m_nPoly			= 0;
	m_nPrimitive	= 0;

	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();

	//
	//	Setting Camera
	//
	if( m_pCamera )
	{
		KMatrix	ViewMatrix = m_pCamera->GetMatrix();
		glLoadMatrixf( (float*)&ViewMatrix );
	}

	//
	// Clean the backbuffer
	//
	glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );
	glClearDepth( 1.0f );
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	return KR_OK;
}

//--------------------------------------------------------------------------------------------------------------------------
KRESULT KRender::EndScene()
{
	glFlush();

	return KR_OK;
}

//--------------------------------------------------------------------------------------------------------------------------
KRESULT KRender::Flip( HWND hWnd, KRect* pRect )
{
	KASSERT( (hWnd && pRect) || (!hWnd && !pRect) );
	
	// Flip buffers
#ifdef _WIN32
	if( !wglSwapLayerBuffers( m_hDC, WGL_SWAP_MAIN_PLANE ) )
		return KR_ERROR;
#endif // _WIN32

#ifdef _LINUX
	glXSwapBuffers( m_hDC, m_hWnd );
#endif // _LINUX

	return KR_OK;
}

//--------------------------------------------------------------------------------------------------------------------------
//
//	Vertex
//
//--------------------------------------------------------------------------------------------------------------------------
KRESULT KRender::DrawTriangles( KRenderFace& Face )
{
	KASSERT( Face.m_pVertex );
	KASSERT( Face.m_nVertex );
	KASSERT( Face.m_pIndices );
	KASSERT( Face.m_nIndices );
	
	KASSERT( m_pVertexBuffer );
	KASSERT( m_pIndexBuffer );
	KASSERT( Face.m_nVertex <= m_MaxVertex );
	KASSERT( Face.m_nIndices <= m_MaxIndex );

	if( ( m_nVertex + Face.m_nVertex > m_MaxVertex ) ||
		( m_nIndex + Face.m_nIndices > m_MaxIndex ) )
		FlushTriangles();

	// Copie les vertex
	memcpy( &((KVertex*)m_pVertexBuffer)[m_nVertex], Face.m_pVertex, sizeof( KVertex ) * Face.m_nVertex );

	// Ajoute les index
	KINDEX*		pI1 = &m_pIndexBuffer[m_nIndex];
	KINDEX*		pI2 = Face.m_pIndices;
	u32			i	= Face.m_nIndices;
	
	while( i-- )
		pI1[i] = pI2[i] + (KINDEX)m_nVertex;

	m_nVertex	+= Face.m_nVertex;
	m_nIndex	+= Face.m_nIndices;

	return KR_OK;
}

//--------------------------------------------------------------------------------------------------------------------------
KRESULT KRender::FlushTriangles()
{
	if( m_nVertex && m_nIndex )
	{
		KASSERT( m_pVertexBuffer );	
		KASSERT( m_pIndexBuffer );
		
		DrawPrimitive();
		m_nVertex	= 0;
		m_nIndex	= 0;
	}

	return KR_OK;
}

//--------------------------------------------------------------------------------------------------------------------------
KRESULT KRender::DrawPrimitive()
{
	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

	glEnableClientState( GL_VERTEX_ARRAY );
	glEnableClientState( GL_NORMAL_ARRAY );
	glEnableClientState( GL_TEXTURE_COORD_ARRAY );

	KVertex*	pVertex = (KVertex*)m_pVertexBuffer;
	
	glVertexPointer(	3,	GL_FLOAT, sizeof( KVertex ), &pVertex->x	);
	glNormalPointer(		GL_FLOAT, sizeof( KVertex ), &pVertex->nx	);
	glTexCoordPointer(	2,	GL_FLOAT, sizeof( KVertex ), &pVertex->tu	);
	glDrawElements( GL_TRIANGLES, m_nIndex, GL_UNSIGNED_SHORT, m_pIndexBuffer );

	glDisableClientState( GL_VERTEX_ARRAY );
	glDisableClientState( GL_NORMAL_ARRAY );
	glDisableClientState( GL_TEXTURE_COORD_ARRAY );

	glDisable( GL_BLEND );

	m_nPoly += m_nIndex / 3;
	m_nPrimitive ++;

	return KR_OK;
}

//--------------------------------------------------------------------------------------------------------------------------
//
//	TLVertex
//
//--------------------------------------------------------------------------------------------------------------------------
KRESULT KRender::DrawTrianglesTL( KRenderFace& Face )
{
	KASSERT( Face.m_pVertex );
	KASSERT( Face.m_nVertex );
	KASSERT( Face.m_pIndices );
	KASSERT( Face.m_nIndices );

	KASSERT( m_pTLVertexBuffer );
	KASSERT( m_pTLIndexBuffer );
	KASSERT( Face.m_nVertex <= m_MaxVertex );
	KASSERT( Face.m_nIndices <= m_MaxIndex );

	if( ( m_nTLVertex + Face.m_nVertex > m_MaxVertex ) ||
		( m_nTLIndex + Face.m_nIndices > m_MaxIndex ) )
		FlushTrianglesTL();

	// Copie les vertex
	memcpy( &((KTLVertex*)m_pTLVertexBuffer)[m_nTLVertex], Face.m_pVertex, sizeof( KTLVertex ) * Face.m_nVertex );

	// Ajoute les index
	KINDEX*		pI1 = &m_pTLIndexBuffer[m_nTLIndex];
	KINDEX*		pI2 = Face.m_pIndices;
	u32			i	= Face.m_nIndices;
	
	while( i-- )
		pI1[i] = pI2[i] + (KINDEX)m_nTLVertex;

	m_nTLVertex	+= Face.m_nVertex;
	m_nTLIndex	+= Face.m_nIndices;

	return KR_OK;
}

//--------------------------------------------------------------------------------------------------------------------------
KRESULT KRender::FlushTrianglesTL()
{
	if( m_nTLVertex && m_nTLIndex )
	{
		KASSERT( m_pTLVertexBuffer );	
		KASSERT( m_pTLIndexBuffer );
		
		DrawPrimitiveTL();
		m_nTLVertex	= 0;
		m_nTLIndex	= 0;
	}

	return KR_OK;
}

//--------------------------------------------------------------------------------------------------------------------------
KRESULT KRender::DrawPrimitiveTL()
{
	glDisable( GL_DEPTH_TEST );

	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();
	glLoadIdentity();
	glMatrixMode( GL_PROJECTION );
	glPushMatrix();
	glLoadIdentity();
	glOrtho( 0, m_Width, m_Height, 0, 0.0f, 1.0f );

	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

	glEnableClientState( GL_VERTEX_ARRAY );
	glEnableClientState( GL_COLOR_ARRAY );
	glEnableClientState( GL_TEXTURE_COORD_ARRAY );

	KTLVertex*	pVertex = (KTLVertex*)m_pTLVertexBuffer;
	
	glVertexPointer(	4,	GL_FLOAT,			sizeof( KTLVertex ), &pVertex->x	);
	glColorPointer(		4,	GL_UNSIGNED_BYTE,	sizeof( KTLVertex ), &pVertex->color);
	glTexCoordPointer(	2,	GL_FLOAT,			sizeof( KTLVertex ), &pVertex->tu	);
	glDrawElements( GL_TRIANGLES, m_nTLIndex, GL_UNSIGNED_SHORT, m_pTLIndexBuffer );

	glDisableClientState( GL_VERTEX_ARRAY );
	glDisableClientState( GL_COLOR_ARRAY );
	glDisableClientState( GL_TEXTURE_COORD_ARRAY );

	glMatrixMode( GL_PROJECTION );
	glPopMatrix();
	glMatrixMode( GL_MODELVIEW );
	glPopMatrix();

	glDisable( GL_BLEND );
	glEnable( GL_DEPTH_TEST );

	m_nPoly += m_nTLIndex / 3;
	m_nPrimitive ++;

	return KR_OK;
}

//--------------------------------------------------------------------------------------------------------------------------
//
//	2D Render
//
//--------------------------------------------------------------------------------------------------------------------------
KRESULT KRender::DrawQuad( s32 x, s32 y, s32 sx, s32 sy, KShader* pShader, KCOLOR Color, KCOLOR Specular, KRMODE RenderMode, float *pTU, float *pTV )
{
	KRenderFace		Face;
	KTLVertex		pVertex[4];
	KINDEX			pIndex[6]	= { 0, 2, 1, 1, 2, 3 };
	float			tu[4] = {0.0f, 1.0f, 0.0f, 1.0f};
	float			tv[4] = {0.0f, 0.0f, 1.0f, 1.0f};
	float			dtexelu = 0.0f;
	float			dtexelv = 0.0f;

	if( pTU && pTV )
	{
		for(u32 i = 0;i < 4;i ++)
		{
			tu[i] = pTU[i];
			tv[i] = pTV[i];
		}
	}

	pVertex[0].x		= (float)x;
	pVertex[0].y		= (float)y;
	pVertex[0].z		= 0.0f;
	pVertex[0].rhw		= 1.0f;
	pVertex[0].color	= Color;
	pVertex[0].specular	= Specular;
	pVertex[0].tu		= tu[0] + dtexelu;
	pVertex[0].tv		= tv[0] + dtexelv;

	pVertex[1].x		= (float)x+sx;
	pVertex[1].y		= (float)y;
	pVertex[1].z		= 0.0f;
	pVertex[1].rhw		= 1.0f;
	pVertex[1].color	= Color;
	pVertex[1].specular	= Specular;
	pVertex[1].tu		= tu[1] + dtexelu;
	pVertex[1].tv		= tv[1] + dtexelv;
	
	pVertex[2].x		= (float)x;
	pVertex[2].y		= (float)y+sy;
	pVertex[2].z		= 0.0f;
	pVertex[2].rhw		= 1.0f;
	pVertex[2].color	= Color;
	pVertex[2].specular	= Specular;
	pVertex[2].tu		= tu[2] + dtexelu;
	pVertex[2].tv		= tv[2] + dtexelv;
	
	pVertex[3].x		= (float)x+sx;
	pVertex[3].y		= (float)y+sy;
	pVertex[3].z		= 0.0f;
	pVertex[3].rhw		= 1.0f;
	pVertex[3].color	= Color;
	pVertex[3].specular	= Specular;
	pVertex[3].tu		= tu[3] + dtexelu;
	pVertex[3].tv		= tv[3] + dtexelv;
	
	Face.m_pVertex		= pVertex;
	Face.m_nVertex		= 4;
	Face.m_pIndices		= pIndex;
	Face.m_nIndices		= 6;

	SetShader( pShader );
	DrawTrianglesTL( Face );
	FlushTrianglesTL();

	return KR_OK;
}

//--------------------------------------------------------------------------------------------
KRESULT KRender::DrawText( s32 x, s32 y, char* pText, s32 Size )
{
	float	pTU[4], pTV[4];
	char	c;

	for( s32 i = 0; i < (s32)strlen( pText ); i ++ )
	{
		c = pText[i];

		s32		offsetx = c % 16;
		s32		offsety = c / 16;
		float	us = 1.0f / 16.0f;
		float	vs = 1.0f / 16.0f;
		float	up = us * offsetx;
		float	vp = vs * offsety;

		pTU[0] = up;		pTV[0] = vp;
		pTU[1] = up + us;	pTV[1] = vp;
		pTU[2] = up;		pTV[2] = vp + vs;
		pTU[3] = up + us;	pTV[3] = vp + vs;

		DrawQuad( x + i * Size, y, Size, Size, m_pFontShader, KRGBA( 255, 255, 255, 255 ), KRGBA( 0, 0, 0, 0 ), KRM_NORMAL, pTU, pTV );
	}

	return KR_OK;
}

//--------------------------------------------------------------------------------------------------------------------------
//
//	Shader
//
//--------------------------------------------------------------------------------------------------------------------------
KRESULT KRender::SetShader( KShader* pShader )
{
/*	m_pD3DDevice->SetRenderState( D3DRS_ALPHABLENDENABLE, TRUE );
	m_pD3DDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA );
	m_pD3DDevice->SetRenderState( D3DRS_SRCBLEND, D3DBLEND_SRCALPHA );

	m_pD3DDevice->SetTextureStageState( 0, D3DTSS_COLOROP, D3DTOP_MODULATE );
	m_pD3DDevice->SetTextureStageState( 0, D3DTSS_COLORARG1, D3DTA_TEXTURE );
	m_pD3DDevice->SetTextureStageState( 0, D3DTSS_COLORARG2, D3DTA_DIFFUSE );
	m_pD3DDevice->SetTextureStageState( 0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1 );
	m_pD3DDevice->SetTextureStageState( 0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE );

	m_pD3DDevice->SetTexture( 0, pShader ? pShader->GetpTexture() : NULL );
*/	
	if( pShader )
	{
		glBindTexture( GL_TEXTURE_2D, pShader->GetTextureId() );
		glEnable( GL_TEXTURE_2D );
	}
	else
		glDisable( GL_TEXTURE_2D );


	return KR_OK;
}

//--------------------------------------------------------------------------------------------------------------------------
KRESULT KRender::LoadShader( char* pFileName, KShader** ppShader )
{
	*ppShader = NULL;

	KShader*	pShader = new KShader();
	if( KR_FAILED( pShader->LoadShader( this, pFileName ) ) )
	{
		SafeDelete( pShader );
		return KR_ERROR;
	}

	*ppShader = pShader;

	return KR_OK;
}

//--------------------------------------------------------------------------------------------------------------------------
//
//	Mesh
//
//--------------------------------------------------------------------------------------------------------------------------
KRESULT KRender::LoadMesh( char* pFileName, KMesh** ppMesh, bool bLoadShader )
{
	*ppMesh = NULL;
	
	KMesh*	pMesh = new KMesh();
	if( KR_FAILED( pMesh->LoadMesh( this, pFileName, bLoadShader ) ) )
	{
		SafeDelete( pMesh );
		return KR_ERROR;
	}

	*ppMesh = pMesh;

	return KR_OK;
}

//--------------------------------------------------------------------------------------------------------------------------
KRESULT KRender::DrawMesh( KMatrix& Matrix, KMesh* pMesh )
{
	if( !pMesh )
		return KR_ERROR;

	glPushMatrix();
	glMultMatrixf( (float*)&Matrix );
	for( u32 i = 0; i < pMesh->GetnSubMeshes(); i ++ )
	{
		pMesh->DrawSubMesh( i );
	}
	glPopMatrix();

	return KR_OK;
}

//--------------------------------------------------------------------------------------------------------------------------
KRESULT KRender::DrawSubMesh( KMatrix& Matrix, KMesh* pMesh, u32 MeshId )
{
	if( !pMesh )
		return KR_ERROR;

	glPushMatrix();
	glMultMatrixf( (float*)&Matrix );
	pMesh->DrawSubMesh( MeshId );
	glPopMatrix();

	return KR_OK;
}

//--------------------------------------------------------------------------------------------------------------------------
KRESULT	KRender::ActivateLighting( bool bActivate )
{
	if( bActivate )
		glEnable( GL_LIGHTING );
	else
		glDisable( GL_LIGHTING );

	return KR_OK;
}

//--------------------------------------------------------------------------------------------------------------------------
KRESULT KRender::ActivateWireFrame( bool bActivate )
{
	glPolygonMode( GL_FRONT_AND_BACK, bActivate ? GL_LINE : GL_FILL );

	return KR_OK;
}
