//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: Parser.cpp
//	Author			: - Dino			dino@zythum-project.com
//
//	Date			: 12/12/2003
//	Modification	:
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------
#include "Parser.h"

//---------------------------------------------------------------------------------------------------------------------
KParser::KParser()
{
	m_pBuffer		= NULL;
	m_nSize			= 0;
	m_nCurrentPos	= 0;
	m_bIsNewLine	= false;
	m_pToken[0]		= '\0';
	m_nLine			= 0;
}

//---------------------------------------------------------------------------------------------------------------------
KParser::KParser( char* pFileName )
{
	m_pBuffer		= NULL;
	m_nSize			= 0;
	m_nCurrentPos	= 0;
	m_pToken[0]		= '\0';
	m_nLine			= 0;

	LoadASE( pFileName );
}

//---------------------------------------------------------------------------------------------------------------------
KParser::~KParser()
{
	Clean();
}


//---------------------------------------------------------------------------------------------------------------------
void KParser::Clean()
{
	m_nSize			= 0;
	m_nCurrentPos	= 0;
	if( m_pBuffer )
		Deletev( m_pBuffer );
	m_pBuffer		= NULL;
	m_pToken[0]		= '\0';
}

//---------------------------------------------------------------------------------------------------------------------
bool KParser::LoadASE( char* pFileName )
{
	FILE*	hFile;

	Clean();

	// Load le fichier ASCII
	if( !( hFile = fopen( pFileName, "rb" ) ) )
		return false;

	fseek( hFile, 0, SEEK_END );
	m_nSize = ftell( hFile );
	if( !m_nSize )
	{
		fclose( hFile );
		return false;
	}

	fseek( hFile, 0, SEEK_SET );

	m_pBuffer = new char[m_nSize];
	u32	Size = 0;
	u32	SizeRead = 0;
	
	while( Size < m_nSize )
	{
		SizeRead = fread( (unsigned char*)&m_pBuffer[Size], 1, MIN( m_nSize - Size, 1024 ), hFile );
		if( !SizeRead )
		{
			Deletev( m_pBuffer );
			m_pBuffer	= NULL;
			m_nSize		= 0;
			fclose( hFile );
			return false;
		}

		Size += SizeRead;
	}

	fclose( hFile );

	return true;
}

//---------------------------------------------------------------------------------------------------------------------
bool KParser::SearchToken( char* pToken )
{
	KASSERT( m_pBuffer );

	FindToken();
	while( !IsEOF() && stricmp( pToken, GetToken() ) )
	{
		FindToken();
	}

	if( IsEOF() )
		return false;

	return true;
}

// Cherche le prochain token
//---------------------------------------------------------------------------------------------------------------------
void KParser::FindToken()
{
	KASSERT( m_pBuffer );

	u32		nPos;
	u32		nTokenPos = 0;

	m_pToken[0]		= '\0';

	for( nPos = m_nCurrentPos; nPos < m_nSize; nPos ++ )
	{
		m_bIsNewLine = false;
		switch( m_pBuffer[nPos] )
		{
		case '\n':
		case '\r':
			m_bIsNewLine = true;
		case ' ':
		case '\t':
			if( nTokenPos )
			{
				m_pToken[nTokenPos++] = '\0';
				m_nCurrentPos = nPos;
				return;
			}
			break;
		case '#':
		case ';':
			// "#" ";" Commentaire : Passe la ligne
			while( ( m_pBuffer[++nPos] != '\n' ) && nPos < m_nSize );
			break;
		case '/':
			// "//" "/*" Commentaire ?
			if(nPos + 1 < m_nSize )
			{
				if( m_pBuffer[nPos+1] == '/' )
				{
					// Passe la ligne
					while( ( m_pBuffer[++nPos] != '\n' ) && nPos < m_nSize );
					break;
				}
				else
				if( m_pBuffer[nPos+1] == '*' )
				{
					// Cherche la fin du commentaire
					while( nPos + 1 < m_nSize )
					{
						nPos ++;
						if( ( m_pBuffer[nPos] == '*' ) && ( m_pBuffer[nPos + 1] == '/' ) )
						{
							nPos += 2;
							break;
						}
					}
					break;
				}
			}
		default:
			m_pToken[nTokenPos++] = m_pBuffer[nPos];
			break;
		}

		if( m_bIsNewLine )
			m_nLine ++;
	}
	m_nCurrentPos = nPos;
}

//---------------------------------------------------------------------------------------------------------------------
char* KParser::GetToken()
{
	return m_pToken;
}
