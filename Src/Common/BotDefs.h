//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: BotDefs.h
//	Author			: - Aitonfrere	
//					  - Dino			dino@zythum-project.com
//
//	Date			: 17/07/2003
//	Modification	:
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------
#ifndef	__BOTDEFS_H__
#define	__BOTDEFS_H__

#include "Types.h"
#include "Item.h"

#define	BOT_NAME_LENGTH		12		// Taille maximum du nom du Bot
#define BOT_MAX_CHILDS		5		// Taille de l'inventaire
#define BOT_POINTS			15		// Nombres de points r�artir
#define	BOT_MAX_LIFE		1000	// Points de vie maximum du robot

class KCell;

//--------------------------------------------------------------------------------------------
//
//  Vitesse => Nombres d'actions par tour
//
//      1           1
//      2           1
//      3           1
//      4           2
//      5           2
//      6           2
//      7           3
//      8           3
//      9           3
//     10 et +      4
//--------------------------------------------------------------------------------------------
#define GETNUMACTIONS( __Speed )	MIN( __Speed / 4 + 1, 4 )

//--------------------------------------------------------------------------------------------
//
//  Vitalit�=> Point de vie
//
//      1           100
//      2           200
//      3           300
//      4           400
//      5           500
//      6           600
//      7           700
//      8           800
//      9           900
//     10 et +      1000
//--------------------------------------------------------------------------------------------
#define GETLIFE( __Vitality )	MIN( __Vitality * 100, BOT_MAX_LIFE )

//--------------------------------------------------------------------------------------------
//
//   Vision   =>  Distance
//
//      1           1
//      2           2
//      3           2
//      4           3
//      5           3
//      6           4
//      7           4
//      8           5
//      9           5
//     10 et +      6
//--------------------------------------------------------------------------------------------
#define GETSIGHT( __Vision )	MIN( __Vision / 2 + 1, 6 )

//--------------------------------------------------------------------------------------------
//	La classe KItemInfo repr�ente un objet quelconque du labyrinthe (Robot, Objets divers, etc...)
class KItemInfo
{
protected:
	KITEMTYPE		m_Type;			// Type d'item
	KPt				m_Pos;			// Position de l'item dans le labyrinthe
	bool			m_bAlive;		// true si l'item est vivant, false s'il est mort

public:
	KItemInfo()
	{
		m_Type		= KIT_NONE;
		m_Pos		= KPt( 0, 0 );
		m_bAlive	= false;
	}

	void			SetType( KITEMTYPE Type )			{ m_Type = Type;					}
	void			SetPos( KPt Pos )					{ m_Pos = Pos;						}
	void			SetAlive( bool bAlive )				{ m_bAlive = bAlive;				}

	KITEMTYPE		GetType()							{ return m_Type;					}
	KPt				GetPos()							{ return m_Pos;						}
	bool			IsAlive()							{ return m_bAlive;					}
};

//--------------------------------------------------------------------------------------------
//	La classe KBotInfo contient la description du robot
class KBotInfo
{
protected:
	char	m_pBotName[BOT_NAME_LENGTH];	// Nom du robot
	u32		m_Vitality;
	u32		m_Power;
	u32		m_Speed;
	u32		m_Vision;

public:

	KBotInfo()
	{
		m_Vitality	= 3;
		m_Power		= 3;
		m_Speed		= 3;
		m_Vision	= 3;
		strcpy( m_pBotName, "NoName" );
	}

	void		SetpBotName( char* pBotName )
	{
		s32 Len = MIN( BOT_NAME_LENGTH - 1, (s32)strlen( pBotName ) );
		strncpy( m_pBotName, pBotName, Len );
		m_pBotName[Len] = 0;
	}

	char*		GetpBotName()					{ return m_pBotName;				}
	
	static bool	CheckPoints( u32 Vitality, u32 Power, u32 Speed, u32 Vision )
	{
		return ( ( Vitality + Power + Speed + Vision ) <= BOT_POINTS );
	}
	
	bool		SetPoints( u32 Vitality, u32 Power, u32 Speed, u32 Vision )
	{
		m_Vitality	= Vitality;
		m_Power		= Power;
		m_Speed		= Speed;
		m_Vision	= Vision;

		return true;
	}

	u32			GetVitality()		{ return m_Vitality;	}
	u32			GetPower()			{ return m_Power;		}
	u32			GetSpeed()			{ return m_Speed;		}
	u32			GetVision()			{ return m_Vision;		}
};

//--------------------------------------------------------------------------------------------
//	Action choisie par le robot lors de son tour
typedef enum _KBOTACTION
{
	KBA_NOTHING,		// Le robot ne fait rien, il passe son tour
	KBA_MOVE,			// Le robot veut se deplacer sur une case adjacente
	KBA_ATTACK,			// Le robot veut attaquer
	KBA_ATTACKLASER,	// Le robot veut attaquer avec un laser
	KBA_DROP,			// Le robot veut poser un objet de son inventaire
	KBA_COUNT
} KBOTACTION;

//--------------------------------------------------------------------------------------------
// Direction du d�lacement
typedef enum _KBOTMOVEDIR
{
	KBMD_UP,			// D�lacement en haut
	KBMD_DOWN,			// D�lacement en bas
	KBMD_LEFT,			// D�lacement en gauche
	KBMD_RIGHT,			// D�lacement en droite
	KBMD_COUNT
} KBOTMOVEDIR;

//--------------------------------------------------------------------------------------------
// Cible lors d'une attaque de type corps �corps
typedef enum _KBOTATTACKTARGET
{
	KBAT_UP,			// Attaque en haut
	KBAT_DOWN,			// Attaque en basse
	KBAT_LEFT,			// Attaque �gauche
	KBAT_RIGHT,			// Attaque �droite
	KBAT_COUNT
} KBOTATTACKTARGET;

//--------------------------------------------------------------------------------------------
// Cette classe contient l'action que le robot �choisit d'effectuer
class KBotAction
{
protected:
	KBOTACTION			m_Action;			// Type d'action
	KBOTMOVEDIR			m_MoveDir;			// Dans le cas d'une Action KBA_MOVE uniquement
	KBOTATTACKTARGET	m_AttackTarget;		// Dans le cas d'une Action KBA_ATTACK ou KBA_ATTACKLASER uniquement
	KITEMTYPE			m_DropItemType;		// Dans le cas d'une Action KBA_DROP uniquement

public:
	KBotAction() {}

	void				SetAction( KBOTACTION Action )					{ m_Action = Action;			}
	void				SetMoveDir( KBOTMOVEDIR MoveDir )				{ m_MoveDir = MoveDir;			}
	void				SetAttackTarget( KBOTATTACKTARGET Target )		{ m_AttackTarget = Target;		}
	void				SetDropItemType( KITEMTYPE ItemType )			{ m_DropItemType = ItemType;	}
	KBOTACTION			GetAction()										{ return m_Action;				}
	KBOTMOVEDIR			GetMoveDir()									{ return m_MoveDir;				}
	KBOTATTACKTARGET	GetAttackTarget()								{ return m_AttackTarget;		}
	KITEMTYPE			GetDropItemType()								{ return m_DropItemType;		}
};

//--------------------------------------------------------------------------------------------
//	La classe KBotActionInfo contient tout les ��ents sur : l'�at du bot, son inventaire, et ce qu'il voit
class KBotActionInfo
{
public:
	KPt				m_Pos;			// Position du robot
	u32				m_Life;			// Vie du robot
	u32				m_Power;		// Puissance du robot
	u32				m_Speed;		// Vitesse du robot
	u32				m_Agility;		// Agilit�du robot
	u32				m_Vision;		// Vision du robot
	
	KItemInfo*		m_pInventory;	// Contient les items pr�ent dans l'inventaire du robot
	u32				m_nInventory;	// Contient le nombre d'items dans l'inventaire du robot

	KCell*			m_pMapCell;		// Contient tout le labyrinthe, les cellules hors du champ de vision sont du type : UNKNOWN_CELL_TYPE
	
	KItemInfo*		m_pItems;		// Contient les items dans le champ de vision du robot
	u32				m_nItems;		// Contient le nombre d'items dans le champ de vision du robot	
};

#endif // __BOTDEFS_H__
