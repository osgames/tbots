//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: PngFile.cpp
//	Author			: - Dino			dino@zythum-project.com
//
//	Date			: 26/10/2003
//	Modification	:
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------
#include "PngFile.h"

//--------------------------------------------------------------------------------------------
KPngFile::KPngFile()
{
	m_hFile		= NULL;
	m_Width		= 0;
	m_Height	= 0;
	m_Depth		= 0;
	m_Channels	= 0;
	m_pPixels	= NULL;
}

//--------------------------------------------------------------------------------------------
KPngFile::~KPngFile()
{
	SafeFree( m_pPixels );
}

//--------------------------------------------------------------------------------------------
void KPngFile::ReadDataFunc(png_structp png_ptr, png_bytep data, png_size_t length)
{
   png_size_t check;

   /* fread() returns 0 on error, so it is OK to store this in a png_size_t
    * instead of an int, which is what fread() actually returns.
    */
   check = (png_size_t)fread(data, (png_size_t)1, length, (FILE *)png_ptr->io_ptr);
}

//--------------------------------------------------------------------------------------------
bool KPngFile::Load( char* pFileName )
{
	m_hFile = fopen( pFileName, "rb" );
	if( !m_hFile )
		return false;

    // first check the eight byte PNG signature
    png_byte            Header[8];
	
	fread( Header, 1, sizeof( Header ), m_hFile );

	if( png_sig_cmp( Header, 0, sizeof( Header ) ) )
		return false;

    // create the two png(-info) structures
	png_structp			png_ptr;
	png_ptr = png_create_read_struct( PNG_LIBPNG_VER_STRING, NULL, (png_error_ptr)NULL, (png_error_ptr)NULL);
	if( !png_ptr )
		return false;

	png_infop			info_ptr;
	info_ptr = png_create_info_struct( png_ptr );
	if( !info_ptr )
	{
		png_destroy_read_struct( &png_ptr, NULL, NULL );
		return false;
	}

	png_set_read_fn( png_ptr, (png_voidp)m_hFile, ReadDataFunc );

	png_set_sig_bytes(png_ptr, 8);

	// read all PNG info up to image data

	png_read_info(png_ptr, info_ptr);

	// get width, height, bit-depth and color-type
	int iColorType;
	png_get_IHDR(png_ptr, info_ptr, (png_uint_32*)&m_Width, (png_uint_32*)&m_Height, (int*)&m_Depth, &iColorType, NULL, NULL, NULL);

	// expand images of all color-type and bit-depth to 3x8 bit RGB images
	// let the library process things like alpha, transparency, background

	if (m_Depth == 16)
		png_set_strip_16(png_ptr);
	if (iColorType == PNG_COLOR_TYPE_PALETTE)
		png_set_expand(png_ptr);
	if (m_Depth < 8)
		png_set_expand(png_ptr);
	if (png_get_valid(png_ptr, info_ptr, PNG_INFO_tRNS))
		png_set_expand(png_ptr);
	if (iColorType == PNG_COLOR_TYPE_GRAY ||
		iColorType == PNG_COLOR_TYPE_GRAY_ALPHA)
		png_set_gray_to_rgb(png_ptr);

	// set the background color to draw transparent and alpha images over.
	png_color_16*		pBackground;
	png_color*			pBkgColor = NULL;

	if (png_get_bKGD(png_ptr, info_ptr, &pBackground))
	{
		png_set_background(png_ptr, pBackground, PNG_BACKGROUND_GAMMA_FILE, 1, 1.0);
		pBkgColor->red   = (u8) pBackground->red;
		pBkgColor->green = (u8) pBackground->green;
		pBkgColor->blue  = (u8) pBackground->blue;
	}
	else
	{
		pBkgColor = NULL;
	}

	// if required set gamma conversion
    double              dGamma;
	if (png_get_gAMA(png_ptr, info_ptr, &dGamma))
		png_set_gamma(png_ptr, (double) 2.2, dGamma);

	// after the transformations have been registered update info_ptr data

	png_read_update_info(png_ptr, info_ptr);

	// get again width, height and the new bit-depth and color-type

	png_get_IHDR(png_ptr, info_ptr, (png_uint_32*)&m_Width, (png_uint_32*)&m_Height, (int*)&m_Depth, &iColorType, NULL, NULL, NULL);


	// row_bytes is the width x number of channels
    png_uint_32         ulChannels;
    png_uint_32         ulRowBytes;

	ulRowBytes = png_get_rowbytes(png_ptr, info_ptr);
	ulChannels = png_get_channels(png_ptr, info_ptr);

	m_Channels = ulChannels;

	// now we can allocate memory to store the image

	m_pPixels = (u8*) malloc(ulRowBytes * m_Height * sizeof(png_byte));

	// and allocate memory for an array of row-pointers
	png_bytepp		ppbRowPointers;
	ppbRowPointers = (png_bytepp)malloc(m_Height * sizeof(png_bytep));

	// set the individual row-pointers to point at the correct offsets

	for (s32 i = 0; i < m_Height; i++)
		ppbRowPointers[i] = (unsigned char*)m_pPixels + i * ulRowBytes;

	// now we can go ahead and just read the whole image

	png_read_image(png_ptr, ppbRowPointers);

	// read the additional chunks in the PNG file (not really needed)

	png_read_end(png_ptr, NULL);

	// and we're done

	free (ppbRowPointers);
	ppbRowPointers = NULL;

	png_destroy_info_struct( png_ptr, &info_ptr );
	png_destroy_read_struct( &png_ptr, NULL, NULL );

	// yepp, done
		
	fclose( m_hFile );

	m_hFile = NULL;

	return true;
}

