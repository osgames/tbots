//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: Mesh.h
//	Author			: - Aitonfrere	
//					  - Dino			dino@zythum-project.com
//
//	Date			: 05/08/2003
//	Modification	:
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------
#ifndef	__MESH_H__
#define	__MESH_H__

#include "Types.h"
#include "Parser.h"
#include "Render.h"
#include "Shader.h"
#include <vector>

using namespace std;

class KRender;

//--------------------------------------------------------------------------------------------
class KMaterialMesh
{
public:
	char*				m_pFileName;
	KShader*			m_pShader;
	bool				m_bShaderAllocated;

	KMaterialMesh()
	{
		m_pFileName			= NULL;
		m_pShader			= NULL;
		m_bShaderAllocated	= false;
	}

	~KMaterialMesh()
	{
		SafeFree( m_pFileName );
		if( m_bShaderAllocated )
			SafeDelete( m_pShader );
	}
};

//--------------------------------------------------------------------------------------------
class KFaceMesh
{
public:
	KINDEX*				m_pIndex;
	u32					m_nIndex;
	u32					m_MaterialId;
	
	KFaceMesh()
	{
		m_pIndex		= NULL;
		m_nIndex		= 0;
		m_MaterialId	= (u32)-1;
	}

	~KFaceMesh()
	{
		SafeDeleteArray( m_pIndex );
	}
};

//--------------------------------------------------------------------------------------------
class KSubMesh
{
public:
	char*				m_pName;
	KMatrix				m_Matrix;
	KVertex*			m_pVertex;
	u32					m_nVertex;
	KFaceMesh*			m_pFaceMeshes;
	u32					m_nFaceMeshes;

	KSubMesh()
	{
		m_pName		= NULL;
		m_pVertex	= NULL;
		m_nVertex	= 0;
	}

	~KSubMesh()
	{
		SafeFree( m_pName );
		SafeDeleteArray( m_pVertex );
		SafeDeleteArray( m_pFaceMeshes );
	}
};


//--------------------------------------------------------------------------------------------
class KMesh
{
protected:
	KRender*			m_pRender;
	KParser				m_Parser;
	vector<KSubMesh*>	m_SubMeshes;
	KMaterialMesh*		m_pMaterials;
	u32					m_nMaterials;

	static int			FaceCompare( const void* pFace1, const void* pFace2 );

public:
	KMesh();
	~KMesh();

	KRESULT				LoadMesh( KRender* pRender, char* pFileName, bool bLoadShader = true );
	void				DrawSubMesh( u32 SubMeshId );
	void				Free();

	KMaterialMesh*		GetpMaterial( s32 MatId )		{ return &m_pMaterials[MatId];	}
	s32					GetnMaterials()					{ return m_nMaterials;			}
	u32					GetnSubMeshes()					{ return m_SubMeshes.size();	}

	bool				LoadASE( char* pFileName, bool bLoadShader = true );
	bool				SaveMSH( char* pFileName );
	bool				LoadMSH( char* pFileName, bool bLoadShader = true );
};

#endif // __MESH_H__
