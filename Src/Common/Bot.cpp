//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: Bot.cpp
//	Author			: - Aitonfrere	
//					  - Dino			dino@zythum-project.com
//
//	Date			: 16/07/2003
//	Modification	:
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------
#include "Render.h"
#include "Mesh.h"
#include "Shader.h"
#include "Light.h"
#include "Level.h"
#include "Timer.h"
#include "Regenerator.h"
#include "Medikit.h"
#include "Bonus.h"
#include "Bot.h"

KCOLOR g_pBotColor[] = {
	KRGB( 255, 255, 0 ),
	KRGB( 0, 0, 255 ),
	KRGB( 255, 255, 255 ),
	KRGB( 255, 150, 0 ),
	KRGB( 255, 0, 0 ),
	KRGB( 64, 64, 64 ),
	KRGB( 0, 255, 0 ),
	KRGB( 0, 255, 150 ),
	KRGB( 255, 0, 255 )
};

//--------------------------------------------------------------------------------------------
KMesh* KBot::m_pMesh			= NULL;
KMesh* KBot::m_pGraveMesh		= NULL;
KShader* KBot::m_pShader[9];

//--------------------------------------------------------------------------------------------
KBot::KBot( KRender* pRender, KLevel* pLevel )
: KItem( pRender, pLevel )
{
	m_Type			= KIT_BOT;
	m_Vitality		= 0;
	m_Life			= 0;
	m_Power			= 0;
	m_Speed			= 0;
	m_Vision		= 0;
	m_Action		= KBA_NOTHING;
	m_Number		= 0;
	m_bUseBinocular	= false;
	m_FireRange		= 0;
	m_bCheater		= false;

	strcpy( m_pName, "NoName" );

	m_pLight = new KLight( pRender );
	m_pLight->SetType( KLT_POINT );
//	m_pLight->SetType( KLT_SPOT );
	m_pLight->SetAmbient( KRGB( 0, 0, 0 ) );
	m_pLight->SetDiffuse( KRGB( 255, 0, 0 ) );
	m_pLight->Enable( true );
}

//--------------------------------------------------------------------------------------------
KBot::~KBot()
{
	SafeDelete( m_pLight );
}

//--------------------------------------------------------------------------------------------
bool KBot::Load( KRender* pRender )
{
	pRender->LoadMesh( "Meshes/bot", &m_pMesh, false );
	pRender->LoadMesh( "Meshes/grave", &m_pGraveMesh );

	pRender->LoadShader( "Textures/bot0.png", &m_pShader[0] );
	pRender->LoadShader( "Textures/bot1.png", &m_pShader[1] );
	pRender->LoadShader( "Textures/bot2.png", &m_pShader[2] );
	pRender->LoadShader( "Textures/bot3.png", &m_pShader[3] );
	pRender->LoadShader( "Textures/bot4.png", &m_pShader[4] );
	pRender->LoadShader( "Textures/bot5.png", &m_pShader[5] );
	pRender->LoadShader( "Textures/bot6.png", &m_pShader[6] );
	pRender->LoadShader( "Textures/bot7.png", &m_pShader[7] );
	pRender->LoadShader( "Textures/botc.png", &m_pShader[8] );

	return true;
}

//--------------------------------------------------------------------------------------------
void KBot::Unload()
{
	SafeDelete( m_pMesh );
	SafeDelete( m_pGraveMesh );

	SafeDelete( m_pShader[0] );
	SafeDelete( m_pShader[1] );
	SafeDelete( m_pShader[2] );
	SafeDelete( m_pShader[3] );
	SafeDelete( m_pShader[4] );
	SafeDelete( m_pShader[5] );
	SafeDelete( m_pShader[6] );
	SafeDelete( m_pShader[7] );
	SafeDelete( m_pShader[8] );
}

//--------------------------------------------------------------------------------------------
void KBot::Display()
{
	KFPt	Pos;
	KFPt	InterpoledPos = GetInterpoledPos();

	Pos.x	= InterpoledPos.x * CELL_SIZE;
	Pos.y	= InterpoledPos.y * CELL_SIZE;

	KPt	iPos = KPt( Pos );
	Display( iPos );
}

//--------------------------------------------------------------------------------------------
void KBot::Display( KPt& Pos )
{
	// Light
	KVector		Direction;

	float	Angle = (float)g_Timer.GetTime() / 100.0f;

	Angle += m_Number * PI / 8.0f;

	Direction.x = cosf( Angle ) + sinf( Angle );
	Direction.y = -sinf( Angle ) + cosf( Angle );
	Direction.z = 0.0f;

	s32	x = Pos.x + CELL_SIZE / 2;
	s32 y = m_pLevel->GetHeight() * CELL_SIZE - Pos.y + CELL_SIZE / 2;
	KVector	Position( (float)x, (float)y, 10.0f );
	
	m_pLight->SetDirection( Direction );
	m_pLight->SetDiffuse( g_pBotColor[m_Number] );
/*
	if( Pos == ( m_Pos * CELL_SIZE ) )
		m_pLight->SetType( KLT_POINT );
	else
		m_pLight->SetType( KLT_SPOT );
*/
//	SetMatrix( Pos, 0.0f );
	m_pLight->SetPosition( Position/*KVector( m_Matrix._41, m_Matrix._42, m_Matrix._43 )*/ );

//	m_pLight->SetPosition( KVector( 0.0f, 0.0f, 0.0f ) );

	// Define specific shader bot
	if( m_pMesh && m_pMesh->GetnMaterials() )
	{
		m_pMesh->GetpMaterial( 0 )->m_pShader = m_pShader[m_Number];
	}

	if( m_bCheater )
	{
		SetMatrix( Pos, 16.0f, PI, (float)g_Timer.GetTime() / 100.0f, cosf( (float)g_Timer.GetTime() / 100.0f ) / 5.0f );
		m_pRender->SetShader( m_pShader[8] );
		m_pRender->DrawSubMesh( m_Matrix, m_pMesh, 0 );
	}
	else
	{
		if( m_Life > 0 )
		{
			SetMatrix( Pos, 0.0f );
			m_pRender->DrawMesh( m_Matrix, m_pMesh );
		}
		else
		{
			SetMatrix( Pos, 0.0f );
			m_pRender->DrawMesh( m_Matrix, m_pGraveMesh );
		}
	}
}

//--------------------------------------------------------------------------------------------
void KBot::Display2D()
{
	KFPt	InterpoledPos = GetInterpoledPos();
	KPt	Pos( s32(SCREEN_OFFSETX + InterpoledPos.x * CELL_SIZE),	s32(InterpoledPos.y * CELL_SIZE) );
	KPt	Size( CELL_SIZE, CELL_SIZE );

	Display2D( Pos, Size );
}

//--------------------------------------------------------------------------------------------
void KBot::Display2D( KPt& Pos, KPt& Size )
{
	float	pTU[4], pTV[4];
	float	BotNumber = (float)m_Number;

	if( m_bCheater )
	{
		// Affiche le robot
		pTV[0] = 0.75f;
		pTV[1] = 0.75f;
		pTV[2] = 0.875f;
		pTV[3] = 0.875f;

		if( ( g_Timer.GetTime() / 100 ) % 2 )
		{
			// Change de sprite pour faire style qu'on a une super anim
			pTV[0] += 0.125f;
			pTV[1] += 0.125f;
			pTV[2] += 0.125f;
			pTV[3] += 0.125f;
		}

		pTU[0] = 0.75f;
		pTU[1] = 0.875f;
		pTU[2] = 0.75f;
		pTU[3] = 0.875f;
	}
	else
	if( m_Life > 0 )
	{
		// Affiche le robot
		pTV[0] = 0.5f;
		pTV[1] = 0.5f;
		pTV[2] = 0.625f;
		pTV[3] = 0.625f;

		if( ( g_Timer.GetTime() / 500 ) % 2 )
		{
			// Change de sprite pour faire style qu'on a une super anim
			pTV[0] += 0.125f;
			pTV[1] += 0.125f;
			pTV[2] += 0.125f;
			pTV[3] += 0.125f;
		}

		pTU[0] = BotNumber * 0.125f + 0.0f;
		pTU[1] = BotNumber * 0.125f + 0.125f;
		pTU[2] = BotNumber * 0.125f + 0.0f;
		pTU[3] = BotNumber * 0.125f + 0.125f;
	}
	else
	{
		// Affiche la tombe
		pTU[0] = 0.875;
		pTU[1] = 1.0f;
		pTU[2] = 0.875;
		pTU[3] = 1.0f;
		pTV[0] = 0.375f;
		pTV[1] = 0.375f;
		pTV[2] = 0.5f;
		pTV[3] = 0.5f;
	}

	m_pRender->DrawQuad( Pos.x, Pos.y, Size.x, Size.y, m_pLevel->GetpShader(), KRGBA( 255, 255, 255, 255 ), KRGBA( 0, 0, 0, 0 ), KRM_NORMAL, pTU, pTV );
}

//--------------------------------------------------------------------------------------------
s32 KBot::GetMaxLife()
{
	return GETLIFE( m_Vitality ); 
}

//--------------------------------------------------------------------------------------------
void KBot::Use( KItem* pItem )
{
	KASSERT( pItem );

	// Reg��ation de la vie
	if( pItem->GetType() == KIT_REGENERATOR )
	{
		m_Life = MIN( GetMaxLife(), m_Life + ((KRegenerator*)pItem)->GetRegenLife() ); 
		return;
	}

	// Redonne de la vie
	if( ( pItem->GetType() == KIT_MEDIKIT ) && ( m_Life < GetMaxLife() ) )
	{
		m_Life = MIN( GetMaxLife(), m_Life + ((KMedikit*)pItem)->GetHealLife() );

		// Detruit le m�ikit, il ne sert qu'une fois
		m_pLevel->RemovepItem( pItem );
		SafeDelete( pItem );
		return;
	}

	// Permet de voir tout le labyrinthe
	if( pItem->GetType() == KIT_BINOCULARS )
	{
		m_bUseBinocular = true;

		// Detruit les jumelles, elles ne servent qu'une fois
		m_pLevel->RemovepItem( pItem );
		SafeDelete( pItem );
		return;
	}
}

//--------------------------------------------------------------------------------------------
s32 KBot::GetSight()
{
	s32		Vision = m_Vision;

	// Ajoute les Bonus de Vision
	for( KItem* pChild = GetChilds().GetFirst(); pChild; pChild = GetChilds().GetNext( pChild ) )
	{
		if ( ( pChild->GetType() == KIT_BONUSVISION ) ||
			 ( ( pChild->GetType() == KIT_BONUSJOKER ) &&
			   ( ((KBonus *)pChild)->GetTypeJoker() == KIT_BONUSVISION) ) )
			Vision ++;
	}

	return GETSIGHT( Vision );
}

//--------------------------------------------------------------------------------------------
s32 KBot::GetPower()
{
	s32		Power = m_Power;

	// Ajoute les Bonus de Vision
	for( KItem* pChild = GetChilds().GetFirst(); pChild; pChild = GetChilds().GetNext( pChild ) )
	{
		if( pChild->GetType() == KIT_BONUSPOWER ||
			 ( ( pChild->GetType() == KIT_BONUSJOKER ) &&
			   ( ((KBonus *)pChild)->GetTypeJoker() == KIT_BONUSPOWER) ) )
			Power ++;
	}

	return Power;
}

//--------------------------------------------------------------------------------------------
s32 KBot::GetSpeed()
{
	s32		Speed = m_Speed;

	// Ajoute les Bonus de Vision
	for( KItem* pChild = GetChilds().GetFirst(); pChild; pChild = GetChilds().GetNext( pChild ) )
	{
		if( pChild->GetType() == KIT_BONUSSPEED ||
			 ( ( pChild->GetType() == KIT_BONUSJOKER ) &&
			   ( ((KBonus *)pChild)->GetTypeJoker() == KIT_BONUSSPEED) ) )
			Speed ++;
	}

	return Speed;
}

//--------------------------------------------------------------------------------------------
s32 KBot::GetVision()
{
	return m_Vision;
}
