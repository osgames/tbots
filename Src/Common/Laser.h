//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: Laser.h
//	Author			: - Aitonfrere	
//					  - Dino			dino@zythum-project.com
//
//	Date			: 21/07/2003
//	Modification	:
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------
#ifndef	__LASER_H__
#define	__LASER_H__

#include "Item.h"
#include "BotDefs.h"

//--------------------------------------------------------------------------------------------
class KLaser : public KItem
{
protected:
	s32						m_Range;
	
	static KMesh*			m_pMesh;

public:
	KLaser( KRender* pRender, KLevel* pLevel );
	virtual ~KLaser();

	virtual void			Display( KPt& Pos );
	virtual void			Display2D( KPt& Pos, KPt& Size );
	static bool				Load( KRender* pRender );
	static void				Unload();

	void					SetRange( s32 Range )		{ m_Range = Range;			}
	s32						GetRange()					{ return m_Range;			}
};

#endif // __LASER_H__
