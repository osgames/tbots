//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: Item.h
//	Author			: - Aitonfrere	
//					  - Dino			dino@zythum-project.com
//
//	Date			: 16/07/2003
//	Modification	:
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------
#ifndef	__ITEM_H__
#define	__ITEM_H__

#include "Types.h"
#include "Timer.h"
#include "Matrix.h"

class KRender;
class KLevel;

typedef enum _KITEMTYPE
{
	KIT_NONE,
	KIT_BOT,
	KIT_BONUSVISION,
	KIT_BONUSPOWER,
	KIT_BONUSSPEED,
	KIT_BONUSJOKER,
	KIT_REGENERATOR,
	KIT_MEDIKIT,
	KIT_BINOCULARS,
	KIT_MINE,
	KIT_LASER,
	KIT_COUNT
} KITEMTYPE;

//--------------------------------------------------------------------------------------------
class KItem
{
protected:
	KItem*			m_pFather;			// Le pere est l'item qui porte cet item, en g�n�ral le p�re est un KBot
	KList<KItem*>	m_Childs;			// Les fils sont les items que porte cet item, correspond a l'inventaire
	KRender*		m_pRender;			// Render...
	KLevel*			m_pLevel;			// Level...
	KPt				m_Pos;				// Position du Bot
	KPt				m_PrevPos;			// Position pr�cedante, permet de calculer l'interpolation
	KTIME			m_StartPos;			// Pour l'interpolation de position
	KTIME			m_EndPos;			// Pour l'interpolation de position
	KITEMTYPE		m_Type;				// Type d'item
	bool			m_bTakeAway;		// D�termine si l'objet est emportable, c'est a dire qu'on puisse le mettre dans l'inventaire
	bool			m_bVisible;			// Certains objets comme les visibles peuvent etre invisible
	KMatrix			m_Matrix;

	void			SetMatrix( KPt& Pos, float PosZ = 0.0f, float AngleX = 0.0f, float AngleY = 0.0f, float AngleZ = 0.0f );

public:
	KItem( KRender* pRender, KLevel* pLevel );
	virtual ~KItem();

	virtual void	Display();
	virtual void	Display( KPt& Pos )=0;
	virtual void	Display2D();
	virtual void	Display2D( KPt& Pos, KPt& Size )=0;

	KPt&			GetPos()							{ return m_Pos;				}
	void			SetPos( KPt& Pos, KTIME Delay = 0 );

	KFPt			GetInterpoledPos();

	void			SetType( KITEMTYPE Type )			{ m_Type = Type;			}
	KITEMTYPE		GetType()							{ return m_Type;			}

	void			SetpRender( KRender* pRender )		{ m_pRender = pRender;		}
	void			SetpLevel( KLevel* pLevel )			{ m_pLevel = pLevel;		}

	KRender*		GetpRender()						{ return m_pRender;			}

	void			SetpFather( KItem* pItem )			{ m_pFather = pItem;		}
	KItem*			GetpFather()						{ return m_pFather;			}
	KList<KItem*>&	GetChilds()							{ return m_Childs;			}

	bool			IsTakeAway()						{ return m_bTakeAway;		}
	void			Use( KItem* pItem ) {};

	bool			IsVisible()							{ return m_bVisible;		}
};


#endif // __ITEM_H__
