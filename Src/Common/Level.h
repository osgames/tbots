//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: Level.h
//	Author			: - Aitonfrere	
//					  - Dino			dino@zythum-project.com
//
//	Date			: 16/07/2003
//	Modification	:
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------
#ifndef	__LEVEL_H__
#define	__LEVEL_H__

#include "Types.h"
#include "BotDefs.h"
#include "Render.h"
#include <list>

using namespace std;

#define	CELL_SIZE			16
#define	SCREEN_OFFSETX		240
#define	UNKNOWN_CELL_TYPE	0xFFFF

#define LEVEL_NAME_LENGTH	20
#define LEVEL_AUTHOR_LENGTH	20
#define LEVEL_WIDTH_MAX		40
#define LEVEL_HEIGHT_MAX	35

#define CELL_FLAG_LOCKED	0x8000
#define CELL_FLAG_FLIPH		0x4000
#define CELL_FLAG_FLIPV		0x2000

#define CELL_MASK_TYPE		0x1FFF

#define	SIGHT_UNLIMITED		(-1)

//--------------------------------------------------------------------------------------------
typedef struct
{
	char	Name[LEVEL_NAME_LENGTH];
	char	Author[LEVEL_NAME_LENGTH];
	u8		Width;
	u8		Height;
} KTBotLevelHeader;

typedef u16 CellType;

//--------------------------------------------------------------------------------------------

class KItem;

//--------------------------------------------------------------------------------------------
class KCell
{
protected:
	CellType	m_Type;
public:
	KCell()
	{
		m_Type = 0;
	}

	void		SetType( CellType Type )	{ m_Type = Type;		}
	CellType	GetType()					{ return m_Type;		}
};

//--------------------------------------------------------------------------------------------
class KLevel
{
protected:
	u32				m_Width;
	u32				m_Height;
	KCell*			m_pCell;
	KRender*		m_pRender;
	KShader*		m_pShader;
	KList<KItem*>	m_Items;
	KItemInfo*		m_pSightItems;
	KCell*			m_pSightCell;

public:
	KLevel( KRender* pRender );
	~KLevel();

	KRESULT			Load( char* pLevelName );
	KRESULT			Unload();
	KRESULT			Draw2D( KPt& Pos );
	KRESULT			Draw();

	KRESULT			Create( u32 Width, u32 Height );
	KRESULT			Flush();
	KRESULT			FlushItems();

	u32				GetWidth()								{ return m_Width;									}
	u32				GetHeight()								{ return m_Height;									}
																											
	KCell*			GetpCell( u32 Width, u32 Height )		{ return &m_pCell[Height * m_Width + Width];		}
	KCell*			GetpSightCell( u32 Width, u32 Height )	{ return &m_pSightCell[Height * m_Width + Width];	}

	void			AddpItem( KItem* pItem )				{ m_Items.Add( pItem );								}
	void			RemovepItem( KItem* pItem )				{ m_Items.Remove( pItem );							}
	KItem*			GetpItem( u32 x, u32 y );																
	KItem*			GetpNotItem( u32 x, u32 y, KITEMTYPE Type );																
	KItem*			GetpItem( u32 x, u32 y, KITEMTYPE Type );																
	KItem*			GetpItem( u32 ItemId );																	
	u32				GetnItems()								{ return (u32)m_Items.GetSize();					}
	u32				GetnItems( KITEMTYPE Type );
																											
	KShader*		GetpShader()							{ return m_pShader;									}

	bool			CanMove( s32 x, s32 y );
	bool			CanSpawn( s32 x, s32 y );

	void			DrawFlame( KPt Pos, u32 Type );
	void			DrawHalo( KPt Pos );
	void			DrawMineExplosion( KPt Pos, u32 Type );
	void			DrawLaser( KPt Pos, KPt Dir, s32 Range, u32 Type );
	void			DrawLaser( KPt Pos, bool bUpDown );

	static KPt		GetDeltaPos( KBOTMOVEDIR MoveDir );
	static KPt		GetDeltaAttack( KBOTATTACKTARGET Targer );

	KCell*			GetpSightCell( KPt& Pos, s32 SightDistance );
	u32				GetpSightItem( KPt& Pos, s32 SightDistance, KItemInfo** pItems );
};

#endif // __LEVEL_H__
