//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: Shader.h
//	Author			: - Aitonfrere	
//					  - Dino			dino@zythum-project.com
//
//	Date			: 16/07/2003
//	Modification	:
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------
#ifndef	__SHADER_H__
#define	__SHADER_H__

#include "Types.h"
#include <GL/gl.h>

class KRender;

//--------------------------------------------------------------------------------------------
class KShaderInfo
{
public:
	s32		m_Width;
	s32		m_Height;
};

typedef KShaderInfo		KSHADERINFO;

//--------------------------------------------------------------------------------------------
class KShader
{
protected:
	GLuint				m_TexId;
	KSHADERINFO			m_Info;

public:
	KShader();
	~KShader();

	KRESULT				LoadShader( KRender* pRender, char* pFileName );
	void				Free();

	KSHADERINFO			GetShaderInfo()		{ return m_Info;		}
	GLuint				GetTextureId()		{ return m_TexId;		}
//	IDirect3DTexture9*	GetpTexture()		{ return m_pTexture;	}
};

#endif // __SHADER_H__
