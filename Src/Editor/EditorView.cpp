// EditorView.cpp : implementation of the CEditorView class
//

#include "stdafx.h"
#include "Editor.h"

#include "..\Common\Render.h"
#include "..\Common\Level.h"

#include "ChildFrm.h"
#include "MainFrm.h"

#include "EditorDoc.h"
#include "EditorView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


////////////////////////////////////////////////////////////////////////////////
// Global variables
////////////////////////////////////////////////////////////////////////////////
UINT gToolsID[] = { ID_BUTTON_DRAW, ID_BUTTON_FILL, ID_BUTTON_PICKER };


//==============================================================================
//
// 								CLASS CEditorView
//
//==============================================================================


IMPLEMENT_DYNCREATE(CEditorView, CView)

BEGIN_MESSAGE_MAP(CEditorView, CView)
	//{{AFX_MSG_MAP(CEditorView)
	ON_WM_DESTROY()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_RBUTTONDOWN()
	ON_WM_SETCURSOR()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


////////////////////////////////////////////////////////////////////////////////
// CEditorView constructor & destructor
//																	CEditorView
////////////////////////////////////////////////////////////////////////////////
CEditorView::CEditorView()
{
	m_pRender		= NULL;
	m_pLevel		= NULL;
}

CEditorView::~CEditorView()
{
	SafeDelete(m_pLevel);
	SafeDelete(m_pRender);
}

////////////////////////////////////////////////////////////////////////////////
// PreCreateWindow
//																	CEditorView
////////////////////////////////////////////////////////////////////////////////
BOOL CEditorView::PreCreateWindow(CREATESTRUCT& cs)
{
	return CView::PreCreateWindow(cs);
}

////////////////////////////////////////////////////////////////////////////////
// OnInitialUpdate
//																	CEditorView
////////////////////////////////////////////////////////////////////////////////
void CEditorView::OnInitialUpdate() 
{
	CView::OnInitialUpdate();

	InitRender();

	CEditorDoc *pDocument = GetDocument();
	ASSERT(pDocument);

	NewLevel(pDocument->GetMazeWidth(),pDocument->GetMazeHeight());

	((CChildFrame *)GetParentFrame())->SetWindowPos(NULL,
													0,
													0,
													pDocument->GetMazeWidth()  * CELL_SIZE + (GetSystemMetrics(SM_CXSIZEFRAME)+1) * 2,
													pDocument->GetMazeHeight() * CELL_SIZE + (GetSystemMetrics(SM_CYSIZEFRAME)+1) * 2 + GetSystemMetrics(SM_CYCAPTION),
													SWP_NOMOVE);

	CRect	Rect;
	GetClientRect(&Rect);
	
	m_pRender->Reset( m_hWnd, Rect.right - Rect.left, Rect.bottom - Rect.top, 32, false );
}

////////////////////////////////////////////////////////////////////////////////
// OnDraw
//																	CEditorView
////////////////////////////////////////////////////////////////////////////////
void CEditorView::OnDraw(CDC* pDC)
{
	CEditorDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	DrawFrame();
}


#ifdef _DEBUG

////////////////////////////////////////////////////////////////////////////////
// AssertValid
//																	CEditorView
////////////////////////////////////////////////////////////////////////////////
void CEditorView::AssertValid() const
{
	CView::AssertValid();
}

////////////////////////////////////////////////////////////////////////////////
// Dump
//																	CEditorView
////////////////////////////////////////////////////////////////////////////////
void CEditorView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

////////////////////////////////////////////////////////////////////////////////
// GetDocument
//																	CEditorView
////////////////////////////////////////////////////////////////////////////////
CEditorDoc* CEditorView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CEditorDoc)));
	return (CEditorDoc*)m_pDocument;
}

#endif //_DEBUG


////////////////////////////////////////////////////////////////////////////////
// OnDestroy
//																	CEditorView
////////////////////////////////////////////////////////////////////////////////
void CEditorView::OnDestroy() 
{
	CView::OnDestroy();
	
	if (m_pLevel)
		m_pLevel->Flush();

	if (m_pRender)
		m_pRender->End();		
}

////////////////////////////////////////////////////////////////////////////////
// PreTranslateMessage
//																	CEditorView
////////////////////////////////////////////////////////////////////////////////
BOOL CEditorView::PreTranslateMessage(MSG* pMsg) 
{
	CToolBarCtrl &ToolBar = ((CMainFrame *)AfxGetApp()->m_pMainWnd)->m_wndToolBar.GetToolBarCtrl();
	BOOL bRefresh = FALSE;
	
	if (GetKeyState(VK_LCONTROL) & 0x8000)
	{
		ToolBar.CheckButton(ID_BUTTON_PICKER,true);
		::SetCursor(gImageListTools.ExtractIcon(2));
		gTBotsEditorApp.SelectEditorTool(EditorToolBlockPicker);
	}
	else
	{
		if (gTBotsEditorApp.ToolHasChanged())
		{
			gTBotsEditorApp.RestoreTool();
			bRefresh = TRUE;
		}

		if (pMsg->message == WM_KEYDOWN || pMsg->message == WM_SYSKEYDOWN)
		{
			switch(pMsg->wParam)
			{
				case 'D' :
				case 'd' :
					gTBotsEditorApp.SelectEditorTool(EditorToolDraw);
					break;

				case 'F' :
				case 'f' :
					gTBotsEditorApp.SelectEditorTool(EditorToolFill);
					break;
			}
		}

		if ( gTBotsEditorApp.ToolHasChanged() || bRefresh )
		{
			ToolBar.CheckButton(gToolsID[gTBotsEditorApp.GetCurrentTool()],true);
			::SetCursor(gImageListTools.ExtractIcon(gTBotsEditorApp.GetCurrentTool()));
		}

		gTBotsEditorApp.BackupTool();
	}

	return CView::PreTranslateMessage(pMsg);
}

////////////////////////////////////////////////////////////////////////////////
// OnSetCursor
//																	CEditorView
////////////////////////////////////////////////////////////////////////////////
BOOL CEditorView::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
	switch(gTBotsEditorApp.GetCurrentTool())
	{
		case EditorToolDraw :
			::SetCursor(gImageListTools.ExtractIcon(0));
			return true;

		case EditorToolFill :
			::SetCursor(gImageListTools.ExtractIcon(1));
			return true;

		case EditorToolBlockPicker :
			::SetCursor(gImageListTools.ExtractIcon(2));
			return true;
	}
	
	return CView::OnSetCursor(pWnd, nHitTest, message);
}

////////////////////////////////////////////////////////////////////////////////
// NewLevel
//																	CEditorView
////////////////////////////////////////////////////////////////////////////////
void CEditorView::NewLevel(u32 _Width, u32 _Height)
{
	KASSERT(m_pRender);
	KASSERT(!m_pLevel);

	CEditorDoc *pDocument = GetDocument();
	
	m_pLevel	= new KLevel(m_pRender);

	m_pLevel->Create(_Width, _Height);

	if (!pDocument)
		return;

	for (u32 y = 0; y < _Height; y++ )
		for (u32 x = 0; x < _Width; x++ )
		{
			m_pLevel->GetpCell( x, y )->SetType(pDocument->GetMazeCell(x,y));
		}
}

////////////////////////////////////////////////////////////////////////////////
// ClearLevel
//																	CEditorView
////////////////////////////////////////////////////////////////////////////////
void CEditorView::ClearLevel()
{
	KASSERT(m_pLevel);
	
	for (u32 y = 0; y < m_pLevel->GetHeight(); y++ )
		for (u32 x = 0; x < m_pLevel->GetWidth(); x++ )
		{
			m_pLevel->GetpCell(x, y)->SetType(0);
		}
}

////////////////////////////////////////////////////////////////////////////////
// OnMouseMove
//																	CEditorView
////////////////////////////////////////////////////////////////////////////////
void CEditorView::OnMouseMove(UINT nFlags, CPoint point) 
{
	if (!m_pLevel)
		return;

	u32 x = point.x / CELL_SIZE;
	u32 y = point.y / CELL_SIZE;

	if ( x >= m_pLevel->GetWidth() )
		return;
	if ( y >= m_pLevel->GetHeight() )
		return;

	if ( nFlags & MK_RBUTTON )
		PaintLevel(x, y, true, false);

	if ( nFlags & MK_LBUTTON )
		PaintLevel(x, y, false, false);

	CView::OnMouseMove(nFlags, point);
}

////////////////////////////////////////////////////////////////////////////////
// OnLButtonDown
//																	CEditorView
////////////////////////////////////////////////////////////////////////////////
void CEditorView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	if (!m_pLevel)
		return;

	u32 x = point.x / CELL_SIZE;
	u32 y = point.y / CELL_SIZE;

	if ( x >= m_pLevel->GetWidth() )
		return;
	
	if ( y >= m_pLevel->GetHeight() )
		return;

	PaintLevel(x, y, false, true);

	CView::OnLButtonDown(nFlags, point);
}

////////////////////////////////////////////////////////////////////////////////
// OnRButtonDown
//																	CEditorView
////////////////////////////////////////////////////////////////////////////////
void CEditorView::OnRButtonDown(UINT nFlags, CPoint point) 
{
	if (!m_pLevel)
		return;

	u32 x = point.x / CELL_SIZE;
	u32 y = point.y / CELL_SIZE;

	if ( x >= m_pLevel->GetWidth() )
		return;
	
	if ( y >= m_pLevel->GetHeight() )
		return;

	PaintLevel(x, y, true, true);
	
	CView::OnRButtonDown(nFlags, point);
}

////////////////////////////////////////////////////////////////////////////////
// InitRender
//																	CEditorView
////////////////////////////////////////////////////////////////////////////////
void CEditorView::InitRender()
{
	KASSERT(!m_pRender);
	KASSERT(!m_pLevel);

	CRect	Rect;
		
	GetClientRect(&Rect);

	m_pRender = new KRender();
	
	m_pRender->Init( m_hWnd, ::GetDC( m_hWnd ), Rect.right - Rect.left, Rect.bottom - Rect.top, 32, false );
}

////////////////////////////////////////////////////////////////////////////////
// DrawFrame
//																	CEditorView
////////////////////////////////////////////////////////////////////////////////
void CEditorView::DrawFrame()
{
	if (m_pRender)
	{
		m_pRender->BeginScene();

		DrawLevel();

		m_pRender->EndScene();
		m_pRender->Flip();
	}
}

////////////////////////////////////////////////////////////////////////////////
// DrawLevel
//																	CEditorView
////////////////////////////////////////////////////////////////////////////////
void CEditorView::DrawLevel()
{
	// Draw the level
	if (m_pLevel)
		m_pLevel->Draw2D( KPt( 0, 0 ) );

	// Draw the helpers
	for (u32 y = 0; y < m_pLevel->GetHeight(); y++ )
		for (u32 x = 0; x < m_pLevel->GetWidth(); x++ )
		{
			if (m_pLevel->GetpCell( x, y )->GetType() & CELL_FLAG_LOCKED)
				m_pRender->DrawQuad( x * CELL_SIZE, y * CELL_SIZE, CELL_SIZE, CELL_SIZE, 0, COLOR_CELL_LOCKED );
		}
}

////////////////////////////////////////////////////////////////////////////////
// PaintLevel
//																	CEditorView
////////////////////////////////////////////////////////////////////////////////
class KTestItem : public KItem
{
protected:

public:
	KTestItem( KRender* pRender, KLevel* pLevel );

	virtual void			Display( KPt& Pos, KPt& Size );
};

KTestItem::KTestItem( KRender* pRender, KLevel* pLevel )
: KItem( pRender, pLevel )
{
	m_Type		= KIT_BONUSJOKER;
}

//--------------------------------------------------------------------------------------------
void KTestItem::Display( KPt& Pos, KPt& Size )
{
	float	pTU[4], pTV[4];

	CellType Type, TypeNoFlags;
	u16 Width = m_pLevel->GetpShader()->GetShaderInfo().m_Width/CELL_SIZE;

	Type = (CellType)m_Type;
	TypeNoFlags = Type & CELL_MASK_TYPE;

	
	if (Type & CELL_FLAG_FLIPH)
	{
		pTU[1] = pTU[3] = ((float) (TypeNoFlags % Width) * 0.125f);
		pTU[0] = pTU[2] = ((float) (TypeNoFlags % Width) * 0.125f) + 0.125f;
	}
	else
	{
		pTU[0] = pTU[2] = ((float) (TypeNoFlags % Width) * 0.125f);
		pTU[1] = pTU[3] = ((float) (TypeNoFlags % Width) * 0.125f) + 0.125f;
	}
	
	if (Type & CELL_FLAG_FLIPV)
	{
		pTV[2] = pTV[3] = ((float) ((TypeNoFlags / Width) * 0.125f));
		pTV[0] = pTV[1] = ((float) ((TypeNoFlags / Width) * 0.125f)) + 0.125f;
	}
	else
	{
		pTV[0] = pTV[1] = ((float) ((TypeNoFlags / Width) * 0.125f));
		pTV[2] = pTV[3] = ((float) ((TypeNoFlags / Width) * 0.125f)) + 0.125f;
	}

	m_pRender->DrawQuad( m_Pos.x * CELL_SIZE, m_Pos.y * CELL_SIZE, Size.x, Size.y, m_pLevel->GetpShader(), KRGBA( 255, 255, 255, 255 ), KRGBA( 0, 0, 0, 0 ), KRM_NORMAL, pTU, pTV );
}


void CEditorView::PaintLevel(u32 x, u32 y, bool bErase, bool bClick)
{
	switch(gTBotsEditorApp.GetCurrentTool())
	{
		case EditorToolDraw :
			m_pLevel->GetpCell(x,y)->SetType(bErase?0:gTBotsEditorApp.GetCurrentBrush() | gTBotsEditorApp.m_CellFlags);
			GetDocument()->SetModifiedFlag(true);
			break;

		case EditorToolFill :
			m_CellToFill = m_pLevel->GetpCell(x,y)->GetType();
			FillLevel(x,y,bErase?0:gTBotsEditorApp.GetCurrentBrush());

			GetDocument()->SetModifiedFlag(true);
			break;

		case EditorToolBlockPicker :
			gTBotsEditorApp.SetCurrentBrush(m_pLevel->GetpCell(x,y)->GetType());
			break;
	}
}

////////////////////////////////////////////////////////////////////////////////
// FillLevel
//																	CEditorView
////////////////////////////////////////////////////////////////////////////////
void CEditorView::FillLevel(u32 x, u32 y, CellType Brush)
{
	if ( (x >= m_pLevel->GetWidth()) ||  (y >= m_pLevel->GetHeight()) )
		return;

	if (m_pLevel->GetpCell(x,y)->GetType() != m_CellToFill)
		return;

	if (m_pLevel->GetpCell(x,y)->GetType() == Brush)
		return;

	m_pLevel->GetpCell(x,y)->SetType(Brush);

	// Fill left direction
	FillLevel(x-1,y,Brush);
	
	// Fill right direction
	FillLevel(x+1,y,Brush);
	
	// Fill up direction
	FillLevel(x,y-1,Brush);

	// Fill down direction
	FillLevel(x,y+1,Brush);
}
