// NewMazeDlg.cpp : implementation file
//

#include "stdafx.h"
#include "editor.h"

#include "..\Common\Level.h"
#include "NewMazeDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CNewMazeDlg dialog


CNewMazeDlg::CNewMazeDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CNewMazeDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CNewMazeDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	m_MazeName		= "Maze";
	m_MazeAuthor	= "";
	m_MazeWidth		= 20;
	m_MazeHeight	= 15;
}


void CNewMazeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CNewMazeDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP

	DDX_Text(pDX, IDC_EDIT_MAZE_NAME, m_MazeName);
	DDX_Text(pDX, IDC_EDIT_MAZE_AUTHOR, m_MazeAuthor);
	DDX_Text(pDX, IDC_EDIT_MAZE_WIDTH, m_MazeWidth);
	DDX_Text(pDX, IDC_EDIT_MAZE_HEIGHT, m_MazeHeight);

	DDV_MaxChars(pDX, m_MazeName, LEVEL_NAME_LENGTH);
	DDV_MaxChars(pDX, m_MazeAuthor, LEVEL_AUTHOR_LENGTH);
	DDV_MinMaxInt(pDX, m_MazeWidth, 8, LEVEL_WIDTH_MAX);
	DDV_MinMaxInt(pDX, m_MazeHeight, 8, LEVEL_HEIGHT_MAX);
}


BEGIN_MESSAGE_MAP(CNewMazeDlg, CDialog)
	//{{AFX_MSG_MAP(CNewMazeDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNewMazeDlg message handlers
