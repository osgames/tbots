// EditorDoc.cpp : implementation of the CEditorDoc class
//

#include "stdafx.h"
#include "Editor.h"

#include "EditorView.h"
#include "EditorDoc.h"

#include "NewMazeDlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


//==============================================================================
//
// 								CLASS CEditorDoc
//
//==============================================================================


IMPLEMENT_DYNCREATE(CEditorDoc, CDocument)

BEGIN_MESSAGE_MAP(CEditorDoc, CDocument)
	//{{AFX_MSG_MAP(CEditorDoc)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


////////////////////////////////////////////////////////////////////////////////
// CEditorDoc constructor & destructor
//																	 CEditorDoc
////////////////////////////////////////////////////////////////////////////////
CEditorDoc::CEditorDoc()
{
	memset(&m_MazeHeader,0,sizeof(KTBotLevelHeader));
	m_MazeCells = NULL;
}

CEditorDoc::~CEditorDoc()
{
	SafeFree(m_MazeCells);
}

////////////////////////////////////////////////////////////////////////////////
// OnNewDocument
//																	 CEditorDoc
////////////////////////////////////////////////////////////////////////////////
BOOL CEditorDoc::OnNewDocument()
{
	CNewMazeDlg dlg;

	if (dlg.DoModal() == IDOK)
	{
		m_MazeHeader.Width = dlg.m_MazeWidth;
		m_MazeHeader.Height = dlg.m_MazeHeight;

		strcpy(m_MazeHeader.Name,dlg.m_MazeName);
		strcpy(m_MazeHeader.Author,dlg.m_MazeAuthor);

		SafeFree(m_MazeCells);
		m_MazeCells = (CellType *) malloc (sizeof(CellType)*m_MazeHeader.Width*m_MazeHeader.Height);
		memset(m_MazeCells,0,sizeof(CellType)*m_MazeHeader.Width*m_MazeHeader.Height);

		if (!CDocument::OnNewDocument())
			return FALSE;
	}
	else
		return FALSE;

	return TRUE;
}

////////////////////////////////////////////////////////////////////////////////
// GetView
//																	 CEditorDoc
////////////////////////////////////////////////////////////////////////////////
CEditorView *CEditorDoc::GetView()
{
	POSITION pos = GetFirstViewPosition();

	return (CEditorView *)GetNextView(pos);
}

////////////////////////////////////////////////////////////////////////////////
// Serialize
//																	 CEditorDoc
////////////////////////////////////////////////////////////////////////////////
void CEditorDoc::Serialize(CArchive& ar)
{
	CellType Type;

	if (ar.IsStoring())
	{
		ar.Write(&m_MazeHeader,sizeof(KTBotLevelHeader));

		for (u32 y = 0; y < m_MazeHeader.Height; y ++ )
			for (u32 x = 0; x < m_MazeHeader.Width; x ++ )
			{
				Type = GetView()->m_pLevel->GetpCell( x, y )->GetType();
				ar.Write(&Type, sizeof(Type));
			}
	}
	else
	{
		ar.Read(&m_MazeHeader,sizeof(KTBotLevelHeader));

		SafeFree(m_MazeCells);
		m_MazeCells = (CellType *) malloc (sizeof(CellType)*m_MazeHeader.Width*m_MazeHeader.Height);

		ar.Read(m_MazeCells,m_MazeHeader.Width*m_MazeHeader.Height*sizeof(CellType));
	}
}


#ifdef _DEBUG

////////////////////////////////////////////////////////////////////////////////
// AssertValid
//																	 CEditorDoc
////////////////////////////////////////////////////////////////////////////////
void CEditorDoc::AssertValid() const
{
	CDocument::AssertValid();
}

////////////////////////////////////////////////////////////////////////////////
// Dump
//																	 CEditorDoc
////////////////////////////////////////////////////////////////////////////////
void CEditorDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}

#endif //_DEBUG
