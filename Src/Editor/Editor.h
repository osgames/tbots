// Editor.h : main header file for the EDITOR application
//

#if !defined(AFX_EDITOR_H__CDC4910C_2BB2_4AA9_99A3_C48E601C6F8E__INCLUDED_)
#define AFX_EDITOR_H__CDC4910C_2BB2_4AA9_99A3_C48E601C6F8E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols


#include "..\Common\Types.h"
#include "..\Common\Level.h"


////////////////////////////////////////////////////////////////////////////////
// Defines & Enumerations
////////////////////////////////////////////////////////////////////////////////
enum EditorTool
{
	EditorToolDraw,
	EditorToolFill,
	EditorToolBlockPicker,

	EditorToolLast
};


/////////////////////////////////////////////////////////////////////////////
// CEditorApp:
// See Editor.cpp for the implementation of this class
//

class CEditorApp : public CWinApp
{
protected :

	EditorTool	m_CurrentTool;
	EditorTool	m_OldTool;
	CellType	m_CurrentBrush;

public :
	CellType	m_CellFlags;

	CEditorApp();

	void Init();
	void Release();

	void SelectEditorTool(EditorTool _SelectedTool)	{ m_CurrentTool = _SelectedTool; }
	BOOL ToolHasChanged()							{ return (m_OldTool != m_CurrentTool); }
	void RestoreTool()								{ m_CurrentTool = m_OldTool; }
	void BackupTool()								{ m_OldTool = m_CurrentTool; }
	EditorTool GetCurrentTool()						{ return m_CurrentTool; }

	CellType GetCurrentBrush()						{ return m_CurrentBrush; }
	void SetCurrentBrush(CellType _NewBrush)		{ m_CurrentBrush = _NewBrush; }

	
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEditorApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	virtual BOOL OnIdle(LONG lCount);
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CEditorApp)
	afx_msg void OnAppAbout();
	afx_msg void OnEditionSettings();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


////////////////////////////////////////////////////////////////////////////////
// Global variables
////////////////////////////////////////////////////////////////////////////////
extern CImageList gImageListTools;
extern CEditorApp gTBotsEditorApp;


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EDITOR_H__CDC4910C_2BB2_4AA9_99A3_C48E601C6F8E__INCLUDED_)
