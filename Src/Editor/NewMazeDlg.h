#if !defined(AFX_NEWMAZEDLG_H__F5805863_92B2_4D91_BBE9_8BB5B37AF1DA__INCLUDED_)
#define AFX_NEWMAZEDLG_H__F5805863_92B2_4D91_BBE9_8BB5B37AF1DA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// NewMazeDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CNewMazeDlg dialog

class CNewMazeDlg : public CDialog
{
// Construction
public:
	CNewMazeDlg(CWnd* pParent = NULL);   // standard constructor

	CString m_MazeName;
	CString m_MazeAuthor;
	int		m_MazeWidth;
	int		m_MazeHeight;

// Dialog Data
	//{{AFX_DATA(CNewMazeDlg)
	enum { IDD = IDD_NEWMAZE };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNewMazeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CNewMazeDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NEWMAZEDLG_H__F5805863_92B2_4D91_BBE9_8BB5B37AF1DA__INCLUDED_)
