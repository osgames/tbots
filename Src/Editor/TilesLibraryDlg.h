#if !defined(AFX_TILESLIBRARYDLG_H__47BECE79_5BA4_4C22_B48B_C5A8120F8075__INCLUDED_)
#define AFX_TILESLIBRARYDLG_H__47BECE79_5BA4_4C22_B48B_C5A8120F8075__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TilesLibraryDlg.h : header file
//

#include "..\Common\Render.h"
#include "..\Common\Shader.h"


/////////////////////////////////////////////////////////////////////////////
// CTilesLibraryDlg dialog

class CTilesLibraryDlg : public CDialog
{
// Construction
public:
	CTilesLibraryDlg(CWnd* pParent = NULL);   // standard constructor
	~CTilesLibraryDlg();

	void Init();
	void DrawLibrary();
	void DrawFrame();

protected:
	KRender*	m_pRender;
	KShader*	m_pShader;
//	KSHADERINFO m_ShaderInfo;


// Dialog Data
	//{{AFX_DATA(CTilesLibraryDlg)
	enum { IDD = IDD_TILES_LIBRARY };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTilesLibraryDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CTilesLibraryDlg)
	afx_msg void OnDestroy();
	afx_msg void OnPaint();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TILESLIBRARYDLG_H__47BECE79_5BA4_4C22_B48B_C5A8120F8075__INCLUDED_)
