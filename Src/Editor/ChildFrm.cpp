// ChildFrm.cpp : implementation of the CChildFrame class
//

#include "stdafx.h"
#include "Editor.h"

#include "MainFrm.h"

#include "EditorDoc.h"
#include "EditorView.h"

#include "ChildFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


////////////////////////////////////////////////////////////////////////////////
// Global variables
////////////////////////////////////////////////////////////////////////////////
POINT CChildFrame::m_sPosition = { 200, 10 };


//==============================================================================
//
// 								CLASS CChildFrame
//
//==============================================================================


IMPLEMENT_DYNCREATE(CChildFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CChildFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CChildFrame)
	ON_COMMAND(ID_BUTTON_DRAW, OnButtonDraw)
	ON_COMMAND(ID_BUTTON_FILL, OnButtonFill)
	ON_COMMAND(ID_BUTTON_PICKER, OnButtonPicker)
	ON_COMMAND(ID_BUTTON_FLIPH, OnButtonFlipH)
	ON_COMMAND(ID_BUTTON_FLIPV, OnButtonFlipV)
	ON_COMMAND(ID_BUTTON_LOCK, OnButtonLock)
	ON_WM_CREATE()
	ON_COMMAND(ID_BUTTON_CLEAR, OnButtonClear)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


////////////////////////////////////////////////////////////////////////////////
// CChildFrame constructor & destructor
//																	CChildFrame
////////////////////////////////////////////////////////////////////////////////
CChildFrame::CChildFrame()
{
	m_sPosition.x += 20;
	m_sPosition.y += 20;
}

CChildFrame::~CChildFrame()
{
	m_sPosition.x -= 20;
	m_sPosition.y -= 20;
}

////////////////////////////////////////////////////////////////////////////////
// PreCreateWindow
//																	CChildFrame
////////////////////////////////////////////////////////////////////////////////
BOOL CChildFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.cx = (CELL_SIZE * LEVEL_WIDTH_MAX);
	cs.cy = (CELL_SIZE * LEVEL_HEIGHT_MAX);
	cs.x = m_sPosition.x;
	cs.y = m_sPosition.y;

	cs.style |= WS_DLGFRAME;
	cs.style &= ~WS_THICKFRAME;
	cs.style &= ~WS_MAXIMIZEBOX;

	cs.cx += (GetSystemMetrics(SM_CXSIZEFRAME)+1) * 2;
	cs.cy += (GetSystemMetrics(SM_CYSIZEFRAME)+1) * 2 + GetSystemMetrics(SM_CYCAPTION);

	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	return TRUE;
}

////////////////////////////////////////////////////////////////////////////////
// OnCreate
//																	CChildFrame
////////////////////////////////////////////////////////////////////////////////
int CChildFrame::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	return 0;
}


#ifdef _DEBUG

////////////////////////////////////////////////////////////////////////////////
// AssertValid
//																	CChildFrame
////////////////////////////////////////////////////////////////////////////////
void CChildFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

////////////////////////////////////////////////////////////////////////////////
// Dump
//																	CChildFrame
////////////////////////////////////////////////////////////////////////////////
void CChildFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


////////////////////////////////////////////////////////////////////////////////
// OnButtonDraw
//																	CChildFrame
////////////////////////////////////////////////////////////////////////////////
void CChildFrame::OnButtonDraw() 
{
	gTBotsEditorApp.SelectEditorTool(EditorToolDraw);
	gTBotsEditorApp.BackupTool();
}

////////////////////////////////////////////////////////////////////////////////
// OnButtonFill
//																	CChildFrame
////////////////////////////////////////////////////////////////////////////////
void CChildFrame::OnButtonFill() 
{
	gTBotsEditorApp.SelectEditorTool(EditorToolFill);
	gTBotsEditorApp.BackupTool();
}

////////////////////////////////////////////////////////////////////////////////
// OnButtonPicker
//																	CChildFrame
////////////////////////////////////////////////////////////////////////////////
void CChildFrame::OnButtonPicker() 
{
	gTBotsEditorApp.SelectEditorTool(EditorToolBlockPicker);
	gTBotsEditorApp.BackupTool();
}

////////////////////////////////////////////////////////////////////////////////
// OnButtonFlipH
//																	CChildFrame
////////////////////////////////////////////////////////////////////////////////
void CChildFrame::OnButtonFlipH() 
{
	if (gTBotsEditorApp.m_CellFlags & CELL_FLAG_FLIPH)
		gTBotsEditorApp.m_CellFlags &= ~CELL_FLAG_FLIPH;
	else
		gTBotsEditorApp.m_CellFlags |= CELL_FLAG_FLIPH;
}

////////////////////////////////////////////////////////////////////////////////
// OnButtonFlipV
//																	CChildFrame
////////////////////////////////////////////////////////////////////////////////
void CChildFrame::OnButtonFlipV() 
{
	if (gTBotsEditorApp.m_CellFlags & CELL_FLAG_FLIPV)
		gTBotsEditorApp.m_CellFlags &= ~CELL_FLAG_FLIPV;
	else
		gTBotsEditorApp.m_CellFlags |= CELL_FLAG_FLIPV;
}

////////////////////////////////////////////////////////////////////////////////
// OnButtonLock
//																	CChildFrame
////////////////////////////////////////////////////////////////////////////////
void CChildFrame::OnButtonLock() 
{
	if (gTBotsEditorApp.m_CellFlags & CELL_FLAG_LOCKED)
		gTBotsEditorApp.m_CellFlags &= ~CELL_FLAG_LOCKED;
	else
		gTBotsEditorApp.m_CellFlags |= CELL_FLAG_LOCKED;
}

////////////////////////////////////////////////////////////////////////////////
// OnButtonClear
//																	CChildFrame
////////////////////////////////////////////////////////////////////////////////
void CChildFrame::OnButtonClear() 
{
	if ( ((CEditorView *)GetActiveView()) )
		((CEditorView *)GetActiveView())->ClearLevel();
}
