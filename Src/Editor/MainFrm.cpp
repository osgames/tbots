// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "Editor.h"

#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


////////////////////////////////////////////////////////////////////////////////
// Global variables
////////////////////////////////////////////////////////////////////////////////
static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};


//==============================================================================
//
// 								CLASS CMainFrame
//
//==============================================================================


IMPLEMENT_DYNAMIC(CMainFrame, CMDIFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CMDIFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


////////////////////////////////////////////////////////////////////////////////
// CMainFrame constructor & destructor
//																	 CMainFrame
////////////////////////////////////////////////////////////////////////////////
CMainFrame::CMainFrame()
{
}

CMainFrame::~CMainFrame()
{
}

////////////////////////////////////////////////////////////////////////////////
// OnCreate
//																	 CMainFrame
////////////////////////////////////////////////////////////////////////////////
int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	m_wndToolBar.SetButtonStyle(6,TBBS_CHECKGROUP);
	m_wndToolBar.SetButtonStyle(7,TBBS_CHECKGROUP);
	m_wndToolBar.SetButtonStyle(8,TBBS_CHECKGROUP);

	m_wndToolBar.GetToolBarCtrl().CheckButton(ID_BUTTON_DRAW,true);

	m_wndToolBar.SetButtonStyle(9,TBBS_CHECKBOX);
	m_wndToolBar.SetButtonStyle(10,TBBS_CHECKBOX);
	m_wndToolBar.SetButtonStyle(11,TBBS_CHECKBOX);


	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);


	m_TilesLibraryDlg.Create(IDD_TILES_LIBRARY,this);
	m_TilesLibraryDlg.Init();

	
	// Move all the windows at their previous position (last session)
	POINT	*WindowPosition;
	UINT	val;

	if (AfxGetApp()->GetProfileBinary("Settings","LibPos",(unsigned char **)&WindowPosition,&val))
	{
		m_TilesLibraryDlg.SetWindowPos( NULL,
										WindowPosition->x, WindowPosition->y,
										0, 0,
										SWP_NOSIZE );

		free(WindowPosition);
	}
	
	m_TilesLibraryDlg.ShowWindow(SW_SHOW);

	return 0;
}

////////////////////////////////////////////////////////////////////////////////
// PreCreateWindow
//																	 CMainFrame
////////////////////////////////////////////////////////////////////////////////
BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CMDIFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CMDIFrameWnd::Dump(dc);
}

#endif //_DEBUG


////////////////////////////////////////////////////////////////////////////////
// OnClose
//																	 CMainFrame
////////////////////////////////////////////////////////////////////////////////
void CMainFrame::OnClose() 
{
	POINT	WindowPosition;
	RECT	Rect;

	// Save in the registry the Main Window position
//	GetWindowRect(&Rect);
//	WindowPosition.x = Rect.left;
//	WindowPosition.y = Rect.top;
//	AfxGetApp()->WriteProfileBinary("Settings","AppPos",(unsigned char *)&WindowPosition,sizeof(POINT));

	// Save in the registry the Tiles Library position
	m_TilesLibraryDlg.GetWindowRect(&Rect);
	WindowPosition.x = Rect.left;
	WindowPosition.y = Rect.top;
	AfxGetApp()->WriteProfileBinary("Settings","LibPos",(unsigned char *)&WindowPosition,sizeof(POINT));

	CFrameWnd::OnClose();
}
