//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Script.rc
//
#define IDI_ICONAPP                     101
#define IDD_DIALOG_BOTS_LIST            102
#define IDR_MAIN_MENU                   103
#define IDD_DIALOG_ABOUT                103
#define ID_FICHIER_RED                  104
#define ID_FICHIER_RED105               105
#define ID_FICHIER_EXIT                 106
#define ID_FILE_RESTARTBATTLE           107
#define ID_FILE_RESTARTGAME             108
#define ID_FILE_EXIT                    109
#define ID_OPTIONS_TOGGLESTEPBYSTEPMODE 110
#define ID_OPTIONS_GO                   111
#define ID_OPTIONS_TOGGLE2D             112
#define ID_HELP_ABOUT                   113
#define IDB_BITMAP1                     122
#define IDB_BITMAP2                     125
#define IDB_BITMAP_TITLE                125
#define IDC_LIST_BOTS                   1000
#define IDC_LABEL_BOTS_SELECTED         1004
#define IDC_BUTTON_SELECTALL            1005
#define IDC_LIST_MAZES                  1007
#define IDC_RICHEDIT21                  1010
#define IDC_CREDITS                     1010
#define IDC_ABOUT_TEXT                  1010

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        126
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1012
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
