# Microsoft Developer Studio Project File - Name="TBots" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=TBots - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "TBots.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "TBots.mak" CFG="TBots - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "TBots - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "TBots - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName "TBots"
# PROP Scc_LocalPath ".."
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "TBots - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /W3 /GX /O2 /I "..\\" /I "..\libpng" /I "..\zlib" /I "..\Common" /I "..\libpng\include" /I "..\zlib\include" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x40c /d "NDEBUG"
# ADD RSC /l 0x40c /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib opengl32.lib glu32.lib libz.lib libpng.lib /nologo /subsystem:windows /machine:I386 /libpath:"..\zlib\lib" /libpath:"..\libpng\lib"
# Begin Special Build Tool
SOURCE="$(InputPath)"
PostBuild_Cmds=xcopy release\tbots.exe ..\Bin /Y
# End Special Build Tool

!ELSEIF  "$(CFG)" == "TBots - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /W3 /Gm /GX /ZI /Od /I "..\libpng\include" /I "..\zlib\include" /I "..\Common" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /FR /YX /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x40c /d "_DEBUG"
# ADD RSC /l 0x40c /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib opengl32.lib glu32.lib libz.lib libpng.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept /libpath:"..\zlib\lib" /libpath:"..\libpng\lib"
# Begin Special Build Tool
SOURCE="$(InputPath)"
PostBuild_Cmds=xcopy debug\tbots.exe ..\Bin  /Y
# End Special Build Tool

!ENDIF 

# Begin Target

# Name "TBots - Win32 Release"
# Name "TBots - Win32 Debug"
# Begin Group "Common"

# PROP Default_Filter ""
# Begin Group "Items"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Common\Binoculars.cpp
# End Source File
# Begin Source File

SOURCE=..\Common\Binoculars.h
# End Source File
# Begin Source File

SOURCE=..\Common\Bonus.cpp
# End Source File
# Begin Source File

SOURCE=..\Common\Bonus.h
# End Source File
# Begin Source File

SOURCE=..\Common\Bot.cpp
# End Source File
# Begin Source File

SOURCE=..\Common\Bot.h
# End Source File
# Begin Source File

SOURCE=..\Common\Item.cpp
# End Source File
# Begin Source File

SOURCE=..\Common\Item.h
# End Source File
# Begin Source File

SOURCE=..\Common\Laser.cpp
# End Source File
# Begin Source File

SOURCE=..\Common\Laser.h
# End Source File
# Begin Source File

SOURCE=..\Common\Medikit.cpp
# End Source File
# Begin Source File

SOURCE=..\Common\Medikit.h
# End Source File
# Begin Source File

SOURCE=..\Common\Mine.cpp
# End Source File
# Begin Source File

SOURCE=..\Common\Mine.h
# End Source File
# Begin Source File

SOURCE=..\Common\Regenerator.cpp
# End Source File
# Begin Source File

SOURCE=..\Common\Regenerator.h
# End Source File
# End Group
# Begin Group "Render"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Common\Light.cpp
# End Source File
# Begin Source File

SOURCE=..\Common\Light.h
# End Source File
# Begin Source File

SOURCE=..\Common\Mesh.cpp
# End Source File
# Begin Source File

SOURCE=..\Common\Mesh.h
# End Source File
# Begin Source File

SOURCE=..\Common\Render.cpp
# End Source File
# Begin Source File

SOURCE=..\Common\Render.h
# End Source File
# Begin Source File

SOURCE=..\Common\RenderDefs.h
# End Source File
# Begin Source File

SOURCE=..\Common\Shader.cpp
# End Source File
# Begin Source File

SOURCE=..\Common\Shader.h
# End Source File
# End Group
# Begin Group "File"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Common\PngFile.cpp
# End Source File
# Begin Source File

SOURCE=..\Common\PngFile.h
# End Source File
# End Group
# Begin Source File

SOURCE=..\Common\BotDefs.h
# End Source File
# Begin Source File

SOURCE=..\Common\Camera.cpp
# End Source File
# Begin Source File

SOURCE=..\Common\Camera.h
# End Source File
# Begin Source File

SOURCE=..\Common\Level.cpp
# End Source File
# Begin Source File

SOURCE=..\Common\Level.h
# End Source File
# Begin Source File

SOURCE=..\Common\Matrix.h
# End Source File
# Begin Source File

SOURCE=..\Common\Sky.cpp
# End Source File
# Begin Source File

SOURCE=..\Common\Sky.h
# End Source File
# Begin Source File

SOURCE=..\Common\Timer.cpp
# End Source File
# Begin Source File

SOURCE=..\Common\Timer.h
# End Source File
# Begin Source File

SOURCE=..\Common\Types.h
# End Source File
# Begin Source File

SOURCE=..\Common\Vector.h
# End Source File
# End Group
# Begin Source File

SOURCE=..\..\changelog.txt
# End Source File
# Begin Source File

SOURCE=.\iconapp.ico
# End Source File
# Begin Source File

SOURCE=..\..\license.txt
# End Source File
# Begin Source File

SOURCE=.\resource.h
# End Source File
# Begin Source File

SOURCE=.\Script.rc
# End Source File
# Begin Source File

SOURCE=.\TBots.cpp
# End Source File
# Begin Source File

SOURCE=.\TBots.h
# End Source File
# Begin Source File

SOURCE=..\..\todo.txt
# End Source File
# Begin Source File

SOURCE=.\WinMain.cpp
# End Source File
# End Target
# End Project
