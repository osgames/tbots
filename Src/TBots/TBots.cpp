//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: TBots.cpp
//	Author			: - Aitonfrere	
//					  - Dino			dino@zythum-project.com
//					  - Mac Load
//
//	Date			: 16/07/2003
//	Modification	: 01/08/2004 by Mac Load
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------
#include "TBots.h"
#include "Bot.h"
#include "Bonus.h"
#include "Regenerator.h"
#include "Medikit.h"
#include "Binoculars.h"
#include "Mine.h"
#include "Laser.h"
#include "Timer.h"

#ifdef _WIN32
#include "Resource.h"
#include "Richedit.h"
#endif // _WIN32

#ifdef _LINUX
#include <unistd.h>
#include <dirent.h>
#include <dlfcn.h>
#include <X11/keysym.h>
#endif // _LINUX

KTBots*	g_pTBots = NULL;

#define	BOT_WAIT_TIME			100
#define	BOT_MOVE_TIME			100
#define	BOT_ATTACK_TIME			100
#define	BOT_ATTACKLASER_TIME	100
#define	BOT_DROP_TIME			100

// [24-07-2003 Aitonfrere] Battles Management
#define GET_DAMAGE_ATTACK(__Power)			( ( (rand() % 10) + 1 ) * (__Power) )

// Old version
/*
#define GET_DAMAGE_ATTACKLASER(__Distance)	( 200 / ( (__Distance) ? (__Distance) : 1) )
*/
// Modified by Mac Load
#define GET_DAMAGE_ATTACKLASER(__Distance)	( 150 - (__Distance) * 20 )
// End Modified by Mac Load

#define ITEM_RESPAWN_TURN		100	// Nombre de tour avant un respawn des objets

// Nombres maximum d'items
#define	RESPAWN_BONUS			10
#define	RESPAWN_REGENERATOR		3
#define	RESPAWN_BINOCULAR		3
#define	RESPAWN_MEDIKIT			8
#define	RESPAWN_LASER			10
#define	RESPAWN_MINE			10

//---------------------------------------------------------------------------------------------------------------------
KTBots::KTBots()
{
#ifdef _WIN32
	m_hWnd				= NULL;
#endif // _WIN32

#ifdef _LINUX
	m_hDC				= NULL;
#endif // _LINUX

	m_bQuit				= false;
	m_pRender			= NULL;
	m_pLevel			= NULL;
	m_pSky				= NULL;
	m_hInstance			= NULL;
	g_pTBots			= this;
	m_pCurrentBot		= NULL;
	m_ActionEndTime		= 0;
	m_pStatusMessage	= NULL;
	m_nRemainingActions	= 0;
	m_bStepByStep		= false;
	m_bStepGo			= false;
	m_pWinner			= NULL;
	m_RespawnCountDown	= 0;
	m_pMazeName			= NULL;
//	m_b3DMode			= false;
	m_b3DMode			= true;
	m_LastTime			= 0;
	m_nFrames			= 0;
	m_nFPS				= 0;

	srand( g_Timer.GetTime() );
}

//---------------------------------------------------------------------------------------------------------------------
KTBots::~KTBots()
{
	SafeFree( m_pMazeName );
}

//---------------------------------------------------------------------------------------------------------------------
KRESULT KTBots::Init( HINSTANCE hInstance )
{
	KRESULT	Result;
	bool	bFullScreen = true;

	m_hInstance = hInstance;

#ifdef _DEBUG
	bFullScreen = false;
#endif

	//
	// Creation de la fenetre
	//
#ifdef _WIN32	
	WNDCLASS	WndClass;

	memset( &WndClass, 0, sizeof( WndClass ) );
	WndClass.style			= CS_HREDRAW | CS_VREDRAW;
	WndClass.lpfnWndProc	= WindowProc;
	WndClass.hInstance		= m_hInstance;
	WndClass.hIcon			= LoadIcon( m_hInstance, MAKEINTRESOURCE(IDI_ICONAPP));
	WndClass.hCursor		= NULL;
	WndClass.hbrBackground	= (HBRUSH)GetStockObject(BLACK_BRUSH);
	WndClass.lpszMenuName	= MAKEINTRESOURCE(IDR_MAIN_MENU);
	WndClass.lpszClassName	= WINDOW_CLASSNAME;
	
	if ( !RegisterClass( &WndClass ) )
		return KR_WINDOWFAILED;

	DWORD	Style;
	RECT	WindowRect = { 0, 0, APP_WIDTH, APP_HEIGHT };

	if( bFullScreen )
		Style = WS_POPUPWINDOW ;
	else
		Style = WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX;

	AdjustWindowRect( &WindowRect, Style, TRUE );

	m_hWnd = CreateWindow(	WINDOW_CLASSNAME,
							APP_NAME,
							Style,
							bFullScreen ? WindowRect.left : CW_USEDEFAULT,
							bFullScreen ? WindowRect.top : CW_USEDEFAULT,
							WindowRect.right - WindowRect.left,
							WindowRect.bottom - WindowRect.top,
							NULL,
							NULL,
							hInstance,
							NULL );

	if ( !m_hWnd )
		return KR_WINDOWFAILED;

	UpdateWindow( m_hWnd );
	ShowWindow( m_hWnd, SW_NORMAL );

	if( !( m_hDC = GetDC( m_hWnd ) ) )
		return KR_WINDOWFAILED;
#endif // _WIN32

#ifdef _LINUX
	Display*		pDisplay;
	Window			Wnd;
	GLXContext		Context;
   	XSetWindowAttributes 	swattr;
	
	pDisplay = XOpenDisplay( NULL );
	if( !pDisplay )
		return false;
	
	int pAttribList[] = {   GLX_USE_GL,
				GLX_RGBA,
				GLX_DOUBLEBUFFER,
				GLX_DEPTH_SIZE, 16,
				GLX_RED_SIZE, 5,
				GLX_GREEN_SIZE, 5,
				GLX_BLUE_SIZE, 5,
				None };
				
	XVisualInfo*	pVisualInfo;

	pVisualInfo = glXChooseVisual( pDisplay, DefaultScreen( pDisplay ), pAttribList );
	if( !pVisualInfo )
        	return false;
		
	swattr.colormap = XCreateColormap( pDisplay, RootWindow( pDisplay, pVisualInfo->screen ), pVisualInfo->visual, AllocNone );
	swattr.background_pixel = BlackPixel( pDisplay ,pVisualInfo->screen );
	swattr.border_pixel = BlackPixel( pDisplay ,pVisualInfo->screen );
	
	Wnd = XCreateWindow(	pDisplay,
				DefaultRootWindow( pDisplay ),
				30,30,
				APP_WIDTH, APP_HEIGHT,
				0,
				pVisualInfo->depth,
				CopyFromParent,
				pVisualInfo->visual,
				CWBackPixel | CWBorderPixel | CWColormap,
				&swattr );

	
	
	XMapWindow( pDisplay, Wnd );
	XFlush( pDisplay );

	Context = glXCreateContext( pDisplay, pVisualInfo, NULL, True );
	if( !Context )
		return false;
	
	glXMakeCurrent( pDisplay, Wnd, Context );

	m_hWnd = Wnd;
	m_hDC = pDisplay;
	
#endif // _LINUX

	//
	//	Init Render
	//
	int rbpp = IDNO;//MessageBox( m_hWnd, "16 bits -> Yes\n32 bits -> No", APP_NAME, MB_YESNO );
	m_pRender = new KRender();
	m_pRender->SetpCamera( &m_Camera );

//	m_Camera.RotateX( PI + PI / 8.0f );
//	m_Camera.Translate( -240.0f, -158.0f, 240.0f );
//	m_Camera.RotateX( PI );

//	Old Version
/*
	m_Camera.Translate( -240.0f, -60.0f, -300.0f );
*/
// Modified By Mac Load
	m_Camera.Translate( -170.0f, -60.0f, -400.0f );
// End Modified By Mac Load

	m_Camera.RotateX( -PI / 8.0f );

	Result = m_pRender->Init( m_hWnd, m_hDC, APP_WIDTH, APP_HEIGHT, (rbpp == IDYES) ? 16 : 32, bFullScreen );
	if( KR_FAILED( Result ) )
	{
		char error[256];
		sprintf( error, "Cannot init render, error : %i", Result );
		KERROR( error );
		return KR_ERROR;
	}

	// Charge le jeu
	Result = LoadGame();
	if( KR_FAILED( Result ) )
		return Result;

	return KR_OK;
}

//---------------------------------------------------------------------------------------------------------------------
KRESULT KTBots::Run()
{
#ifdef _WIN32
	MSG		msg;
	KRESULT	Result;

	while( !m_bQuit )
	{
		if ( PeekMessage( &msg, NULL, 0, 0, PM_REMOVE ) )
		{
			DispatchMessage( &msg );
			TranslateMessage( &msg );

			switch( msg.message )
			{
			case WM_QUIT:
				m_bQuit = true;
				break;
			case WM_RESTARTGAME:
				Result = RestartGame();
				if( KR_FAILED( Result ) )
					return Result;
				break;
			case WM_RESTARTBATTLE:
				Result = RestartBattle();
				if( KR_FAILED( Result ) )
					return Result;
				break;
			case WM_STEPBYSTEP:
				m_bStepByStep = m_bStepByStep ? false : true;
				break;
			case WM_STEPGO:
				m_bStepGo = true;
				break;
			case WM_TOGGLE3DMODE:
				m_b3DMode = m_b3DMode ^ 1;
				break;
			case WM_ABOUT:
//				HMODULE hModuleRichEdit = LoadLibrary( "RICHED20.DLL" );
				DialogBox( m_hInstance, MAKEINTRESOURCE( IDD_DIALOG_ABOUT ), m_hWnd, DialogAboutProc ) ;
				ShowWindow(m_hWnd,SW_SHOWNORMAL) ;
				UpdateWindow(m_hWnd) ;
//				FreeLibrary( hModuleRichEdit );
				break;
			}
		}
		else
		{
			if( IsIconic( m_hWnd ) )
				Sleep( 10 );
//			else
//				Sleep( 1 );

			MoveFrame();
			DrawFrame();
		}
	}
#endif // _WIN32

#ifdef _LINUX
	XSelectInput( m_hDC, m_hWnd, KeyPressMask );
	while( !m_bQuit )
	{
		if( XPending( m_hDC ) )
		{
			printf("Event\n");
			XEvent	Event;
			KRESULT	Result;
			XNextEvent( m_hDC, &Event );
			
			switch( Event.type )
			{
			case DestroyNotify:
				m_bQuit = true;
				printf("Quit\n");
				break;
			case KeyPress:
				switch( XLookupKeysym( &Event.xkey, 0 ) )
				{
				case XK_Escape:
					m_bQuit = true;
					break;
				case XK_space:
					Result = RestartBattle();
					if( KR_FAILED( Result ) )
						return Result;
					break;
				case XK_Return:
					Result = RestartGame();
					if( KR_FAILED( Result ) )
						return Result;
					break;
				case XK_Tab:
					m_bStepGo = true;
					break;
				case XK_BackSpace:
					m_bStepByStep = m_bStepByStep ? false : true;
					break;
				case XK_F2:
					m_b3DMode = m_b3DMode ^ 1;
					break;
				default:
					break;
				}
			default:
				break;
			}
		}
		MoveFrame();
		DrawFrame();
//		usleep( 1000 );
	}
#endif // _LINUX

	return KR_OK;
}

//---------------------------------------------------------------------------------------------------------------------
KRESULT	KTBots::End()
{
#ifdef	_WIN32
	if( m_hDC )
	{
		ReleaseDC( m_hWnd, m_hDC );
		m_hDC = NULL;
	}
#endif // _WIN32

	// Décharge le jeu
	UnloadGame();

	// Flush le Render
	if( m_pRender )
	{
		if( KR_FAILED( m_pRender->End() ) )
			return KR_ERROR;
		Deletep( m_pRender );
	}

	return KR_OK;
}

//---------------------------------------------------------------------------------------------------------------------
KRESULT KTBots::LoadGame()
{
	//
	//	Chargement des Bots
	//
#ifdef _WIN32
	KRESULT Result = LoadDLLBots();
	if( KR_FAILED( Result ) )
		return Result;
#endif // _WIN32

#ifdef _LINUX
	KRESULT Result = LoadSOBots();
	if( KR_FAILED( Result ) )
		return Result;
#endif // _LINUX

	//
	//	Init Item
	//
	if( !KBinoculars::Load( m_pRender ) )	return KR_ERROR;
	if( !KBonus::Load( m_pRender ) )		return KR_ERROR;
	if( !KBot::Load( m_pRender ) )			return KR_ERROR;
	if( !KLaser::Load( m_pRender ) )		return KR_ERROR;
	if( !KMedikit::Load( m_pRender ) )		return KR_ERROR;
	if( !KMine::Load( m_pRender ) )			return KR_ERROR;
	if( !KRegenerator::Load( m_pRender ) )	return KR_ERROR;

	//
	//	Init Level
	//
	if( !m_pMazeName )
		return KR_ERROR;

	m_pLevel = new KLevel( m_pRender );
	if( KR_FAILED( m_pLevel->Load( m_pMazeName ) ) )
		return KR_ERROR;

	//
	//	Init Sky
	//
//	m_pSky = new KSky( m_pRender, 5000 );
	
	// Commence la partie
	StartGame();

	return KR_OK;
}

//---------------------------------------------------------------------------------------------------------------------
KRESULT KTBots::UnloadGame()
{
	// EndGame
	EndGame();

	// Detruit le message
	if( m_pStatusMessage )
		Freep( m_pStatusMessage );

	// Flush les DLL Bots infos
	KDllBotInfo*	pDllBot;

	while( (pDllBot = m_DllBotInfos.GetFirst()) )
	{
		m_DllBotInfos.Remove( pDllBot );
#ifdef _WIN32
		if( pDllBot->m_hModule )
			FreeLibrary( pDllBot->m_hModule );
#endif // _WIN32
#ifdef _LINUX
		if( pDllBot->m_hModule )
			dlclose( pDllBot->m_hModule );
#endif // _LINUX
		SafeDelete( pDllBot );
	}

	// Flush le Level
	if( m_pLevel )
	{
		if( KR_FAILED( m_pLevel->Unload() ) )
			return KR_ERROR;
		Deletep( m_pLevel );
	}

	// Flush les items
	KBinoculars::Unload();
	KBonus::Unload();
	KBot::Unload();
	KLaser::Unload();
	KMedikit::Unload();
	KMine::Unload();
	KRegenerator::Unload();

	// Flush Sky
	SafeDelete( m_pSky );

	return KR_OK;
}

#ifdef _WIN32
//---------------------------------------------------------------------------------------------------------------------
LRESULT CALLBACK KTBots::WindowProc( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
	static bool sChecked = false ;

	switch( uMsg )
	{
	case WM_COMMAND:
	{
		int iValue = LOWORD(wParam) ;
		if(iValue == ID_FILE_EXIT)
			PostQuitMessage( 0 );
		if(iValue == ID_FILE_RESTARTGAME)
			PostMessage( hWnd, WM_RESTARTGAME, 0, 0 );
		if(iValue == ID_FILE_RESTARTBATTLE)
			PostMessage( hWnd, WM_RESTARTBATTLE, 0, 0 );
		if(iValue == ID_OPTIONS_TOGGLE2D)
			PostMessage( hWnd, WM_TOGGLE3DMODE, 0, 0 );
		if(iValue == ID_OPTIONS_GO)
			PostMessage( hWnd, WM_STEPGO, 0, 0 );
		if(iValue == ID_OPTIONS_TOGGLESTEPBYSTEPMODE)
		{
			PostMessage( hWnd, WM_STEPBYSTEP, 0, 0 );
			if(sChecked)
				CheckMenuItem(GetMenu(hWnd),ID_OPTIONS_TOGGLESTEPBYSTEPMODE,MF_UNCHECKED) ;
			else
				CheckMenuItem(GetMenu(hWnd),ID_OPTIONS_TOGGLESTEPBYSTEPMODE,MF_CHECKED) ;
			sChecked = !sChecked ;
		}
		if(iValue == ID_HELP_ABOUT)
			PostMessage( hWnd, WM_ABOUT, 0, 0 );
	}	
	break ;

	case WM_CREATE:
		break;
	
	case WM_CLOSE:
		PostQuitMessage( 0 );
		break;

	case WM_MOUSEMOVE:
		//SetCursor( NULL );
		break;

	case WM_KEYDOWN:
		switch( wParam )
		{
		case VK_ESCAPE:
			PostQuitMessage( 0 );
			break;
		case VK_SPACE:
			PostMessage( hWnd, WM_RESTARTBATTLE, 0, 0 );
			break;
		case VK_RETURN:
			PostMessage( hWnd, WM_RESTARTGAME, 0, 0 );
			break;
		case VK_TAB:
			PostMessage( hWnd, WM_STEPGO, 0, 0 );
			break;
		case VK_BACK:
			{
				PostMessage( hWnd, WM_STEPBYSTEP, 0, 0 );
				if(sChecked)
					CheckMenuItem(GetMenu(hWnd),ID_OPTIONS_TOGGLESTEPBYSTEPMODE,MF_UNCHECKED) ;
				else
					CheckMenuItem(GetMenu(hWnd),ID_OPTIONS_TOGGLESTEPBYSTEPMODE,MF_CHECKED) ;
				sChecked = !sChecked ;
			}
			break;
		case VK_F2:
			PostMessage( hWnd, WM_TOGGLE3DMODE, 0, 0 );
			break;
		}
		break;
	}

	return DefWindowProc( hWnd, uMsg, wParam, lParam);
}

#endif // _WIN32

//---------------------------------------------------------------------------------------------------------------------
#ifdef _WIN32
#define KEYPRESSED(_key)		( GetAsyncKeyState(_key) & 0x8000 )
#endif // _WIN32

#ifdef _LINUX
#define KEYPRESSED(_key)		0
#endif // _LINUX
//---------------------------------------------------------------------------------------------------------------------
KRESULT KTBots::MoveFrame()
{
	float	Speed = 1.0f;
	float	RSpeed = 0.1f;

	if( KEYPRESSED( VK_UP ) )
		m_Camera.Translate( 0.0f, -Speed, 0.0f );
	if( KEYPRESSED( VK_DOWN ) )
		m_Camera.Translate( 0.0f, Speed, 0.0f );
	if( KEYPRESSED( VK_LEFT ) )
		m_Camera.Translate( Speed, 0.0f, 0.0f );
	if( KEYPRESSED( VK_RIGHT ) )
		m_Camera.Translate( -Speed, 0.0f, 0.0f );
	if( KEYPRESSED( VK_RSHIFT ) )
		m_Camera.Translate( 0.0f, 0.0f, Speed );
	if( KEYPRESSED( VK_RCONTROL ) )
		m_Camera.Translate( 0.0f, 0.0f, -Speed );
	
	if( KEYPRESSED( VK_DELETE ) )
		m_Camera.RotateZ( -RSpeed );
	if( KEYPRESSED( VK_NEXT ) )
		m_Camera.RotateZ( RSpeed );

	if( KEYPRESSED( VK_HOME ) )
		m_Camera.RotateX( -RSpeed );
	if( KEYPRESSED( VK_END ) )
		m_Camera.RotateX( RSpeed );

	ManageGame();

	return KR_OK;
}

//---------------------------------------------------------------------------------------------------------------------
//
//	 Display
//
//---------------------------------------------------------------------------------------------------------------------
KRESULT KTBots::DrawFrame()
{
	if( m_pRender )
	{
		m_pRender->BeginScene();

		if( m_b3DMode )
		{
			// Affichage 3D
			if( m_pSky )
				m_pSky->Draw();

			if( m_pLevel )
				m_pLevel->Draw();
		}
		else
		{
			// Affichage 2D
			if( m_pLevel )
			{
				KPt	LevelPos( SCREEN_OFFSETX, 0 );
				m_pLevel->Draw2D( LevelPos );
			}
		}

		// Vainqueur
		if( m_pWinner )
		{
			char	pText[256];
			sprintf( pText, "%s wins the battle!", m_pWinner->GetpName() );

			float	f		= fabsf( sinf( (float)g_Timer.GetTime() / 200.0f) );
			s32		Size	= s32(16.0f + 16.0f * f);
			s32		sx		= (s32)((float)strlen( pText ) * (float)Size);
			m_pRender->DrawText( APP_WIDTH / 2 - sx / 2 , APP_HEIGHT / 2, pText, Size );
		}
        if( GetnAliveBots() == 0 )
        {
            char	pText[256];
            sprintf(pText, "DRAW");

            float	f		= fabsf( sinf( (float)g_Timer.GetTime() / 200.0f) );
            s32		Size	= s32(16.0f + 16.0f * f);
            s32		sx		= (s32)((float)strlen( pText ) * (float)Size);
            m_pRender->DrawText( APP_WIDTH / 2 - sx / 2 , APP_HEIGHT / 2, pText, Size );
        }

		DrawInterface();

		//
		//	Frame / Second
		//
		KTIME	Interval = 200;
		KTIME	Time = g_Timer.GetTime();

		if( (Time - m_LastTime) > Interval )
		{
			m_nFPS = (s32)(((float)m_nFrames / (float)(Time - m_LastTime)) * 1000.0f);
			m_LastTime	= Time;
			m_nFrames	= 0;
		}

		#if _DEBUG
			char	pText[256];
			sprintf( pText, "%i Polys", m_pRender->GetnPolys() );
			m_pRender->DrawText( APP_WIDTH - 140 , 20, pText, 12 );
			sprintf( pText, "%i FPS", m_nFPS );
			m_pRender->DrawText( APP_WIDTH - 80 , 32, pText, 12 );
		#endif

		m_pRender->EndScene();
		m_pRender->Flip();

		m_nFrames ++;
	}

	return KR_OK;
}

//---------------------------------------------------------------------------------------------------------------------
KRESULT KTBots::DrawInterface()
{
	// Affiche la liste des bots et leur status
	KDllBotInfo*					pDllBotInfo;
	s32								i = 0;
	s32								iHeight = GetSystemMetrics( SM_CYMENUSIZE );

//	m_pRender->DrawQuad( 0, 0, 240, APP_HEIGHT, 0, KRGB( 32, 32, 32 ) );

	for( pDllBotInfo = m_DllBotInfos.GetFirst(); pDllBotInfo; pDllBotInfo = m_DllBotInfos.GetNext( pDllBotInfo ) )
	{
		if( pDllBotInfo->m_bFight )
		{
			KASSERT( pDllBotInfo->m_pBot );

			// Image du robot
			KPt	BotPos( 8, iHeight + i * 64 );
			KPt	BotSize( 16, 16 );
			pDllBotInfo->m_pBot->Display2D( BotPos, BotSize );
			
			// Nom du robot
			KCOLOR	Color = KRGBA( 96, 96, 96, 128 );
			
			if( pDllBotInfo == m_pCurrentBot )
				Color = KRGBA( 160, 120, 120, 128 );

			m_pRender->DrawQuad( 32, iHeight + i * 64, 192, 16, 0, Color );

			if( pDllBotInfo->m_pBot->IsCheater() )
			{
				if( ( g_Timer.GetTime() / 500 ) % 2 )
					m_pRender->DrawText( 32, iHeight + i * 64, pDllBotInfo->m_pBot->GetpName() );
				else
					m_pRender->DrawText( 32, iHeight + i * 64, "CHEATER !!!" );
			}
			else
				m_pRender->DrawText( 32, iHeight + i * 64, pDllBotInfo->m_pBot->GetpName() );

			// Vie du robot
			s32   LifeMax = 128 * pDllBotInfo->m_pBot->GetMaxLife() / (s32)BOT_MAX_LIFE ;
			float	Life = pDllBotInfo->m_pBot->GetLife() / (float)BOT_MAX_LIFE;
			m_pRender->DrawQuad( 32, iHeight + 18 + i * 64, LifeMax, 4, 0, KRGB( 128, 0, 0 ) );
			m_pRender->DrawQuad( 32, iHeight + 18 + i * 64, s32(128 * Life), 4, 0, KRGB( 255, 0, 0 ) );

			// Inventaire
			Color = KRGBA( 96, 96, 96, 128 );
			m_pRender->DrawQuad( 48, iHeight + 24 + i * 64, 16, 16, 0, Color );
			m_pRender->DrawQuad( 66, iHeight + 24 + i * 64, 16, 16, 0, Color );
			m_pRender->DrawQuad( 84, iHeight + 24 + i * 64, 16, 16, 0, Color );
			m_pRender->DrawQuad( 102, iHeight + 24 + i * 64, 16, 16, 0, Color );
			m_pRender->DrawQuad( 120, iHeight + 24 + i * 64, 16, 16, 0, Color );

			u32	j = 0;
			for( KItem* pChild = pDllBotInfo->m_pBot->GetChilds().GetFirst(); pChild; pChild = pDllBotInfo->m_pBot->GetChilds().GetNext( pChild ) )
			{
				KPt	ChildPos( j + 48, i * 64 + iHeight + 24 );
				KPt	ChildSize( CELL_SIZE, CELL_SIZE );
				pChild->Display2D( ChildPos, ChildSize );
				j += 18;
			}

			// Caract�istiques
			char	pText[256];
			
			sprintf( pText, "P:%i S:%i V:%i", pDllBotInfo->m_pBot->GetPower(), GETNUMACTIONS(pDllBotInfo->m_pBot->GetSpeed()), GETSIGHT(pDllBotInfo->m_pBot->GetSight()) );
			m_pRender->DrawText( 16, iHeight + 42 + i * 64, pText, 12 );

			i ++;
		}
	}

	#ifdef _LINUX
		s32	Height = 112;
		m_pRender->DrawQuad( 0, APP_HEIGHT - Height, APP_WIDTH, Height, 0, KRGBA( 0, 0, 255, 64 ) );

		m_pRender->DrawText( 16, APP_HEIGHT - 112, (char*)(m_bStepByStep ? "Mode : Step by	step" : "Mode : Normal") );
		m_pRender->DrawText( 16, APP_HEIGHT - 96, "BACKSPACE : Toggle Step by step Mode" );
		m_pRender->DrawText( 16, APP_HEIGHT - 80, "TAB : Go! (Step by step mode only)" );
		m_pRender->DrawText( 16, APP_HEIGHT - 64, "SPACE : Restart Battle     RETURN : Restart Game" );
		m_pRender->DrawText( 16, APP_HEIGHT - 48, "F2 : Toggle 2D/3D Mode" );
	#endif
	#ifdef _WIN32
		s32	Height = 25;
		m_pRender->DrawQuad( 0, APP_HEIGHT - 38, APP_WIDTH, Height, 0, KRGBA( 0, 0, 255, 64 ) );
	#endif

	// Status Message
	if( m_pStatusMessage )
		m_pRender->DrawText( 16, APP_HEIGHT - 36, m_pStatusMessage );

	return KR_OK;
}

//---------------------------------------------------------------------------------------------------------------------
/*
KRESULT KTBots::DrawDebugBots()
{
	if( !m_b3DMode )
	{
		if( m_pCurrentBot && m_pCurrentBot->m_pDebugDisplayProc )
			((void(*)(HINSTANCE,HWND))(m_pCurrentBot)->m_pDebugDisplayProc)(m_hInstance,m_hWnd);
	}

	return KR_OK;
}
*/
//---------------------------------------------------------------------------------------------------------------------
//
//	 Game
//
//---------------------------------------------------------------------------------------------------------------------
void KTBots::StartGame()
{
	m_pWinner			= NULL;
	m_pCurrentBot		= NULL;
	m_nRemainingActions	= 0;

	// Change l'ordre de la liste aleatoirement
	s32	k = m_DllBotInfos.GetSize();
	for( u32 j = 0; j < m_DllBotInfos.GetSize(); j ++ )
	{
		KDllBotInfo* pDllBotInfo;
		
		// Prend un ��ent au hasard dans la liste
		pDllBotInfo = m_DllBotInfos.GetPosition( rand() % k );

		// On l'enleve de la liste
		m_DllBotInfos.Remove( pDllBotInfo );

		// Et on le remet �la fin
		m_DllBotInfos.Add( pDllBotInfo );

		k --;
	}

	// Initialise les DLL de Bots
	for( KDllBotInfo* pDllBotInfo = m_DllBotInfos.GetFirst(); pDllBotInfo; pDllBotInfo = m_DllBotInfos.GetNext( pDllBotInfo ) )
	{
		if( pDllBotInfo->m_bFight )
		{
			#if _DEBUG
				#if _WIN32
					if( pDllBotInfo->m_pDebugDisplayProc )
						((void(*)(HINSTANCE,HWND))(pDllBotInfo)->m_pDebugDisplayProc)(m_hInstance,m_hWnd);
				#endif _WIN32
			#endif _DEBUG

			// Appele la fonction StartGame()
			if( pDllBotInfo->m_pStartGameProc )
				 pDllBotInfo->m_pContext = ((void*(*)( s32, s32 ))pDllBotInfo->m_pStartGameProc)( m_pLevel->GetWidth(), m_pLevel->GetHeight() );
		}
	}

	// Place les bots dans la map
	SpawnBots();

	// Place les items
	SpawnItems();

	m_RespawnCountDown = ITEM_RESPAWN_TURN;
}

//---------------------------------------------------------------------------------------------------------------------
void KTBots::SpawnItems()
{
	// Place les bonus dans la map
	u32	nBonus = 0;
	nBonus += GetnItems( KIT_BONUSVISION );
	nBonus += GetnItems( KIT_BONUSPOWER );
	nBonus += GetnItems( KIT_BONUSSPEED );
	nBonus += GetnItems( KIT_BONUSJOKER );

	u32	nSpawn;

	if(nBonus < RESPAWN_BONUS)
	{
		nSpawn = RESPAWN_BONUS - nBonus;
		if( nSpawn )
			SpawnBonus( rand() % nSpawn );
	}

	// Place les regenerators dans la map
	nSpawn = RESPAWN_REGENERATOR - GetnItems( KIT_REGENERATOR );
	if( nSpawn )
		SpawnRegenerator( rand() % nSpawn );

	// Place les medikit dans la map
	nSpawn = RESPAWN_MEDIKIT - GetnItems( KIT_MEDIKIT );
	if( nSpawn )
		SpawnMedikit( rand() % nSpawn );

	// Place les jumelles dans la map
	nSpawn = RESPAWN_BINOCULAR - GetnItems( KIT_BINOCULARS );
	if( nSpawn )
		SpawnBinoculars( rand() % nSpawn );

	// Place les mines dans la map

	if(GetnItems( KIT_MINE ) < RESPAWN_MINE)
	{
		nSpawn = RESPAWN_MINE - GetnItems( KIT_MINE );
		if( nSpawn )
			SpawnMine( rand() % nSpawn );
	}
	// Place les lasers dans la map
	nSpawn = RESPAWN_LASER - GetnItems( KIT_LASER );
	if( nSpawn )
		SpawnLaser( rand() % nSpawn );
}

//---------------------------------------------------------------------------------------------------------------------
s32 KTBots::GetnItems( KITEMTYPE Type )
{
	return m_pLevel->GetnItems( Type );		
}

//---------------------------------------------------------------------------------------------------------------------
void KTBots::EndGame()
{
	for( KDllBotInfo* pDllBotInfo = m_DllBotInfos.GetFirst(); pDllBotInfo; pDllBotInfo = m_DllBotInfos.GetNext( pDllBotInfo ) )
	{
		if( pDllBotInfo->m_bFight )
		{
			// Appele la fonction EndGame()
			if( pDllBotInfo->m_pEndGameProc )
				((void(*)(void*))pDllBotInfo->m_pEndGameProc)( pDllBotInfo->m_pContext );

			// D�ruit le lien avec le bot
			pDllBotInfo->m_pBot = NULL;
		}
	}

	// D�ruit tous les items
	if( m_pLevel )
		m_pLevel->FlushItems();
}

//---------------------------------------------------------------------------------------------------------------------
KRESULT KTBots::RestartGame()
{
	KRESULT	Result;

	// Relance le jeu
	m_pCurrentBot = NULL;

	Result = UnloadGame();
	if( KR_FAILED( Result ) )
		return Result;

	Result = LoadGame();
	if( KR_FAILED( Result ) )
		return Result;

	return KR_OK;
}

//---------------------------------------------------------------------------------------------------------------------
KRESULT KTBots::RestartBattle()
{
	// Relance un nouveau round
	EndGame();
	StartGame();

	return KR_OK;
}

//---------------------------------------------------------------------------------------------------------------------
KRESULT KTBots::ManageGame()
{
	if( GetnAliveBots() == 1 )
	{
		// Nous avons un heureux vainqueur!
		char							pMess[1024];

		// On recherche son identit�		
		for( KDllBotInfo* pDllBotInfo = m_DllBotInfos.GetFirst(); pDllBotInfo; pDllBotInfo = m_DllBotInfos.GetNext( pDllBotInfo ) )
		{
			if( pDllBotInfo->m_pBot && pDllBotInfo->m_pBot->GetLife() )
			{
				// Reset son action au cas ou...
				pDllBotInfo->m_pBot->SetAction( KBA_NOTHING );

				sprintf( pMess, "%s wins!!!", pDllBotInfo->m_pBot->GetpName() );
				SetpMessage( pMess );

				// note le vainqueur
				m_pWinner = pDllBotInfo->m_pBot;
				break;
			}
		}

		return KR_OK;
	}
    if( GetnAliveBots() == 0 )
    {
        return KR_OK ;
    }

	if( ( !m_bStepByStep && ( g_Timer.GetTime() > m_ActionEndTime ) ) || ( m_bStepByStep && m_bStepGo ) )
	{
		m_bStepGo = false;

		// Fin de l'action, on passe au Bot suivant
		NextTurn();
	}

	return KR_OK;
}

//---------------------------------------------------------------------------------------------------------------------
KRESULT KTBots::SpawnBots()
{
	bool							bSpawnFound;
	
	// Cherche un point de spawn aleatoirement
	for( KDllBotInfo* pDllBotInfo = m_DllBotInfos.GetFirst(); pDllBotInfo; pDllBotInfo = m_DllBotInfos.GetNext( pDllBotInfo ) )
	{
		if( pDllBotInfo->m_bFight )
		{
			KASSERT( !pDllBotInfo->m_pBot );

			bSpawnFound = false;
			while( !bSpawnFound )
			{
				s32	x = rand() % m_pLevel->GetWidth();
				s32	y = rand() % m_pLevel->GetHeight();

				if( m_pLevel->CanSpawn( x, y ) )
				{
					bSpawnFound = true;

					// Cr�tion du Bot
					KBotInfo*	pBotInfo;
					pBotInfo = ((KBotInfo*(*)(void*))pDllBotInfo->m_pInfosProc)( pDllBotInfo->m_pContext );

					KBot*	pBot = new KBot( m_pRender, m_pLevel );
					KPt		BotPos( x, y );
					pBot->SetpName( pBotInfo->GetpBotName() );
					pBot->SetNumber( pDllBotInfo->m_BotNumber );
					pBot->SetPos( BotPos );					
					
					if( !KBotInfo::CheckPoints(	pBotInfo->GetVitality(),
												pBotInfo->GetPower(),
												pBotInfo->GetSpeed(),
												pBotInfo->GetVision() ) )
					{
						pBot->SetbCheater( true );
					}
					else
					{
						pBot->SetVitality( pBotInfo->GetVitality() );
						pBot->SetLife( GETLIFE( pBotInfo->GetVitality() ) );
						pBot->SetPower( pBotInfo->GetPower() );
						pBot->SetSpeed( pBotInfo->GetSpeed() );
						pBot->SetVision( pBotInfo->GetVision() );
					}

					pDllBotInfo->m_pBot = pBot;
					m_pLevel->AddpItem( pBot );
				}
			}
		}
	}

	return KR_OK;
}

//---------------------------------------------------------------------------------------------------------------------
KRESULT KTBots::SpawnBonus( u32 nBonus )
{
	bool							bSpawnFound;
	
	// Cherche un point de spawn aleatoirement
	for( u32 i = 0; i < nBonus; i ++ )
	{
		bSpawnFound = false;
		while( !bSpawnFound )
		{
			s32	x = rand() % m_pLevel->GetWidth();
			s32	y = rand() % m_pLevel->GetHeight();

			if( m_pLevel->CanSpawn( x, y ) )
			{
				bSpawnFound = true;

				// Cr�tion du bonus
				KBonus*	pBonus = new KBonus( m_pRender, m_pLevel );
				KPt		BonusPos( x, y );
				pBonus->SetType( (KITEMTYPE)(KIT_BONUSVISION + rand() % 4) );
				pBonus->SetPos( BonusPos );

				m_pLevel->AddpItem( pBonus );
			}
		}
	}

	return KR_OK;
}

//---------------------------------------------------------------------------------------------------------------------
KRESULT KTBots::SpawnRegenerator( u32 nRegenerator )
{
	bool							bSpawnFound;
	
	// Cherche un point de spawn aleatoirement
	for( u32 i = 0; i < nRegenerator; i ++ )
	{
		bSpawnFound = false;
		while( !bSpawnFound )
		{
			s32	x = rand() % m_pLevel->GetWidth();
			s32	y = rand() % m_pLevel->GetHeight();

			if( m_pLevel->CanSpawn( x, y ) )
			{
				bSpawnFound = true;

				// Cr�tion du Regenerator
				KRegenerator*	pRegenerator = new KRegenerator( m_pRender, m_pLevel );
				KPt				RegenPos( x, y );
				pRegenerator->SetPos( RegenPos );

				m_pLevel->AddpItem( pRegenerator );
			}
		}
	}

	return KR_OK;
}

//---------------------------------------------------------------------------------------------------------------------
KRESULT KTBots::SpawnMedikit( u32 nMedikit )
{
	bool							bSpawnFound;
	
	// Cherche un point de spawn aleatoirement
	for( u32 i = 0; i < nMedikit; i ++ )
	{
		bSpawnFound = false;
		while( !bSpawnFound )
		{
			s32	x = rand() % m_pLevel->GetWidth();
			s32	y = rand() % m_pLevel->GetHeight();

			if( m_pLevel->CanSpawn( x, y ) )
			{
				bSpawnFound = true;

				// Cr�tion du Medikit
				KMedikit*	pMedikit = new KMedikit( m_pRender, m_pLevel );
				KPt			MedikitPos( x, y );
				pMedikit->SetPos( MedikitPos );

				m_pLevel->AddpItem( pMedikit );
			}
		}
	}

	return KR_OK;
}

//---------------------------------------------------------------------------------------------------------------------
KRESULT KTBots::SpawnBinoculars( u32 nBinoculars )
{
	bool							bSpawnFound;
	
	// Cherche un point de spawn aleatoirement
	for( u32 i = 0; i < nBinoculars; i ++ )
	{
		bSpawnFound = false;
		while( !bSpawnFound )
		{
			s32	x = rand() % m_pLevel->GetWidth();
			s32	y = rand() % m_pLevel->GetHeight();

			if( m_pLevel->CanSpawn( x, y ) )
			{
				bSpawnFound = true;

				// Cr�tion du Binoculars
				KBinoculars*	pBinoculars = new KBinoculars( m_pRender, m_pLevel );
				KPt				BinoPos( x, y );
				pBinoculars->SetPos( BinoPos );

				m_pLevel->AddpItem( pBinoculars );
			}
		}
	}

	return KR_OK;
}

//---------------------------------------------------------------------------------------------------------------------
KRESULT KTBots::SpawnMine( u32 nMine )
{
	bool							bSpawnFound;

	// Cherche un point de spawn aleatoirement
	for( u32 i = 0; i < nMine; i ++ )
	{
		bSpawnFound = false;
		while( !bSpawnFound )
		{
			s32	x = rand() % m_pLevel->GetWidth();
			s32	y = rand() % m_pLevel->GetHeight();

			if( m_pLevel->CanSpawn( x, y ) )
			{
				bSpawnFound = true;

				// Cr�tion d'une mine
				KMine*	pMine = new KMine( m_pRender, m_pLevel );
				KPt		MinePos( x, y );
				pMine->SetPos( MinePos );

				m_pLevel->AddpItem( pMine );
			}
		}
	}

	return KR_OK;
}

//---------------------------------------------------------------------------------------------------------------------
KRESULT KTBots::SpawnLaser( u32 nLaser )
{
	bool							bSpawnFound;
	
	// Cherche un point de spawn aleatoirement
	for( u32 i = 0; i < nLaser; i ++ )
	{
		bSpawnFound = false;
		while( !bSpawnFound )
		{
			s32	x = rand() % m_pLevel->GetWidth();
			s32	y = rand() % m_pLevel->GetHeight();

			if( m_pLevel->CanSpawn( x, y ) )
			{
				bSpawnFound = true;

				// Cr�tion du laser
				KLaser*	pLaser = new KLaser( m_pRender, m_pLevel );
				KPt		LaserPos( x, y );
				pLaser->SetPos( LaserPos );

				m_pLevel->AddpItem( pLaser );
			}
		}
	}

	return KR_OK;
}

//---------------------------------------------------------------------------------------------------------------------
s32 KTBots::GetnAliveBots()
{
	s32								i = 0;
	
	// Compte le nombre de Bot vivant
	for( KDllBotInfo* pDllBotInfo = m_DllBotInfos.GetFirst(); pDllBotInfo; pDllBotInfo = m_DllBotInfos.GetNext( pDllBotInfo ) )
	{
		if( pDllBotInfo->m_pBot && pDllBotInfo->m_pBot->GetLife() )
			i ++;
	}

	return i;
}

//---------------------------------------------------------------------------------------------------------------------
KRESULT	KTBots::NextTurn()
{
	s32 nBots = m_DllBotInfos.GetSize();

	// Si le Bot a effectu�toutes ses actions, on passe au Bot suivant
	if( !m_nRemainingActions )
	{
		// Recherche le Bot qui doit d�ormais jouer
		do
		{
			if( !nBots )
				return KR_OK;

			if( m_pCurrentBot == NULL )
			{
				// Dernier Bot, on revient au 1er
				m_pCurrentBot = m_DllBotInfos.GetFirst();
			}
			else
			{
				// Suivant...
				m_pCurrentBot = m_DllBotInfos.GetNext( m_pCurrentBot );
			}

			nBots --;
		} while( !m_pCurrentBot || !m_pCurrentBot->m_bFight || !m_pCurrentBot->m_pBot->GetLife() );

		// D�ermine le nombre d'actions que peux effectuer le Bot
		m_nRemainingActions = GETNUMACTIONS( m_pCurrentBot->m_pBot->GetSpeed() );
	}

	KASSERT( m_pCurrentBot );

	//
	// Demande l'action du Bot
	//
	KDllBotInfo*	pDllBotInfo = m_pCurrentBot;
	KBotActionInfo	BotActionInfo;

	KASSERT( pDllBotInfo->m_pBot );
	KASSERT( pDllBotInfo->m_pBotActionProc );

	// Determine le champs de vision
	KItemInfo*		pItems		= NULL;
	KItemInfo*		pInventory	= NULL;
	u32				nItems;
	u32				nInventory;
	s32				Sight;

	// Les jumelles permettent de voir tout le labyrinthe pendant 1 tour
	if( pDllBotInfo->m_pBot->IsUseBinocular() )
	{
		Sight = SIGHT_UNLIMITED;
		pDllBotInfo->m_pBot->SetbUseBinocular( false );
	}
	else
		Sight = pDllBotInfo->m_pBot->GetSight();

	nInventory = pDllBotInfo->m_pBot->GetChilds().GetSize();
	if( nInventory )
		pInventory = new KItemInfo[nInventory];

	u32 i = 0;
	for( KItem* pItem = pDllBotInfo->m_pBot->GetChilds().GetFirst(); pItem; pItem = pDllBotInfo->m_pBot->GetChilds().GetNext( pItem ) )
	{
		KPt	ItemPos( 0, 0 );
		pInventory[i].SetType( pItem->GetType() );
		pInventory[i].SetPos( ItemPos );
		pInventory[i].SetAlive( false );
		i ++;
	}
		
	nItems		= m_pLevel->GetpSightItem( pDllBotInfo->m_pBot->GetPos(), Sight, &pItems );

	// Fait une copie des infos du robot
	BotActionInfo.m_Pos			= pDllBotInfo->m_pBot->GetPos();
	BotActionInfo.m_Life		= pDllBotInfo->m_pBot->GetLife();
	BotActionInfo.m_Power		= pDllBotInfo->m_pBot->GetPower();
	BotActionInfo.m_Speed		= pDllBotInfo->m_pBot->GetSpeed();
	BotActionInfo.m_Vision		= pDllBotInfo->m_pBot->GetVision();
	BotActionInfo.m_pMapCell	= m_pLevel->GetpSightCell( pDllBotInfo->m_pBot->GetPos(), Sight );
	BotActionInfo.m_pItems		= pItems;
	BotActionInfo.m_nItems		= nItems;
	BotActionInfo.m_pInventory	= pInventory;
	BotActionInfo.m_nInventory	= nInventory;
	
	KBotAction*	pBotAction;
	pBotAction = ((KBotAction*(*)( KBotActionInfo*, void* ))pDllBotInfo->m_pBotActionProc)( &BotActionInfo, pDllBotInfo->m_pContext );

	SafeDelete( pInventory );

	switch( pBotAction->GetAction() )
	{
	case KBA_NOTHING:
		// Passe son tour
		StartActionNothing( pDllBotInfo );
		break;
	case KBA_MOVE:
		// Deplacement
		StartActionMove( pDllBotInfo, pBotAction->GetMoveDir() );
		break;
	case KBA_ATTACK:
		// Attaque
		StartActionAttack( pDllBotInfo, pBotAction->GetAttackTarget() );
		break;
	case KBA_ATTACKLASER:
		// Attaque
		StartActionAttackLaser( pDllBotInfo, pBotAction->GetAttackTarget() );
		break;
	case KBA_DROP:
		// Drop un Item
		StartActionDrop( pDllBotInfo, pBotAction->GetDropItemType() );
		break;
	default:
		break;
	}

	m_nRemainingActions --;

	//
	// Utilise les objets non prenable
	//

	// Recherche si il y a un objet ici, et qui ne soit pas un bot
	KItem* pCellItem = m_pLevel->GetpNotItem( pDllBotInfo->m_pBot->GetPos().x, pDllBotInfo->m_pBot->GetPos().y, KIT_BOT );
	if( pCellItem && !pCellItem->IsTakeAway() )
	{
		// D�lenche les mines quand on passe dessus
		if( pCellItem && ( pCellItem->GetType() == KIT_MINE ) )
		{
			if( ((KMine*)pCellItem)->GetState() == KMS_ACTIVATED )
			{
				((KMine*)pCellItem)->SetState( KMS_EXPLODED );

				// BOOM!!!
				s32		Life = 200;
				KPt		Pos = pCellItem->GetPos();
				Hurt( KPt( Pos.x - 1, Pos.y - 1 ), Life );
				Hurt( KPt( Pos.x    , Pos.y - 1 ), Life );
				Hurt( KPt( Pos.x + 1, Pos.y - 1 ), Life );
				Hurt( KPt( Pos.x - 1, Pos.y     ), Life );
				Hurt( KPt( Pos.x    , Pos.y     ), Life );
				Hurt( KPt( Pos.x + 1, Pos.y     ), Life );
				Hurt( KPt( Pos.x - 1, Pos.y + 1 ), Life );
				Hurt( KPt( Pos.x    , Pos.y + 1 ), Life );
				Hurt( KPt( Pos.x + 1, Pos.y + 1 ), Life );
			}
		}
		else
		{
			// Utilise l'item
			pDllBotInfo->m_pBot->Use( pCellItem );
		}
	}

	//
	//	Gestion des MINES
	//
	for( u32 m = 0; m < m_pLevel->GetnItems(); m ++ )
	{
		KItem* pItem = m_pLevel->GetpItem( m );
		if( pItem && pItem->GetType() == KIT_MINE )
		{
			switch( ((KMine*)pItem)->GetState() )
			{
			case KMS_PLANTED:
				((KMine*)pItem)->SetState( KMS_ACTIVATED );
				break;
			case KMS_EXPLODED:
				((KMine*)pItem)->SetState( KMS_DESTROYED );
				break;
			case KMS_DESTROYED:
				// D�ruit la mine
				m_pLevel->RemovepItem( pItem );
				SafeDelete( pItem );
				break;
			default:
				break;
			}
		}		
	}

	m_RespawnCountDown --;
	if( m_RespawnCountDown <= 0 )
	{
		SpawnItems();
		m_RespawnCountDown = ITEM_RESPAWN_TURN;
	}


	return KR_OK;
}

//---------------------------------------------------------------------------------------------------------------------
KRESULT	KTBots::StartActionNothing( KDllBotInfo* pDllBotInfo )
{
	KASSERT( pDllBotInfo->m_pBot );

	// Lance l'action
	pDllBotInfo->m_pBot->SetAction( KBA_NOTHING );
	m_ActionEndTime = g_Timer.GetTime() + BOT_WAIT_TIME;	// Temps d'attente

/////////////////////
// BEGIN Added by Mac Load : 01-08-2004
////////////////////
	// S'il y a un item, on le prend et il devient fils de bot si l'item est portable
	KPt	BotPos = pDllBotInfo->m_pBot->GetPos();
	KItem* pItem = m_pLevel->GetpNotItem( BotPos.x, BotPos.y , KIT_BOT );
	if( pItem && pItem->IsTakeAway() && ( pDllBotInfo->m_pBot->GetChilds().GetSize() < BOT_MAX_CHILDS ) )
	{
		// Ajoute le pere au fils
		pItem->SetpFather( pDllBotInfo->m_pBot );
		// Ajoute le fils au pere
		pDllBotInfo->m_pBot->GetChilds().Add( pItem );
		// Enleve l'item du level
		m_pLevel->RemovepItem( pItem );
	}
/////////////////////
// END Added by Mac Load : 01-08-2004
////////////////////

	// Status Message
	char	pMess[1024];
	sprintf( pMess, "%s is sleeping...", pDllBotInfo->m_pBot->GetpName() );
	SetpMessage( pMess );

	return KR_OK;
}

//---------------------------------------------------------------------------------------------------------------------
KRESULT	KTBots::StartActionMove( KDllBotInfo* pDllBotInfo, KBOTMOVEDIR MoveDir )
{
	KASSERT( pDllBotInfo->m_pBot );

	char	pMess[1024];

	KTIME	MoveSpeed = BOT_MOVE_TIME;

	// Lance l'action
	pDllBotInfo->m_pBot->SetAction( KBA_MOVE );
	m_ActionEndTime = g_Timer.GetTime() + MoveSpeed;	// Temps de d�lacement

	KPt	BotPos = pDllBotInfo->m_pBot->GetPos();
	KPt	DeltaPos = KLevel::GetDeltaPos( MoveDir );
	
	// Verifie s'il peut se deplacer
	if( m_pLevel->CanMove( BotPos.x + DeltaPos.x, BotPos.y + DeltaPos.y ) )
	{
		// S'il y a un item, on le prend et il devient fils de bot si l'item est portable
		KItem* pItem = m_pLevel->GetpNotItem( BotPos.x + DeltaPos.x, BotPos.y + DeltaPos.y, KIT_BOT );

		if( pItem && pItem->IsTakeAway() && ( pDllBotInfo->m_pBot->GetChilds().GetSize() < BOT_MAX_CHILDS ) )
		{
			// Ajoute le pere au fils
			pItem->SetpFather( pDllBotInfo->m_pBot );
			// Ajoute le fils au pere
			pDllBotInfo->m_pBot->GetChilds().Add( pItem );
			// Enleve l'item du level
			m_pLevel->RemovepItem( pItem );
		}

		// Changement de position
		KPt	BotPos( BotPos.x + DeltaPos.x, BotPos.y + DeltaPos.y );
		pDllBotInfo->m_pBot->SetPos( BotPos, MoveSpeed );

		sprintf( pMess, "%s is moving...", pDllBotInfo->m_pBot->GetpName() );
	}
	else
	{
		sprintf( pMess, "%s has tried to move to a impossible cell...", pDllBotInfo->m_pBot->GetpName() );
	}
	
	// Status Message
	SetpMessage( pMess );

	return KR_OK;
}

//---------------------------------------------------------------------------------------------------------------------
KRESULT KTBots::StartActionAttack( KDllBotInfo* pDllBotInfo, KBOTATTACKTARGET Target )
{
	KASSERT( pDllBotInfo->m_pBot );

	char	pMess[1024];

	KTIME	AttackTime = BOT_ATTACK_TIME;

	// Lance l'action
	pDllBotInfo->m_pBot->SetAction( KBA_ATTACK );
	m_ActionEndTime = g_Timer.GetTime() + AttackTime;	// Dur� de l'attaque

	pDllBotInfo->m_pBot->SetAttackTarget( Target );

	// Verifie si quelqu'un est touche
	KPt	DeltaPos	= KLevel::GetDeltaAttack( pDllBotInfo->m_pBot->GetAttackTarget() );
	KPt	TargetPos	= pDllBotInfo->m_pBot->GetPos() + DeltaPos;

	// Inflige des d�ats si un bot est touch�
	Hurt( TargetPos, GET_DAMAGE_ATTACK( pDllBotInfo->m_pBot->GetPower() ) );

	sprintf( pMess, "%s is attacking...", pDllBotInfo->m_pBot->GetpName() );

	// Status Message
	SetpMessage( pMess );

	return KR_OK;
}

//---------------------------------------------------------------------------------------------------------------------
KRESULT KTBots::StartActionAttackLaser( KDllBotInfo* pDllBotInfo, KBOTATTACKTARGET Target )
{
	KASSERT( pDllBotInfo->m_pBot );

	KLaser*	pLaser = NULL;

	// Recherche un laser dans l'inventaire
	for( KItem* pItem = pDllBotInfo->m_pBot->GetChilds().GetFirst(); pItem; pItem = pDllBotInfo->m_pBot->GetChilds().GetNext( pItem ) )
	{
		if( pItem->GetType() == KIT_LASER )
		{
			pLaser = (KLaser*)pItem;
			break;
		}		
	}

	if( !pLaser )
	{
		// Le Robot n'a pas de laser dans son inventaire, il passe son tour...
		return StartActionNothing( pDllBotInfo );
	}

	char	pMess[1024];

	KTIME	AttackTime = BOT_ATTACKLASER_TIME;

	// Lance l'action
	pDllBotInfo->m_pBot->SetAction( KBA_ATTACKLASER );
	m_ActionEndTime = g_Timer.GetTime() + AttackTime;	// Dur� de l'attaque

	pDllBotInfo->m_pBot->SetAttackTarget( Target );

	KPt	DeltaPos	= KLevel::GetDeltaAttack( pDllBotInfo->m_pBot->GetAttackTarget() );

	// Inflige des d�ats a tout les Bots dans cette direction sur tout le Range
	s32	Range;
// Original Version
/*	
	for( Range = 0; Range < pLaser->GetRange(); Range ++ )
	{
		KPt	TargetPos	= pDllBotInfo->m_pBot->GetPos() + DeltaPos;
		TargetPos.x *= (Range + 1);
		TargetPos.y *= (Range + 1);
		
		// On arrete le laser si on touche un mur
		if( m_pLevel->GetpCell( TargetPos.x, TargetPos.y )->GetType() != 0 )
			break;

		// Inflige des d�ats si un bot est sur le passage du laser
		Hurt( TargetPos, GET_DAMAGE_ATTACKLASER( Range + 1 ) );
	}
*/
// Modified by Mac Load 23-03-2004
	for( Range = 0; Range < pLaser->GetRange(); Range ++ )
	{
		KPt TempDeltaPos = DeltaPos ;
		TempDeltaPos.x *= (Range + 1);
		TempDeltaPos.y *= (Range + 1);

		KPt	TargetPos	= pDllBotInfo->m_pBot->GetPos() + TempDeltaPos ;
		
		// On arrete le laser si on touche un mur
		if( m_pLevel->GetpCell( TargetPos.x, TargetPos.y )->GetType() != 0 )
			break;

		// Inflige des degats si un bot est sur le passage du laser
// Old Version
/*
		Hurt( TargetPos, GET_DAMAGE_ATTACKLASER( Range + 1 ) );
*/
// Modified by Mac Load
		Hurt( TargetPos, GET_DAMAGE_ATTACKLASER( Range ) );
// End Modified by Mac Load
	}

	pDllBotInfo->m_pBot->SetFireRange( Range );

	// Le laser n'est valable qu'une seule fois, on le d�ruit de l'inventaire
	pDllBotInfo->m_pBot->GetChilds().Remove( pLaser );
	SafeDelete( pLaser );

	sprintf( pMess, "%s is attacking with a laser...", pDllBotInfo->m_pBot->GetpName() );

	// Status Message
	SetpMessage( pMess );

	return KR_OK;
}

//---------------------------------------------------------------------------------------------------------------------
KRESULT KTBots::StartActionDrop( KDllBotInfo* pDllBotInfo, KITEMTYPE ItemType )
{
	KASSERT( pDllBotInfo->m_pBot );

	// Recherche si il y a un objet ici, et qui ne soit pas un bot
	KItem* pCellItem = m_pLevel->GetpNotItem( pDllBotInfo->m_pBot->GetPos().x, pDllBotInfo->m_pBot->GetPos().y, KIT_BOT );
	if( pCellItem )
	{
		// On ne pose pas 2 objets l'un sur l'autre, donc on annule l'action
		return StartActionNothing( pDllBotInfo );
	}

	// Lance l'action
	pDllBotInfo->m_pBot->SetAction( KBA_DROP );
	m_ActionEndTime = g_Timer.GetTime() + BOT_DROP_TIME;	// Temps d'attente
	
	// Recherche l'objet dans l'inventaire
	for( KItem* pItem = pDllBotInfo->m_pBot->GetChilds().GetFirst(); pItem; pItem = pDllBotInfo->m_pBot->GetChilds().GetNext( pItem ) )
	{
		if( pItem->GetType() == ItemType )
		{
			// On le pose par terre
			pDllBotInfo->m_pBot->GetChilds().Remove( pItem );
			pItem->SetpFather( NULL );
			pItem->SetPos( pDllBotInfo->m_pBot->GetPos() );
			m_pLevel->AddpItem( pItem );

			// Si c'est une mine, on l'active
			if( pItem->GetType() == KIT_MINE )
			{
				KMine*	pMine = (KMine*)pItem;

				pMine->SetState( KMS_PLANTED );
			}
		}
	}

	// Status Message
	char	pMess[1024];
	sprintf( pMess, "%s is dropping a item...", pDllBotInfo->m_pBot->GetpName() );
	SetpMessage( pMess );

	return KR_OK;
}

//---------------------------------------------------------------------------------------------------------------------
void KTBots::Hurt( KPt Pos, s32 Life )
{
	KBot*	pBot = (KBot*)m_pLevel->GetpItem( Pos.x, Pos.y, KIT_BOT );
	if( pBot )
	{
		// Inflige les d�ats
		s32	NewLife = MAX( 0, pBot->GetLife() - Life );
		pBot->SetLife( NewLife );
		if( !NewLife )
		{
			// Bot mort
			pBot->SetAction( KBA_NOTHING );
		}
	}
}

#ifdef _WIN32
//---------------------------------------------------------------------------------------------------------------------
KRESULT KTBots::LoadDLLBots()
{
	WIN32_FIND_DATA			wfData;
	HANDLE					hFind;
	HMODULE					hModule;
	FARPROC					pGetpBotNameProc;
	char*					pBotName;
	char					pFileName[1024];

	// Recherche les DLL de tous les bots install�
	hFind = FindFirstFile( "Bots\\*.dll", &wfData );
	if( hFind != INVALID_HANDLE_VALUE )
	{
		do
		{
			strcpy( pFileName, "Bots\\" );
			strcat( pFileName, wfData.cFileName );
			if( hModule = LoadLibrary( pFileName ) )
			{
				if( pGetpBotNameProc = GetProcAddress( hModule, "GetpBotName" ) )
				{
					pBotName = ((char*(*)())pGetpBotNameProc)();

					// Ajoute les infos dans la liste
					KDllBotInfo*	pDllBotInfo = new KDllBotInfo();
					pDllBotInfo->SetDllName( wfData.cFileName );
					pDllBotInfo->SetBotName( pBotName );
					m_DllBotInfos.Add( pDllBotInfo );
				}
				else
					KERROR( "Function 'GetpBotName' not found in %s\nCorrupted DLL ?", pFileName );

				FreeLibrary( hModule );
			}

		} while( FindNextFile( hFind, &wfData ) );
	}
	FindClose( hFind );

	// Recherche les .maz
	hFind = FindFirstFile( "*.maz", &wfData );
	if( hFind != INVALID_HANDLE_VALUE )
	{
		do
		{
			m_MazeList.Add( strdup( wfData.cFileName ) );
		} while( FindNextFile( hFind, &wfData ) );
	}
	FindClose( hFind );

	// Affiche la liste des Bots trouv�	
	if( DialogBox( m_hInstance, MAKEINTRESOURCE( IDD_DIALOG_BOTS_LIST ), m_hWnd, DialogBotsListProc ) != IDOK )
		return KR_ERROR;

	// Flush la liste de map
	char*	pMazeName = NULL;
	while( pMazeName = m_MazeList.GetFirst() )
	{
		m_MazeList.Remove( pMazeName );
		SafeFree( pMazeName );
	}


	// Initialise les Bots
	u32								Number = 0;

	for( KDllBotInfo* pDllBotInfo = m_DllBotInfos.GetFirst(); pDllBotInfo; pDllBotInfo = m_DllBotInfos.GetNext( pDllBotInfo ) )
	{
		if( pDllBotInfo->m_bFight )
		{
			// Charge la DLL
			strcpy( pFileName, "Bots\\" );
			strcat( pFileName, pDllBotInfo->m_pDllName );
			if( hModule = LoadLibrary( pFileName ) )
			{
				FARPROC		pGetpInfosProc;
				FARPROC		pGetpBotActionProc;
				FARPROC		pStartGameProc;
				FARPROC		pEndGameProc;

				pDllBotInfo->m_hModule	= hModule;
				
				// Infos du Bot
				pGetpInfosProc = GetProcAddress( hModule, "GetpInfos" );
				if( !pGetpInfosProc )
				{
					KERROR( "Function 'GetpInfos' not found in %s\nCorrupted DLL ?", pFileName );
					return KR_ERROR;
				}
				pDllBotInfo->m_pInfosProc = pGetpInfosProc;

				// Fonction pour les requetes du Bot
				pGetpBotActionProc = GetProcAddress( hModule, "GetpBotAction" );
				if( !pGetpBotActionProc )
				{
					KERROR( "Function 'GetpBotAction' not found in %s\nCorrupted DLL ?", pFileName );
					return KR_ERROR;
				}
				pDllBotInfo->m_pBotActionProc = pGetpBotActionProc;

				// Fonction StartGame
				pStartGameProc = GetProcAddress( hModule, "StartGame" );
				if( !pStartGameProc )
				{
					KERROR( "Function 'StartGame' not found in %s\nCorrupted DLL ?", pFileName );
					return KR_ERROR;
				}
				pDllBotInfo->m_pStartGameProc = pStartGameProc;

				// Fonction EndGame
				pEndGameProc = GetProcAddress( hModule, "EndGame" );
				if( !pEndGameProc )
				{
					KERROR( "Function 'EndGame' not found in %s\nCorrupted DLL ?", pFileName );
					return KR_ERROR;
				}
				pDllBotInfo->m_pEndGameProc = pEndGameProc;

				// Fonction DebugDisplay pour le DEBUG UNIQUEMENT!
#if _DEBUG
	#if _WIN32
				FARPROC		pDebugDisplayProc;
				pDebugDisplayProc = GetProcAddress( hModule, "DebugDisplay" );
				pDllBotInfo->m_pDebugDisplayProc = pDebugDisplayProc;
	#endif _WIN32
#endif _DEBUG

				pDllBotInfo->m_BotNumber = Number;

				Number ++;
			}
		}
	}

	return KR_OK;
}

//---------------------------------------------------------------------------------------------------------------------
INT_PTR CALLBACK KTBots::DialogBotsListProc( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
	switch( uMsg )
	{
	case WM_INITDIALOG:
		{
			// Affiche la liste des DLL trouv�s dans la listbox
			for( KDllBotInfo* pDllBotInfo = g_pTBots->m_DllBotInfos.GetFirst(); pDllBotInfo; pDllBotInfo = g_pTBots->m_DllBotInfos.GetNext( pDllBotInfo ) )
				SendDlgItemMessage( hWnd, IDC_LIST_BOTS, LB_ADDSTRING, 0, (LPARAM)(LPCTSTR)pDllBotInfo->m_pBotName );

			// Affiche la liste des mazes trouv� dans la listbox
			for( char* pMazeName = g_pTBots->m_MazeList.GetFirst(); pMazeName; pMazeName = g_pTBots->m_MazeList.GetNext( pMazeName ) )
				SendDlgItemMessage( hWnd, IDC_LIST_MAZES, LB_ADDSTRING, 0, (LPARAM)(LPCTSTR)pMazeName );

			SendDlgItemMessage( hWnd, IDC_LIST_MAZES, LB_SETCURSEL, 0, 0 );
			break;
		}

	case WM_COMMAND:
        switch( LOWORD( wParam ) ) 
        { 
            case IDOK:
				{
					int		pIndex[1024];
					int		nIndex = SendDlgItemMessage( hWnd, IDC_LIST_BOTS, LB_GETSELITEMS, 1024, (LPARAM)(int*)pIndex );

					s32								i = 0;								
					s32								j = 0;								

					// Tag les Bots qui ont ��s�ectionn�pour le combat
					for( KDllBotInfo* pDllBotInfo = g_pTBots->m_DllBotInfos.GetFirst(); pDllBotInfo; pDllBotInfo = g_pTBots->m_DllBotInfos.GetNext( pDllBotInfo ) )
					{
						if( pIndex[j] == i )
						{
							pDllBotInfo->m_bFight = true;
							j ++;
						}

						i ++;
					}

					if( j < 2 )
					{
						KERROR( "A least 2 bots for a battle..." );
						return TRUE;
					}

					
					int		ItemSelected;

					ItemSelected = SendDlgItemMessage( hWnd, IDC_LIST_MAZES, LB_GETCURSEL, 0, 0 );
					if( ItemSelected == LB_ERR )
					{
						KERROR( "Select a map!" );
						return TRUE;
					}
					
					char	pMazeName[1024];
					SendDlgItemMessage( hWnd, IDC_LIST_MAZES, LB_GETTEXT, ItemSelected, (LPARAM)(LPSTR)pMazeName );
					g_pTBots->SetpMazeName( pMazeName );
				}

            case IDCANCEL:
                EndDialog( hWnd, wParam ); 
                return TRUE; 

            case IDC_BUTTON_SELECTALL:
                {
					int	nItems = SendDlgItemMessage( hWnd, IDC_LIST_BOTS, LB_GETCOUNT , 0, 0 );
					SendDlgItemMessage( hWnd, IDC_LIST_BOTS, LB_SELITEMRANGE, TRUE, MAKELPARAM( 0, nItems ) );

				}
                return TRUE; 

			case IDC_LIST_BOTS:
				{
					if( HIWORD( wParam ) == LBN_SELCHANGE )
					{
						// Affiche le nombre de bots s�ectionn�	
						char	pText[256];
						int nSelected = SendDlgItemMessage( hWnd, IDC_LIST_BOTS, LB_GETSELCOUNT, 0, 0 );
						sprintf( pText, "%i bot%s selected.", nSelected, (nSelected > 1 ) ? "s" : "" );
						SetDlgItemText( hWnd, IDC_LABEL_BOTS_SELECTED, pText );
						
						return TRUE;
					}
				}
				break;
        } 
	}

	return FALSE;
}

INT_PTR CALLBACK KTBots::DialogAboutProc( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
	switch( uMsg )
	{
		case WM_NOTIFY:
			{
				if( LOWORD(wParam) == IDC_ABOUT_TEXT )
				{
					ENLINK*	pEnlink = (ENLINK*)(LPARAM)lParam;
					if( pEnlink->msg == WM_LBUTTONDOWN )
					{
						ShellExecute( NULL, "open", "http://www.t-bots.com", NULL, NULL, SW_SHOW );
					}
				}
				
				return TRUE;
			}
		case WM_INITDIALOG:
			{
				CHARFORMAT2	Format, FormatTitle;
				memset( &Format, 0, sizeof( Format ) );
				Format.cbSize		= sizeof( Format );
				Format.dwMask		= CFM_COLOR | CFM_SIZE | CFM_FACE | CFM_BOLD;
				Format.yHeight		= 200;
				Format.dwEffects	= 0;
				Format.crTextColor	= RGB( 100, 50, 255 );	
				
				memset( &FormatTitle, 0, sizeof( FormatTitle ) );
				FormatTitle.cbSize		= sizeof( FormatTitle );
				FormatTitle.dwMask		= CFM_COLOR | CFM_SIZE | CFM_FACE | CFM_BOLD;
				FormatTitle.yHeight		= 250;
				FormatTitle.dwEffects	= CFE_BOLD;
				FormatTitle.crTextColor	= RGB( 30, 20, 200 );	
				strcpy( FormatTitle.szFaceName, "Trebuchet MS" );
				
				DWORD Mask = SendDlgItemMessage(hWnd, IDC_ABOUT_TEXT, EM_GETEVENTMASK, 0, 0);
				SendDlgItemMessage(hWnd,IDC_ABOUT_TEXT, EM_SETEVENTMASK, 0, Mask | ENM_LINK);
				SendDlgItemMessage(hWnd,IDC_ABOUT_TEXT, EM_AUTOURLDETECT, TRUE, 0);

				SendDlgItemMessage( hWnd,IDC_ABOUT_TEXT, EM_SETCHARFORMAT, (WPARAM)SCF_SELECTION, (LPARAM)&FormatTitle );
				SendDlgItemMessage(hWnd,IDC_ABOUT_TEXT, EM_REPLACESEL, (WPARAM)FALSE, (LPARAM)"---- Project Leader ----");
				SendDlgItemMessage( hWnd,IDC_ABOUT_TEXT, EM_SETCHARFORMAT, (WPARAM)SCF_SELECTION, (LPARAM)&Format );
				SendDlgItemMessage(hWnd,IDC_ABOUT_TEXT, EM_REPLACESEL, (WPARAM)FALSE, (LPARAM)"\nDino");

				SendDlgItemMessage( hWnd,IDC_ABOUT_TEXT, EM_SETCHARFORMAT, (WPARAM)SCF_SELECTION, (LPARAM)&FormatTitle );
				SendDlgItemMessage(hWnd,IDC_ABOUT_TEXT, EM_REPLACESEL, (WPARAM)FALSE, (LPARAM)"\n\n---- Game Programmer ----");
				SendDlgItemMessage( hWnd,IDC_ABOUT_TEXT, EM_SETCHARFORMAT, (WPARAM)SCF_SELECTION, (LPARAM)&Format );
				SendDlgItemMessage(hWnd,IDC_ABOUT_TEXT, EM_REPLACESEL, (WPARAM)FALSE, (LPARAM)"\nAitonfrere");
				SendDlgItemMessage(hWnd,IDC_ABOUT_TEXT, EM_REPLACESEL, (WPARAM)FALSE, (LPARAM)"\nMac Load");

				SendDlgItemMessage( hWnd,IDC_ABOUT_TEXT, EM_SETCHARFORMAT, (WPARAM)SCF_SELECTION, (LPARAM)&FormatTitle );
				SendDlgItemMessage(hWnd,IDC_ABOUT_TEXT, EM_REPLACESEL, (WPARAM)FALSE, (LPARAM)"\n\n---- Web Developer ----");
				SendDlgItemMessage( hWnd,IDC_ABOUT_TEXT, EM_SETCHARFORMAT, (WPARAM)SCF_SELECTION, (LPARAM)&Format );
				SendDlgItemMessage(hWnd,IDC_ABOUT_TEXT, EM_REPLACESEL, (WPARAM)FALSE, (LPARAM)"\nNicn0c");

				SendDlgItemMessage( hWnd,IDC_ABOUT_TEXT, EM_SETCHARFORMAT, (WPARAM)SCF_SELECTION, (LPARAM)&Format );
				SendDlgItemMessage(hWnd,IDC_ABOUT_TEXT, EM_REPLACESEL, (WPARAM)FALSE, (LPARAM)"\n\nWeb : http://www.t-bots.com");
		
				return TRUE;
			}
		case WM_SHOWWINDOW:
			{
				SendDlgItemMessage(hWnd,IDC_ABOUT_TEXT, EM_SETSEL, (WPARAM)-1, (LPARAM)-1 );

				return TRUE;
			}
		case WM_CLOSE:
			{
				EndDialog( hWnd, wParam );
				
				return TRUE;
			}
	}

	return FALSE;
}

#endif // _WIN32

#ifdef _LINUX
//---------------------------------------------------------------------------------------------------------------------
KRESULT KTBots::LoadSOBots()
{
	DIR*	pDir;
	dirent*	pDirEnt;
	void*	pLibrary;
	void*	pFunction;
	char	pFileName[1024];
	char*	pBotName;
	
	// Recherche les DLL de tous les bots installés
	printf( "Scanning bots...\n" );	
	if( !(pDir = opendir( "Bots" ) ) )
	{
		KERROR( "Directory 'Bots' not found\n", pFileName );
		return KR_ERROR;
	}
	
	while( ( pDirEnt = readdir( pDir ) ) )
	{
		if( strlen( pDirEnt->d_name ) < 3 )
			continue;
		
		if( strcasecmp( ".so", &pDirEnt->d_name[strlen( pDirEnt->d_name ) - 3] ) )
			continue;
		
		strcpy( pFileName, "Bots/" );
		strcat( pFileName, pDirEnt->d_name );
		if( !( pLibrary = dlopen( pFileName, RTLD_NOW ) ) )
		{
			KERROR( "Cannot load Bot library [%s]\n", pFileName );
			continue;
		}
		
		if( !( pFunction = dlsym( pLibrary, "GetpBotName" ) ) )
		{
			KERROR( "Cannot found 'GetpBotName' function in library [%s]\n", pFileName );
			continue;
		}
		
		pBotName = ((char*(*)())pFunction)();
		
		printf( "Bot '%s' found in library %s\n", pBotName, pDirEnt->d_name );	
		
		// Ajoute les infos dans la liste
		KDllBotInfo*	pDllBotInfo = new KDllBotInfo();
		pDllBotInfo->SetDllName( pDirEnt->d_name );
		pDllBotInfo->SetBotName( pBotName );
		pDllBotInfo->m_bFight = true;
		m_DllBotInfos.Add( pDllBotInfo );
		
		dlclose( pLibrary );
	}
	
	closedir( pDir );

	// Recherche les .maz
	printf( "Scanning maps...\n" );	
	if( !(pDir = opendir( "." ) ) )
	{
		KERROR( "Cannot open current directory\n" );
		return KR_ERROR;
	}
	
	while( ( pDirEnt = readdir( pDir ) ) )
	{
		if( strlen( pDirEnt->d_name ) < 4 )
			continue;
		
		if( strcasecmp( ".maz", &pDirEnt->d_name[strlen( pDirEnt->d_name ) - 4] ) )
			continue;
			
		m_MazeList.Add( strdup( pDirEnt->d_name ) );
		printf( "Map '%s' found\n", pDirEnt->d_name );	
	}
	
	closedir( pDir );

	// Map par defaut	
	SetpMazeName( "Maze1.maz" );
	
		// Initialise les Bots
	u32								Number = 0;

	printf( "Current players:\n" );
	for( KDllBotInfo* pDllBotInfo = m_DllBotInfos.GetFirst(); pDllBotInfo; pDllBotInfo = m_DllBotInfos.GetNext( pDllBotInfo ) )
	{
		if( pDllBotInfo->m_bFight )
		{
			// Charge la DLL
			strcpy( pFileName, "Bots/" );
			strcat( pFileName, pDllBotInfo->m_pDllName );
			if( ( pLibrary = dlopen( pFileName, RTLD_NOW ) ) )
			{
				FARPROC		pGetpInfosProc;
				FARPROC		pGetpBotActionProc;
				FARPROC		pStartGameProc;
				FARPROC		pEndGameProc;

				pDllBotInfo->m_hModule	= pLibrary;
				
				// Infos du Bot
				pGetpInfosProc = dlsym( pLibrary, "GetpInfos" );
				if( !pGetpInfosProc )
				{
					KERROR( "Function 'GetpInfos' not found in %s\nCorrupted DLL ?", pFileName );
					return KR_ERROR;
				}
				pDllBotInfo->m_pInfosProc = pGetpInfosProc;

				// Fonction pour les requetes du Bot
				pGetpBotActionProc = dlsym( pLibrary, "GetpBotAction" );
				if( !pGetpBotActionProc )
				{
					KERROR( "Function 'GetpBotAction' not found in %s\nCorrupted DLL ?", pFileName );
					return KR_ERROR;
				}
				pDllBotInfo->m_pBotActionProc = pGetpBotActionProc;

				// Fonction StartGame
				pStartGameProc = dlsym( pLibrary, "StartGame" );
				if( !pStartGameProc )
				{
					KERROR( "Function 'StartGame' not found in %s\nCorrupted DLL ?", pFileName );
					return KR_ERROR;
				}
				pDllBotInfo->m_pStartGameProc = pStartGameProc;

				// Fonction EndGame
				pEndGameProc = dlsym( pLibrary, "EndGame" );
				if( !pEndGameProc )
				{
					KERROR( "Function 'EndGame' not found in %s\nCorrupted DLL ?", pFileName );
					return KR_ERROR;
				}
				pDllBotInfo->m_pEndGameProc = pEndGameProc;

				// Fonction DebugDisplay pour le DEBUG UNIQUEMENT!
#if _DEBUG
	#if _WIN32
				FARPROC		pDebugDisplayProc;
				pDebugDisplayProc = GetProcAddress( hModule, "DebugDisplay" );
				pDllBotInfo->m_pDebugDisplayProc = pDebugDisplayProc;
	#endif _WIN32
#endif _DEBUG

				pDllBotInfo->m_BotNumber = Number;

				Number ++;
				printf( "%i %s\n", Number, pDllBotInfo->m_pBotName );
			}
		}
	}

	return KR_OK;
}

#endif // _LINUX

//---------------------------------------------------------------------------------------------------------------------
KRESULT KTBots::SetpMessage( char* pMessage )
{
	if( m_pStatusMessage )
		Freep( m_pStatusMessage );

	if( pMessage )
		m_pStatusMessage = strdup( pMessage );

	return KR_OK;
}

//---------------------------------------------------------------------------------------------------------------------
void KTBots::SetpMazeName( char* pMazeName )
{
	SafeFree( m_pMazeName );

	if( pMazeName )
		m_pMazeName = strdup( pMazeName );
}

